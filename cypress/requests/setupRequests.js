import setupModel from '../fixtures/baseModels/setupModel';

import { SETUP_ID } from '../fixtures/fakeConstants';


function cloneSetup(waitFunc){
    cy.route({
        method: 'POST',
        url: `api/setups/${SETUP_ID}/clone`,
        response: { response: setupModel.get() },
    }).as('cloneSetup');

    waitFunc();

    cy.wait(['@cloneSetup']);
}

module.exports = {
    cloneSetup,
};
