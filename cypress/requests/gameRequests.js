import setupModel from '../fixtures/baseModels/setupModel';


function updateSetup(waitFunc, onRequest){
    cy.route({
        method: 'PUT',
        url: 'api/setups',
        response: { response: setupModel.get() },
        onRequest,
    }).as('setSetup');

    waitFunc();

    cy.wait(['@setSetup']);
}

module.exports = {
    updateSetup,
};
