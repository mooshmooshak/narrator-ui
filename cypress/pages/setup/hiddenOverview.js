// i guess this is for non mobile
function openEditor(){
    cy.get('.setupEditorPane .setupEntitiesContainer .entityDetailsContainer').within(() => {
        cy.get('.entityDetailsEditorButton').click();
    });
}

function assertNoEditButton(){
    cy.get('.setupEditorPane .setupEntitiesContainer .entityDetailsContainer').within(() => {
        cy.get('.entityDetailsEditorButton').should('not.be.visible');
    });
}

module.exports = {
    openEditor,

    assertNoEditButton,
};
