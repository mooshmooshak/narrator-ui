import { SETUP_HIDDEN_ID } from '../../fixtures/fakeConstants';

function click(name){
    cy.get('.setupEditorPane .titledListItems').within(() => {
        cy.contains(name).click();
    });
}

function removeSetupHidden(name){
    cy.route({
        method: 'DELETE',
        url: `api/setupHiddens/${SETUP_HIDDEN_ID}`,
        response: '',
    }).as('setupHiddenDeleteResponse');
    clickRemoveButton(name);
    cy.wait(['@setupHiddenDeleteResponse']);
}

function assertColor(color){
    cy.contains('Roles List').parent().within(() => {
        cy.get('.titledListItem span')
            .should('have.css', 'color')
            .and('be.colored', color);
    });
}

function assertText(text){
    cy.contains('Roles List').parent().within(() => {
        cy.contains(text);
    });
}

function assertSize(size){
    cy.get('.setupEditorPane .setupHiddensContainer')
        .find('.titledListItem').should('have.length', size);
    cy.get('.setupEditorPane .setupHiddensContainer .titledListHeader').contains(`(${size})`);
}

module.exports = {
    click,
    removeSetupHidden,

    assertColor,
    assertSize,
    assertText,
};

function clickRemoveButton(name){
    cy.get('.setupEditorPane').within(() => {
        cy.contains('Roles List').parent().within(() => {
            cy.contains(name).parent().within(() => {
                cy.get('.titledListItemActionIcon').click();
            });
        });
    });
}
