function turnOnFullCustomizability(){
    cy.get('.setupEditorAdvancedToggle').within(() => {
        cy.contains('ON').click();
    });
}

module.exports = {
    turnOnFullCustomizability,
};
