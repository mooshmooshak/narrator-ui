import { MAX_PLAYER_COUNT } from '../../../src/js/util/constants';

import roleModel from '../../fixtures/baseModels/roleModel';

import factionRoleAbilityModifierResponse from '../../fixtures/responses/factionRoleAbilityModifierResponse'; // eslint-disable-line max-len
import setupModifierResponse from '../../fixtures/responses/setupModifierResponse';


function clickOther(roleName){
    cy.get('.setupEditorPane').within(() => {
        cy.contains('Also available in').parent().within(() => {
            cy.contains(roleName).click();
        });
    });
}

function setFactionRoleAbilityModifierInputValue(value, responseValue, customRoleResponse){
    cy.route({
        method: 'POST',
        url: 'api/factionRoles/*/abilities/*/modifiers',
        response: { response: setupModifierResponse.get({ value: responseValue }) },
        onRequest: ensureMinMaxPlayerCount,
    }).as('factionRoleAbilityModifierResponse');
    cy.route({
        method: 'GET',
        url: 'api/factionRoles/*',
        response: { response: customRoleResponse || roleModel.get() },
    }).as('factionRoleResponse');

    cy.get('.factionRolesContainer .factionRoleEditor input').clear();
    cy.get('.factionRolesContainer .factionRoleEditor input').type(value);

    cy.wait(['@factionRoleAbilityModifierResponse', '@factionRoleResponse']);
}

function setFactionRoleModifierInputValue(value, responseValue, customRoleResponse){
    cy.route({
        method: 'POST',
        url: 'api/factionRoles/*/modifiers',
        response: { response: setupModifierResponse.get({ value: responseValue }) },
        onRequest: ensureMinMaxPlayerCount,
    }).as('roleModifierResponse');
    cy.route({
        method: 'GET',
        url: 'api/factionRoles/*',
        response: { response: customRoleResponse || roleModel.get() },
    }).as('factionRoleResponse');

    cy.get('.factionRolesContainer .factionRoleEditor input').clear();
    cy.get('.factionRolesContainer .factionRoleEditor input').type(value);

    cy.wait(['@roleModifierResponse', '@factionRoleResponse']);
}

function setFactionRoleAbilityModifierChecked(isChecked, customRoleResponse){
    cy.route({
        method: 'POST',
        url: 'api/factionRoles/*/abilities/*/modifiers',
        response: factionRoleAbilityModifierResponse.get(isChecked),
        onRequest: ensureMinMaxPlayerCount,
    }).as('factionRoleAbilityModifierResponse');
    cy.route({
        method: 'GET',
        url: 'api/factionRoles/*',
        response: { response: customRoleResponse || roleModel.get() },
    }).as('factionRoleResponse');

    if(isChecked)
        cy.get('.factionRolesContainer .factionRoleEditor input').check();
    else
        cy.get('.factionRolesContainer .factionRoleEditor input').uncheck();

    cy.wait(['@factionRoleAbilityModifierResponse', '@factionRoleResponse']);
}

function setFactionRoleModifierChecked(isChecked, customRoleResponse){
    cy.route({
        method: 'POST',
        url: 'api/factionRoles/*/modifiers',
        response: factionRoleAbilityModifierResponse.get(isChecked),
        onRequest: ensureMinMaxPlayerCount,
    }).as('roleModifierResponse');
    cy.route({
        method: 'GET',
        url: 'api/factionRoles/*',
        response: { response: customRoleResponse || roleModel.get() },
    }).as('factionRoleResponse');

    if(isChecked)
        cy.get('.factionRolesContainer .factionRoleEditor input').check();
    else
        cy.get('.factionRolesContainer .factionRoleEditor input').uncheck();

    cy.wait(['@roleModifierResponse', '@factionRoleResponse']);
}

function setSetupModifierChecked(isChecked, roleID, customRoleResponse){
    roleID = roleID || '*';
    cy.route({
        method: 'POST',
        url: 'api/setupModifiers',
        response: { response: setupModifierResponse.get({ value: isChecked }) },
    }).as('setupModifierResponse');
    cy.route({
        method: 'GET',
        url: `api/factionRoles/${roleID}`,
        response: { response: customRoleResponse || roleModel.get() },
    }).as('factionRoleResponse');

    if(isChecked)
        cy.get('.factionRolesContainer .factionRoleEditor input').check();
    else
        cy.get('.factionRolesContainer .factionRoleEditor input').uncheck();

    cy.wait(['@setupModifierResponse', '@factionRoleResponse']);
}

function setSetupModifierInputValue(value, responseValue, customResponse){
    cy.route({
        method: 'POST',
        url: 'api/setupModifiers',
        response: { response: setupModifierResponse.get({ value: responseValue }) },
    }).as('setupModifierResponse');
    cy.route({
        method: 'GET',
        url: 'api/factionRoles/*',
        response: { response: customResponse || roleModel.get() },
    }).as('factionRoleResponse');
    cy.get('.factionRolesContainer .factionRoleEditor input').clear();
    cy.get('.factionRolesContainer .factionRoleEditor input').type(value);
    cy.wait(['@setupModifierResponse', '@factionRoleResponse']);
}

function assertHeader(headerText){
    cy.get('.factionRolesContainer .entityDetailsHeader').should('contain', headerText);
}

function assertHeaderColor(color){
    cy.get('.factionRolesContainer .entityDetailsHeaderText')
        .should('have.css', 'color')
        .and('be.colored', color);
}

function assertInputValue(value){
    cy.get('.factionRolesContainer .factionRoleEditor input')
        .should('have.value', value.toString());
}

function assertInvisible(){
    cy.get('.role_settings_pane').should('not.be.visible');
}

function assertIsChecked(){
    cy.get('.factionRolesContainer .factionRoleEditor input').should('be.checked');
}

function assertIsUnchecked(){
    cy.get('#setup_role_modifiers input').should('not.be.checked');
}

function assertOtherColor(roleName, color){
    cy.contains('Also available in').parent().within(() => {
        cy.contains(roleName).should('have.css', 'color').and('be.colored', color);
    });
}

function assertSpawnable(spawnable){
    cy.get('.factionRolesContainer')
        .find(`.entityDetailsContainer .roleHover:contains("${spawnable}")`)
        .should('have.length', 1);
}

function assertVisible(){
    cy.get('.factionRolesContainer .entityDetailsContainer').should('be.visible');
    cy.get('.factionRolesContainer .entityDetailsContainer').should('exist');
}

function assertVisibleModifiers(modifiers){
    modifiers.forEach(modifier => {
        cy.get('.factionRolesContainer .entityDetailsContainer').should('contain', modifier);
    });
}

module.exports = {
    clickOther,
    setFactionRoleAbilityModifierChecked,
    setFactionRoleAbilityModifierInputValue,
    setFactionRoleModifierChecked,
    setFactionRoleModifierInputValue,
    setSetupModifierChecked,
    setSetupModifierInputValue,

    assertHeader,
    assertHeaderColor,
    assertInputValue,
    assertInvisible,
    assertIsChecked,
    assertIsUnchecked,
    assertOtherColor,
    assertSpawnable,
    assertVisible,
    assertVisibleModifiers,
};

function ensureMinMaxPlayerCount(requestResponse){
    const { body } = requestResponse.request;
    expect(body.minPlayerCount).to.be.equal(0);
    expect(body.maxPlayerCount).to.be.equal(MAX_PLAYER_COUNT);
}
