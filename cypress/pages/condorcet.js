function assertInCurrentRanking(name){
    cy.contains('Current Consensus').parent().within(() => {
        cy.get('.draggableList').within(() => {
            cy.contains(name);
        });
    });
}

function assertInPersonalList(name){
    cy.contains('\'s Ranking').parent().within(() => {
        cy.get('.draggableList').within(() => {
            cy.contains(name);
        });
    });
}

module.exports = {
    assertInCurrentRanking,
    assertInPersonalList,
};
