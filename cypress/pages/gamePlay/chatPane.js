function assertInputInvisible(){
    cy.get('.chat_input').should('not.be.visible');
}

function assertInputVisible(){
    cy.get('.chat_input').should('be.visible');
}

function assertMessageVisible(text){
    cy.get('#messages').should('contain', text);
}

module.exports = {
    assertInputInvisible,
    assertInputVisible,
    assertMessageVisible,
};
