function clickEnemyFaction(factionName){
    cy.get('#your_exp_extras').within(() => {
        cy.contains(factionName).click();
    });
}

function clickFaction(){
    cy.get('#yourRole .roleHover').click();
}

function assertAllyVisible(playerName){
    cy.get('#your_exp_extras').within(() => {
        cy.contains(playerName);
    });
}

function assertDetails(details){
    details.forEach(detail => {
        cy.get('#your_exp_extras').should('contain', detail);
    });
}

function assertNameVisible(playerName){
    cy.get('#yourName').should('contain', playerName);
}

function assertOneClickableFaction(){
    cy.get('#yourRole').find('span').should('have.length', 2);
}

function assertRoleVisible(roleName){
    cy.get('#yourRole').should('contain', roleName);
    cy.get('#yourRole').should('be.visible');
}

module.exports = {
    clickEnemyFaction,
    clickFaction,

    assertAllyVisible,
    assertDetails,
    assertNameVisible,
    assertOneClickableFaction,
    assertRoleVisible,
};
