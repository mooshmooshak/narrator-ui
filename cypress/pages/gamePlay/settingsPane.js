import actionsResponse from '../../fixtures/responses/actionResponse';


function clickForceEndPhase(onRequest){
    cy.route({
        method: 'PUT',
        url: 'api/phases',
        response: actionsResponse.get(),
        onRequest,
    }).as('actionStub');
    cy.get('#forceEndNightButton').click();
    cy.wait(['@actionStub']);
}

function assertNightMusicOn(){
    cy.get('.night_music_enabled')
        .should('have.class', 'selected_toggle');
}

module.exports = {
    clickForceEndPhase,

    assertNightMusicOn,
};
