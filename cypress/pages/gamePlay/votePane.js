import * as colors from '../../../src/js/util/colors';


function clickSortByName(){
    cy.get('#vote_recap_pane').within(() => {
        cy.get('.fa-sort-alpha-up').click();
    });
}

function clickSortByVoteCount(){
    cy.get('#vote_recap_pane').within(() => {
        cy.get('.fa-sort-amount-up').click();
    });
}

function closeVotePopout(){
    cy.get('#vote_recap_pane .fa-minus-square').click();
}

function openVotePopout(){
    cy.get('#vote_recap_pane .fa-exclamation-circle').click();
}

function vote(playerName, { onRequest, response = {} }){
    cy.route({
        method: 'PUT',
        url: 'api/actions',
        response: { response },
        onRequest,
    }).as('actionResponse');
    cy.get('#vote_recap_pane').within(() => {
        cy.get('.voteListHeaderTitle').contains(`${playerName}`).parent().parent()
            .within(() => {
                cy.contains('Add vote').click();
            });
    });
    cy.wait(['@actionResponse']);
}

function assertEliminationHeader(){
    cy.get('#vote_recap_pane').within(() => {
        cy.contains('Votes for Elimination');
    });
}

function removeVote(playerName, controllerName, { onRequest, response = {} }){
    cy.route({
        method: 'PUT',
        url: 'api/actions',
        response: { response },
        onRequest,
    }).as('actionResponse');
    clickUnvote(playerName, controllerName);
    cy.wait(['@actionResponse']);
}

function stopVoting(playerName, controllerName, args = {}){
    cy.route({
        method: 'DELETE',
        url: 'api/actions?actionIndex=0&command=vote',
        response: '',
        onRequest: args.onRequest,
    }).as('actionResponse');

    clickUnvote(playerName, controllerName);

    cy.wait(['@actionResponse']);
}

function assertFirstVoteListsPosition(playerName){
    cy.get('#vote_recap_pane').within(() => {
        cy.get('.voteListHeaderContainer:first').within(() => {
            cy.get('.voteListHeaderTitle').within(() => {
                cy.contains(playerName);
            });
        });
    });
}

function assertMinVoteSubheader(minCount){
    cy.get('#vote_recap_pane').within(() => {
        cy.contains(`${minCount} vote minimum`);
    });
}

function assertNoVoters(votedPlayerName){
    getVoteTallyContext(votedPlayerName, () => {
        cy.contains('No voters').should('exist');
    });
}

function assertNotUnvoteable(votedPlayerName, voterPlayerName){
    getVoteTallyContext(votedPlayerName, () => {
        cy.get('.voteVotersList').within(() => {
            cy.contains(voterPlayerName).parent().get('.voteCheckableIcon').should('not.exist');
        });
    });
}

function assertNotVotingFor(voter, voted){
    getVoteTallyContext(voted, () => {
        cy.get('.voteVotersList').within(() => {
            cy.contains(voter).should('not.exist');
        });
    });
}

function assertNoVoteButton(playerName){
    getVoteTallyContext(playerName, () => {
        cy.contains('Add vote').should('not.exist');
    });
}

function assertNoVoteCancelPending(votedPlayerName, voterPlayerName){
    getVoteTallyContext(votedPlayerName, () => {
        cy.get('.voteVotersList').within(() => {
            cy.contains(voterPlayerName).parent().within(() => {
                cy.get('.votePendingIcon').should('not.exist');
            });
        });
    });
}

function assertPreTrialHeader(){
    cy.get('#vote_recap_pane').within(() => {
        cy.contains('Votes for Trial');
    });
}

function assertOnTrial(playerName){
    cy.get('#vote_recap_pane').within(() => {
        cy.contains(`${playerName} is on trial!`);
    });
}

function assertSortByNameHighlighted(){
    cy.get('#vote_recap_pane').within(() => {
        cy.get('.fa-sort-alpha-up')
            .should('have.css', 'color')
            .and('be.colored', colors.selectedIconColor);
    });
}

function assertSortByVoteCountHighlighted(){
    cy.get('#vote_recap_pane').within(() => {
        cy.get('.fa-sort-amount-up')
            .should('have.css', 'color')
            .and('be.colored', colors.selectedIconColor);
    });
}

function assertVoteButtonExists(votedPlayerName){
    getVoteTallyContext(votedPlayerName, () => {
        cy.contains('Add vote').should('exist');
    });
}

function assertVotePopoutClosed(){
    cy.get('.votePopout').should('not.be.visible');
}

function assertVotePopoutOpen(){
    cy.get('.votePopout').should('be.visible');
}

function assertVoteCount(votedPlayerName, voteCount){
    getVoteTallyContext(votedPlayerName, () => {
        cy.get('.voteListCount').contains(voteCount.toString());
    });
}

function assertVotingFor(voterPlayerName, votedPlayerName){
    getVoteTallyContext(votedPlayerName, () => {
        cy.get('.voteVotersList').within(() => {
            cy.contains(voterPlayerName).should('exist');
        });
    });
}

function assertVoteCancelPending(votedPlayerName, voterPlayerName){
    assertVotePending(votedPlayerName, voterPlayerName);
    getVoteTallyContext(votedPlayerName, () => {
        cy.get('.voteVotersList').within(() => {
            cy.contains(voterPlayerName).should('have.class', 'voteCancelPendingText');
        });
    });
}

function assertVotePending(votedPlayerName, voterPlayerName){
    getVoteTallyContext(votedPlayerName, () => {
        cy.get('.voteVotersList').within(() => {
            cy.contains(voterPlayerName).parent().get('.fa-stopwatch').should('exist');
        });
    });
}

module.exports = {
    clickSortByName,
    clickSortByVoteCount,
    closeVotePopout,
    openVotePopout,
    removeVote,
    stopVoting,
    vote,

    assertEliminationHeader,
    assertFirstVoteListsPosition,
    assertMinVoteSubheader,
    assertNoVoters,
    assertNotUnvoteable,
    assertNotVotingFor,
    assertNoVoteButton,
    assertNoVoteCancelPending,
    assertOnTrial,
    assertPreTrialHeader,
    assertSortByNameHighlighted,
    assertSortByVoteCountHighlighted,
    assertVoteButtonExists,
    assertVoteCount,
    assertVotePopoutClosed,
    assertVotePopoutOpen,
    assertVotingFor,
    assertVoteCancelPending,
    assertVotePending,
};

function getVoteTallyContext(votedPlayerName, assertionFunc){
    cy.get('#vote_recap_pane').within(() => {
        cy.get('.voteListHeaderTitle').contains(`${votedPlayerName}`).parent().parent()
            .within(assertionFunc);
    });
}

function clickUnvote(votedPlayerName, profileName){
    getVoteTallyContext(votedPlayerName, () => {
        cy.contains(profileName).click();
    });
}
