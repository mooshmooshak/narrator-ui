function assertTime(text){
    cy.get('#time .timerText').contains(text);
}

module.exports = {
    assertTime,
};
