function assertChatIcon(chatIcon){
    cy.get('#chat_ticker_new_message .chatPic')
        .should('have.class', chatIcon);
}

function assertChatIconColor(color){
    cy.get('#chat_ticker_new_message .chatPic')
        .should('have.css', 'color')
        .and('be.colored', color);
}

module.exports = {
    assertChatIcon,
    assertChatIconColor,
};
