function clickLast(){
    cy.get('#chat_tabs .chatTab:last').click();
}

module.exports = {
    clickLast,
};
