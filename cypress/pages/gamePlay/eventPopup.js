function assertTitle(titleText){
    cy.get('.newFeedbackHeader').contains(titleText);
}

function assertVisible(){
    cy.get('#feedbackBackdrop').should('be.visible');
}

module.exports = {
    assertTitle,
    assertVisible,
};
