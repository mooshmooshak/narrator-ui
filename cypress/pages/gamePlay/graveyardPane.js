function clickFirst(){
    cy.contains('Graveyard').parent().within(() => {
        cy.get('li:first').click();
    });
}

function assertCount(count){
    cy.contains('Graveyard').parent().within(() => {
        cy.get('ul').find('li').should('have.length', count);
    });
}

function assertVisibleRoleForPlayer(playerName, roleName){
    cy.contains('Graveyard').parent().within(() => {
        cy.contains(playerName).contains(roleName);
    });
}

module.exports = {
    clickFirst,

    assertCount,
    assertVisibleRoleForPlayer,
};
