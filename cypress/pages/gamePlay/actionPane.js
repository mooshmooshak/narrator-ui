function clickEndNight(){
    cy.get('#end_night_button').click();
}

function clickLeft(name){
    cy.get('#player_list').within(() => {
        cy.contains(name).parent().parent().find('.left_targetable_click')
            .click();
    });
}

function clickLeftSubmit(name, { onRequest, response = {} }){
    cy.route({
        method: 'PUT',
        url: 'api/actions',
        response: { response },
        onRequest,
    }).as('actionResponse');
    clickLeft(name);
    cy.wait(['@actionResponse']);
}

function clickLeftUnsubmit(name){
    cy.route({
        method: 'DELETE',
        url: 'api/actions*',
        response: '',
    }).as('actionResponse');
    clickLeft(name);
    cy.wait(['@actionResponse']);
}

function hoverLeft(name){
    cy.get('#playerListPane2').within(() => {
        cy.contains(name).parent().parent().find('.left_targetable_click')
            .trigger('mouseover');
    });
}

function assertActionButtonText(text){
    cy.get('#end_night_button').should('contain', text);
}

function assertActionButtonVisible(){
    cy.get('#end_night_button').should('be.visible');
}

function assertClicked(name){
    cy.contains(name).parent().parent().find('.left_playerList_selected')
        .should('have.length', 1);
    cy.contains(name).parent().parent().find('.right_playerList_selected_red')
        .should('have.length', 1);
}

function assertHeader(text){
    cy.get('#playerList_header span').contains(text);
}

function assertLeftClicked(name){
    cy.contains(name).parent().parent().find('.left_playerList_selected')
        .should('have.length', 1);
    cy.contains(name).parent().parent().find('.right_playerList_selected_red')
        .should('have.length', 0);
}

function assertNotClicked(name){
    cy.contains(name).parent().parent().find('.left_playerList_selected')
        .should('have.length', 0);
    cy.contains(name).parent().parent().find('.right_playerList_selected_red')
        .should('have.length', 0);
}

function assertNotVisible(targets){
    targets.forEach(target => {
        cy.get('#player_list').should('not.contain', target);
    });
    cy.get('#player_list').should('not.contain', 'undefined');
}

function assertRightClicked(name){
    cy.contains(name).parent().parent().find('.left_playerList_selected')
        .should('have.length', 0);
    cy.contains(name).parent().parent().find('.right_playerList_selected_red')
        .should('have.length', 1);
}

function assertRightNotHovered(name){
    cy.contains(name).parent().parent().find('.right_targetable.targetable_hovering')
        .should('have.length', 0);
}

function assertVisible(targets){
    targets.forEach(target => {
        cy.get('#player_list').should('contain', target);
    });
    cy.get('#player_list').should('not.contain', 'undefined');
}

module.exports = {
    clickEndNight,
    clickLeft,
    clickLeftSubmit,
    clickLeftUnsubmit,
    hoverLeft,

    assertActionButtonText,
    assertActionButtonVisible,
    assertClicked,
    assertHeader,
    assertLeftClicked,
    assertNotClicked,
    assertNotVisible,
    assertRightClicked,
    assertRightNotHovered,
    assertVisible,
};
