function clickActions(){
    cy.get('#nav_tab_4').click();
}

function clickGameOverview(){
    cy.get('#nav_tab_1').click();
}

function clickRoleOverview(){
    cy.get('#nav_tab_2').click();
}

function clickSettings(){
    cy.get('#nav_tab_5').click();
}

function clickVotePane(){
    cy.get('#nav_tab_3').click();
}

function assertGameOverviewSelected(){
    cy.get('#nav_tab_1').should('have.class', 'selected_nav');
    cy.get('.selected_nav').should('have.length', 1);
}

function assertLeaveButtonShowing(){
    cy.get('#nav_tab_5').should('contain', 'Exit');
}

module.exports = {
    clickActions,
    clickGameOverview,
    clickRoleOverview,
    clickSettings,
    clickVotePane,

    assertGameOverviewSelected,
    assertLeaveButtonShowing,
};
