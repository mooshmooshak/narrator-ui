import moderatorResponses from '../../fixtures/responses/moderatorResponses';
import playerResponses from '../../fixtures/responses/playerResponses';


function click(playerName){
    cy.get('.playersOverview').contains(playerName).click();
}

function clickSetAsHost(playerName, newHostID, onRequest){
    cy.route({
        method: 'POST',
        url: 'api/moderators/repick',
        response: moderatorResponses.repickHost(newHostID),
        onRequest,
    }).as('hostRepick');
    cy.contains('Set Host').click();
    cy.wait(['@hostRepick']);
}

function clickKickUser(userID){
    cy.route({
        method: 'DELETE',
        url: `api/players/kick?userID=${userID}`,
        response: playerResponses.kick(),
    }).as('kick');
    cy.contains('Kick').click();
    cy.wait(['@kick']);
}

function assertActive(playerName){
    assertPlayerDetailsContains(playerName, 'active');
}

function assertActiveHost(playerName){
    assertHost(playerName);
    assertActive(playerName);
}

function assertColor(playerName, playerColor){
    withinPlayerCard(playerName, () => {
        cy.get('.playerOverviewHeaderText')
            .should('have.css', 'color')
            .and('be.colored', playerColor);
    });
}

function assertDetailsHidden(playerName){
    withinPlayerCard(playerName, () => {
        cy.get('.playerOverviewDetails').should('not.be.visible');
    });
}

function assertDetailsVisible(playerName){
    withinPlayerCard(playerName, () => {
        // uncomment when stats start getting reported
        // cy.get('.playerOverviewDetails').should('be.visible');
        cy.get('.playerOverviewMenu').should('have.class', 'fas fa-chevron-up');
    });
}

function assertHost(playerName){
    assertPlayerDetailsContains(playerName, 'moderator');
}

function assertIcon(playerName, className){
    withinPlayerCard(playerName, () => {
        cy.get('.playerIcon').should('have.class', className);
    });
}

function assertPlayerHeaderCount(count){
    cy.get('.playersOverviewHeader').should('include.text', `(${count.toString()})`);
}

function assertIsComputer(playerName){
    assertPlayerDetailsContains(playerName, 'bot');
}

function assertInactive(playerName){
    assertPlayerDetailsNotContains(playerName, 'active');
    // check color when it's brought back
    // cy.get(`#setup_players_list .playerLI${playerName} .playerListSetupMarking`)
    //     .should('have.css', 'color')
    //     .and('be.colored', '#666');
}

function assertNameInUL(playerName){
    cy.get('.playersOverview').contains(playerName);
}

function assertNameNotInUL(playerName){
    cy.get('.playersOverview').should('not.have.text', playerName);
}

function assertNoRepickOption(playerName){
    withinPlayerCard(playerName, () => {
        cy.contains('Repick').should('not.be.visible');
    });
}

function assertRepickOption(playerName){
    withinPlayerCard(playerName, () => {
        cy.contains('Repick').should('be.visible');
    });
}

module.exports = {
    click,
    clickSetAsHost,
    clickKickUser,

    assertActive,
    assertActiveHost,
    assertColor,
    assertDetailsHidden,
    assertDetailsVisible,
    assertPlayerHeaderCount,
    assertHost,
    assertIcon,
    assertInactive,
    assertIsComputer,
    assertNameInUL,
    assertNameNotInUL,
    assertNoRepickOption,
    assertRepickOption,
};

function assertPlayerDetailsContains(playerName, text){
    withinPlayerCard(playerName, () => {
        cy.get('.playerOverviewDetails')
            .should('include.text', text);
    });
}

function assertPlayerDetailsNotContains(playerName, text){
    withinPlayerCard(playerName, () => {
        cy.get('.playerOverviewDetails')
            .should('not.include.text', text);
    });
}

function withinPlayerCard(playerName, func){
    cy.get('.playersOverview').contains(playerName)
        .parent().parent()
        .parent()
        .within(func);
}
