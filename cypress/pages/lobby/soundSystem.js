function assertNightMusicOn(){
    cy.get('#backgroundMusic').should('have.attr', 'src').and('include', 'audio/nighttime.mp3');
}

module.exports = {
    assertNightMusicOn,
};
