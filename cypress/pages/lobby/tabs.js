function goToGameSettings(){
    cy.get('#nav_tab_2').click();
}

function goToRoles(){
    cy.get('#nav_tab_3').click();
}

module.exports = {
    goToRoles,
    goToGameSettings,
};
