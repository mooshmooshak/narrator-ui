function assertChatIcon(chatIcon){
    cy.get('.lobbyChatTicker.chatTickerContainer .playerIcon')
        .should('have.class', chatIcon);
}

function assertChatIconColor(color){
    cy.get('.lobbyChatTicker.chatTickerContainer .playerIcon')
        .should('have.css', 'color')
        .and('be.colored', color);
}

module.exports = {
    assertChatIcon,
    assertChatIconColor,
};
