import { GAME_ID } from '../../fixtures/fakeConstants';


function sendMessage(text, args){
    cy.route({
        method: 'POST',
        onRequest: args.onRequest,
        response: '', // not currently using output yet
        url: `api/chats/${GAME_ID}/lobby`,
    }).as('sendMessageRequest');
    cy.get('.lobbyChatContainer .chatInput').type(text);

    assertChatInput(text);

    cy.get('.lobbyChatContainer .chatInput').type('\n');
    cy.wait(['@sendMessageRequest']);
}

function assertChatInput(text){
    cy.get('.lobbyChatContainer .chatInput').should('have.value', text);
}

function assertLength(length){
    cy.get('.lobbyChatContainer .chatMessage').should('have.length', length);
}

module.exports = {
    sendMessage,
    assertChatInput,
    assertLength,
};
