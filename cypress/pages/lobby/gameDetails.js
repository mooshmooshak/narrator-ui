import gameRequests from '../../requests/gameRequests';

import gameModifierResponse from '../../fixtures/responses/gameModifierResponse';
import setupModifierResponse from '../../fixtures/responses/setupModifierResponse';


function openEditor(){
    cy.get('.lobbyDetailsTextHeaderContainer i').click();
}

function setDayLengthInput(newValue, onRequest){
    cy.route({
        method: 'POST',
        url: 'api/gameModifiers',
        response: {
            response: gameModifierResponse.get({
                name: 'DAY_LENGTH_START',
                value: newValue,
            }),
        },
        onRequest,
    }).as('gameModifierRequest');
    withinPhaseEditor('DAY_LENGTH_START', () => {
        cy.get('.phaseLengthEditorInput input')
            .clear()
            .type(newValue);
        cy.get('.labelAdornmentLabel').click();
    });
    cy.wait(['@gameModifierRequest']);
}

function setDayStart(){
    withinEditorSection(1, () => {
        cy.contains('Day Start').parent().parent().within(() => {
            cy.contains('ON').click();
        });
    });
}

function setNightLengthDropdown(newValue, valueLabel, onRequest){
    cy.route({
        method: 'POST',
        url: 'api/gameModifiers',
        response: {
            response: gameModifierResponse.get({
                name: 'NIGHT_LENGTH',
                value: newValue,
            }),
        },
        onRequest,
    }).as('gameModifierRequest');

    withinEditorSection(1, () => {
        cy.contains('Night Length').parent().within(() => {
            cy.get('.dropdown').click();
        });
    });
    cy.get('.MuiMenu-list').within(() => {
        cy.contains(valueLabel).click();
    });

    cy.wait(['@gameModifierRequest']);
}

function setSetup(setupName, onRequest){
    gameRequests.updateSetup(() => {
        cy.get('.lobbyDetailsSection').within(() => {
            cy.contains('Setup').parent().parent().within(() => {
                cy.get('.dropdown').click();
            });
        });
        cy.get('.MuiMenu-list').within(() => {
            cy.contains(setupName).click();
        });
    }, onRequest);
}

function setVoteSystem({ label, value }){
    cy.route({
        method: 'POST',
        url: 'api/gameModifiers',
        response: {
            response: gameModifierResponse.get({
                name: 'DAY_LENGTH_START',
                value,
            }),
        },
    }).as('gameModifierRequest');

    setEditorDropdown(0, 'Vote System', label);

    cy.wait(['@gameModifierRequest']);
}

function showAdvancedPhaseSettings(){
    withinEditorSection(1, () => {
        cy.get('.lobbyShowAdvancedText').click();
    });
}

function showAdvancedMiscSettings(){
    withinEditorSection(2, () => {
        cy.get('.lobbyShowAdvancedText').click();
    });
}

function turnOffChatRoles(){
    cy.route({
        method: 'POST',
        url: 'api/gameModifiers',
        response: {
            response: gameModifierResponse.get({
                name: 'CHAT_ROLES',
                value: false,
            }),
        },
    }).as('gameModifierRequest');


    setEditorDropdown(2, 'Format', 'In person');

    cy.wait(['@gameModifierRequest']);
}

function turnOnDayStart(args = {}){
    cy.route({
        method: 'POST',
        url: 'api/setupModifiers',
        response: {
            response: setupModifierResponse.get({
                name: 'DAY_START',
                value: true,
            }),
        },
        onRequest: args.onRequest,
    }).as('setupModifierRequest');

    setDayStart();

    cy.wait(['@setupModifierRequest']);
}

function assertDayLength(dayLength){
    withinPhaseEditor('DAY_LENGTH_START', () => {
        cy.get('.phaseLengthEditorInput input').should('have.value', dayLength);
    });
}

function assertDayStart(isOn){
    assertToggleState(isOn, 1, 'Day Start');
}

function assertFormat(labelValue){
    cy.contains('Format').parent().within(() => {
        cy.contains(labelValue);
    });
}

function assertToggleState(isOn, index, label){
    const [selectedText, unselectedText] = isOn ? ['ON', 'OFF'] : ['OFF', 'ON'];
    withinEditorSection(index, () => {
        cy.contains(label).parent().parent().within(() => {
            cy.contains(selectedText).should('have.class', 'selectedToggle');
            cy.contains(unselectedText).should('not.have.class', 'selectedToggle');
        });
    });
}

function assertHostControls(){
    cy.get('.lobbyDetailsContainer .fas.fa-cogs').should('be.visible');
}

function assertNoChatRolesSetting(){
    cy.get('.lobbyOverviewDetailsContainer').within(() => {
        cy.contains('Some text-based roles will not spawn.').should('be.visible');
    });
}

function assertNoHostControls(){
    cy.get('#Day_Length_Start i').should('not.be.visible');
}

function assertSetupDropdownHidden(){
    cy.get('.setup_input').should('be.not.visible');
}

function assertVoteSystem(voteSystemName){
    cy.contains('Vote System').parent().within(() => {
        cy.contains(voteSystemName);
    });
}

module.exports = {
    openEditor,
    setDayLengthInput,
    setDayStart,
    setNightLengthDropdown,
    setSetup,
    setVoteSystem,
    showAdvancedMiscSettings,
    showAdvancedPhaseSettings,
    turnOffChatRoles,
    turnOnDayStart,

    assertDayLength,
    assertDayStart,
    assertFormat,
    assertHostControls,
    assertNoChatRolesSetting,
    assertNoHostControls,
    assertSetupDropdownHidden,
    assertVoteSystem,
};

function withinPhaseEditor(phaseModifierName, func){
    // +1 is for the day start label
    const index = ['DAY_LENGTH_START'].indexOf(phaseModifierName) + 1;
    cy.get('.lobbyOverviewMiscContainer .labelAdornmentContainer').eq(index).within(func);
}

function withinEditorSection(index, func){
    cy.get('.lobbyEditorContainer>.lobbyEditorAdvancedController').eq(index).within(func);
}

function setEditorDropdown(index, inputLabel, valueLabel){
    withinEditorSection(index, () => {
        cy.contains(inputLabel).parent().parent().within(() => {
            cy.get('.dropdown').click();
        });
    });
    cy.get('.MuiMenu-list').within(() => {
        cy.contains(valueLabel).click();
    });
}
