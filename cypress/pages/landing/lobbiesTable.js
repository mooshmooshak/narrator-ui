function assertLength(lobbyCount){
    cy.get('.lobbiesTable tbody tr').should('have.length', lobbyCount);
}

module.exports = {
    assertLength,
};
