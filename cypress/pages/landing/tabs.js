function logout(){
    cy.contains('Log Out').click();
}

function assertLoggedInNav(){
    cy.contains('Log Out').parent().within(() => {
        cy.get('i').should('have.class', 'fas fa-sign-out-alt');
    });
}

function assertLoggedOutNav(){
    cy.contains('Log In').parent().within(() => {
        cy.get('i').should('have.class', 'fa fa-sign-in-alt');
    });
}

module.exports = {
    logout,

    assertLoggedInNav,
    assertLoggedOutNav,
};
