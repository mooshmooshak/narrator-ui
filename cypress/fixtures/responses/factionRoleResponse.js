import factionRoleModel from '../baseModels/factionRoleModel';


function post(){
    return {
        response: factionRoleModel.get(),
    };
}

module.exports = {
    post,
};
