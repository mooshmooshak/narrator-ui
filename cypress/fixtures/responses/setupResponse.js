import setupModel from '../baseModels/setupModel';


function change(attributes = {}){
    return {
        setup: attributes.setup || setupModel.get(),
    };
}

function create(){
    return setupModel.get();
}

module.exports = {
    change,
    create,
};
