import hiddenModel from '../baseModels/hiddenModel';


function post(){
    return {
        response: hiddenModel.get(),
    };
}

module.exports = {
    post,
};
