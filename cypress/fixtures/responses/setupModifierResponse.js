import { SETUP_MODIFIER_NAME } from '../fakeConstants';

import setupModifierModel from '../baseModels/setupModifierModel';


function get(attributes = {}){
    const value = typeof(attributes.value) === 'undefined' ? true : attributes.value;
    return setupModifierModel.get({
        name: attributes.name || SETUP_MODIFIER_NAME,
        value,
    });
}

module.exports = {
    get,
};
