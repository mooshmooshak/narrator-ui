import { ROLE_ABILITY_MODIFIER_NAME } from '../fakeConstants';


function get(responseValue){
    responseValue = typeof(responseValue) === 'undefined' ? true : responseValue;
    return {
        response: {
            value: responseValue,
            name: ROLE_ABILITY_MODIFIER_NAME,
        },
    };
}

module.exports = {
    get,
};
