import setupHiddenModel from '../baseModels/setupHiddenModel';


function post(){
    return {
        response: setupHiddenModel.get(),
    };
}

module.exports = {
    post,
};
