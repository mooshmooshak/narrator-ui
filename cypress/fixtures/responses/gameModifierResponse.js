import { GAME_MODIFIER_NAME } from '../fakeConstants';

import gameModifierModel from '../baseModels/gameModifierModel';


function get(attributes = {}){
    return gameModifierModel.get({
        name: attributes.name || GAME_MODIFIER_NAME,
        value: attributes.value,
    });
}

module.exports = {
    get,
};
