function get(){
    return {
        errors: [],
        response: {
            value: true,
        },
    };
}

module.exports = {
    get,
};
