const profileModel = require('../baseModels/profileModel');
const setupModel = require('../baseModels/setupModel');
const voteInfoModel = require('../baseModels/voteInfoModel');


function get(attributes = {}){
    return {
        event: 'dayStart',
        profile: attributes.profile || profileModel.getInGame(),
        setup: setupModel.getDefaultStarted(),
        voteInfo: voteInfoModel.get(),
    };
}

module.exports = {
    get,
};
