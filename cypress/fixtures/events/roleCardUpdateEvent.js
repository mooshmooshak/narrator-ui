const profileModel = require('../baseModels/profileModel');


function get(){
    return {
        event: 'roleCardUpdate',
        profile: profileModel.getInGame(),
    };
}

module.exports = {
    get,
};
