import { USER_ID } from '../fakeConstants';


function get(attributes = {}){
    return {
        event: 'playerStatusChange',
        isActive: attributes.isActive,
        userID: attributes.userID || USER_ID,
    };
}

module.exports = {
    get,
};
