const { CHAT_MESSAGE_ID, CHAT_TEXT, USER_ID } = require('../fakeConstants');


function get(){
    return {
        createdAt: new Date().toString(),
        event: 'lobbyChatMessageEvent',
        id: CHAT_MESSAGE_ID,
        text: CHAT_TEXT,
        userID: USER_ID,
    };
}

module.exports = {
    get,
};
