import playerModel from '../baseModels/playerModel';

function get(attributes = {}){
    return {
        event: 'playerUpdate',
        player: attributes.player || playerModel.getInGame(),
    };
}
module.exports = {
    get,
};
