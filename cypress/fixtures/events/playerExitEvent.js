import setupModel from '../baseModels/setupModel';

import { PLAYER_NAME, USER_ID } from '../fakeConstants';


function get(attributes = {}){
    return {
        event: 'playerExit',
        moderatorIDs: attributes.moderatorIDs || [USER_ID],
        playerName: attributes.playerName || PLAYER_NAME,
        setup: attributes.setup || setupModel.get(),
    };
}

module.exports = {
    get,
};
