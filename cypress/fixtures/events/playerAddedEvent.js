import { PLAYER_NAME2 } from '../fakeConstants';

import playerModel from '../baseModels/playerModel';
import setupModel from '../baseModels/setupModel';


function get(attributes = {}){
    const playerName = attributes.playerName || PLAYER_NAME2;
    return {
        event: 'playerAdded',
        players: attributes.players || [playerModel.get({ name: playerName })],
        setup: attributes.setup || setupModel.get(),
    };
}

module.exports = {
    get,
};
