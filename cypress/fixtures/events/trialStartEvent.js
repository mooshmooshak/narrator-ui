import playerModel from '../baseModels/playerModel';

function get(){
    return {
        event: 'trialStart',
        trialedUser: playerModel.getInGame(),
    };
}

module.exports = {
    get,
};
