import { USER_ID } from '../fakeConstants';


function get(attributes = {}){
    return {
        event: 'hostChange',
        hostID: attributes.hostID || USER_ID,
    };
}

module.exports = {
    get,
};
