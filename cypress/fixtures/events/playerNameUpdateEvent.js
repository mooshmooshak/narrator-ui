import { PLAYER_ID, PLAYER_NAME2 } from '../fakeConstants';


function get(attributes = {}){
    return {
        event: 'playerNameUpdate',
        playerID: PLAYER_ID,
        playerName: attributes.name || PLAYER_NAME2,
    };
}

module.exports = {
    get,
};
