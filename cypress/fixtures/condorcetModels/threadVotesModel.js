import { PLAYER_NAME, PLAYER_NAME2 } from '../fakeConstants';


function get(attributes = {}){
    return {
        options: attributes.options || [PLAYER_NAME, PLAYER_NAME2],
        voterMetadata: attributes.voterMetadata || {},
    };
}

module.exports = {
    get,
};
