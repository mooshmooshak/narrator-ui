import {
    FACTION_ID,
    FACTION_ID2, GAME_ID, PLAYER_NAME, ROLE_NAME, USER_ID,
} from '../fakeConstants';


function getInGame(attributes = {}){
    return {
        allies: attributes.allies || [],
        isJailed: attributes.isJailed || false,
        gameID: GAME_ID,
        name: attributes.name || PLAYER_NAME,
        roleCard: {
            abilities: attributes.abilities || [],
            actions: attributes.actions || [],
            details: ['You have a role.'],
            enemyFactionIDs: [FACTION_ID2],
            factionID: FACTION_ID,
            roleName: ROLE_NAME,
            // roleID: ROLE_ID,
        },
        userID: USER_ID,
    };
}

function getInLobby(attributes = {}){
    return {
        gameID: GAME_ID,
        name: PLAYER_NAME,
        userID: attributes.userID || USER_ID,
    };
}

function getNoGame(){
    return {
        userID: USER_ID,
    };
}

module.exports = {
    getInGame,
    getInLobby,
    getNoGame,
};
