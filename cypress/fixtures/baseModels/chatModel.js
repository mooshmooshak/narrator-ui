import { CHAT_NAME } from '../fakeConstants';


function get(attributes = {}){
    return {
        chatReset: [{
            chatKey: 'everyone',
            chatName: CHAT_NAME,
            chatType: 'fas fa-sun',
            isDay: true,
        }],
        message: attributes.messages || [],
    };
}

module.exports = {
    get,
};
