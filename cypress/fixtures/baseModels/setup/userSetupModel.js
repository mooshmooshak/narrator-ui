import { SETUP_KEY, SETUP_NAME } from '../../fakeConstants';


function get(attributes = {}){
    return {
        key: SETUP_KEY,
        name: SETUP_NAME,
        description: attributes.description,
        slogan: attributes.slogan,
    };
}

module.exports = {
    get,
};
