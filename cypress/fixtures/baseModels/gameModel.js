import gameModifierModel from './gameModifierModel';
import phaseModel from './phaseModel';
import playerModel from './playerModel';
import setupModel from './setupModel';
import userModel from './userModel';
import voteInfoModel from './voteInfoModel';

import { GAME_ID, LOBBY_ID } from '../fakeConstants';


function get(attributes = {}){
    return {
        id: GAME_ID,
        integrations: attributes.integrations || [],
        isStarted: attributes.isStarted,
        joinID: LOBBY_ID,
        modifiers: getModifiers(attributes.modifiers),
        phase: attributes.phase || [phaseModel.getNight()],
        players: attributes.players || [playerModel.get()],
        setup: attributes.setup || setupModel.get(),
        users: addModerators(attributes.users || [userModel.get({ isModerator: true })],
            attributes.moderatorIDs),
        voteInfo: attributes.voteInfo || voteInfoModel.get(),
    };
}

function getModifiers(input = {}){
    return {
        CHAT_ROLES: gameModifierModel.get({ name: 'CHAT_ROLES', value: true }),
        DAY_LENGTH_MIN: gameModifierModel.get({ name: 'DAY_LENGTH_MIN', value: 60 }),
        DAY_LENGTH_START: gameModifierModel.get({ name: 'DAY_LENGTH_START', value: 60 }),
        DAY_LENGTH_DECREASE: gameModifierModel.get({ name: 'DAY_LENGTH_DECREASE', value: 0 }),
        NIGHT_LENGTH: gameModifierModel.get({ name: 'NIGHT_LENGTH', value: 60 }),
        OMNISCIENT_DEAD: gameModifierModel.get({ name: 'OMNISCIENT_DEAD', value: true }),
        TRIAL_LENGTH: gameModifierModel.get({ name: 'TRIAL_LENGTH', value: 0 }),
        VOTE_SYSTEM: gameModifierModel.get({ name: 'VOTE_SYSTEM', value: 1 }),
        ...input,
    };
}

module.exports = {
    get,
    getModifiers,
};

function addModerators(users, moderatorIDs){
    if(!moderatorIDs)
        return users;
    return users.map(user => ({ ...user, isModerator: moderatorIDs.includes(user.id) }));
}
