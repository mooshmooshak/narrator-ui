import { MAX_PLAYER_COUNT } from '../../../src/js/util/constants';

import {
    FACTION_ROLE_ID,
    FACTION_ROLE_ID2, HIDDEN_ID,
    HIDDEN_SPAWN_ID,
    HIDDEN_SPAWN_ID2,
} from '../fakeConstants';


function get(attributes = {}){
    return {
        hiddenID: attributes.hiddenID || HIDDEN_ID,
        id: attributes.id || HIDDEN_SPAWN_ID,
        minPlayerCount: 0,
        maxPlayerCount: MAX_PLAYER_COUNT,
        factionRoleID: attributes.factionRoleID || FACTION_ROLE_ID,
    };
}

function get2(){
    return get({
        factionRoleID: FACTION_ROLE_ID2,
        id: HIDDEN_SPAWN_ID2,
    });
}

module.exports = {
    get,
    get2,
};
