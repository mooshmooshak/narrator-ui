import { GAME_MODIFIER_NAME } from '../fakeConstants';


function get(attributes = {}){
    return {
        name: attributes.name || GAME_MODIFIER_NAME,
        value: attributes.value !== 'undefined' ? attributes.value : true,
    };
}

module.exports = {
    get,
};
