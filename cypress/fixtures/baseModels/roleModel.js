import {
    ROLE_ID, ROLE_ID2, ROLE_NAME, ROLE_NAME2,
} from '../fakeConstants';

function get(attributes = {}){
    return {
        abilities: attributes.abilities || [],
        details: attributes.details || [],
        id: attributes.id || ROLE_ID,
        modifiers: attributes.modifiers || [],
        name: attributes.name || ROLE_NAME,
    };
}

function get2(attributes = {}){
    attributes.name = attributes.name || ROLE_NAME2;
    attributes.id = attributes.id || ROLE_ID2;
    return get(attributes);
}

module.exports = {
    get,
    get2,
};
