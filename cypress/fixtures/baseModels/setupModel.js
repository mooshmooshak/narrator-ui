import {
    FACTION_ID, FACTION_ID2, SETUP_ID, SETUP_NAME, USER_ID,
} from '../fakeConstants';

import factionRoleModel from './factionRoleModel';
import factionModel from './factionModel';
import roleModel from './roleModel';
import hiddenModel from './hiddenModel';
import hiddenSpawnModel from './hiddenSpawnModel';
import setupHiddenModel from './setupHiddenModel';
import setupModifierModel from './setupModifierModel';

function get(attributes = {}){
    return {
        factions: attributes.factions || [],
        hiddens: attributes.hiddens || [],
        key: attributes.key,
        id: attributes.id || SETUP_ID,
        isEditable: 'isEditable' in attributes ? attributes.isEditable : true,
        name: SETUP_NAME,
        ownerID: USER_ID,
        roles: attributes.roles || [],
        setupHiddens: attributes.setupHiddens || [],
        setupModifiers: getSetupModifiers(attributes.setupModifiers),
    };
}

function getDefaultStarted(attributes = {}){
    return {
        factions: [
            factionModel.get({
                enemyIDs: [FACTION_ID2],
                factionRoles: [factionRoleModel.get()],
            }),
            factionModel.get2({
                enemyIDs: [FACTION_ID],
                factionRoles: [factionRoleModel.get2()],
            }),
        ],
        hiddens: [hiddenModel.get({ spawns: [hiddenSpawnModel.get(), hiddenSpawnModel.get2()] })],
        roles: [roleModel.get(), roleModel.get2()],
        setupHiddens: attributes.setupHiddens || [setupHiddenModel.get()],
        setupModifiers: getSetupModifiers(attributes.setupModifiers),
    };
}

module.exports = {
    get,
    getDefaultStarted,
};

function getSetupModifiers(input = []){
    return [
        ...[
            setupModifierModel.get({ name: 'CHARGE_VARIABILITY', value: 60 }),
            setupModifierModel.get({ name: 'CULT_PROMOTION', value: false }),
            setupModifierModel.get({ name: 'DAY_START', value: false }),
            setupModifierModel.get({ name: 'DIFFERENTIATED_FACTION_KILLS', value: false }),
            setupModifierModel.get({ name: 'LAST_WILL', value: false }),
            setupModifierModel.get({ name: 'PUNCH_ALLOWED', value: false }),
            setupModifierModel.get({ name: 'SELF_VOTE', value: false }),
            setupModifierModel.get({ name: 'SKIP_VOTE', value: true }),
        ].filter(m => !input.find(im => im.name === m.name)),
        ...input,
    ];
}
