function get(attributes = {}){
    return {
        allowedTargets: attributes.allowedTargets || [],
        voterToVotes: attributes.voterToVotes || {},
        trialedPlayerName: attributes.trialedPlayerName,
    };
}

module.exports = {
    get,
};
