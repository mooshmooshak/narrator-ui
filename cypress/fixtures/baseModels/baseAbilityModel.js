const { ABILITY_NAME } = require('../fakeConstants');


function get(){
    return {
        isFactionAllowed: true,
        type: ABILITY_NAME,
    };
}

module.exports = {
    get,
};
