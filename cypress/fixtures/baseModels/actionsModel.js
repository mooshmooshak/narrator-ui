function get(attributes = {}){
    return {
        actionList: attributes.actions || [],
    };
}

module.exports = {
    get,
};
