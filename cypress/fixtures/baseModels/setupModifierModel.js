import { SETUP_MODIFIER_NAME } from '../fakeConstants';


function get(attributes = {}){
    return {
        name: attributes.name || SETUP_MODIFIER_NAME,
        value: attributes.value !== 'undefined' ? attributes.value : true,
    };
}

module.exports = {
    get,
};
