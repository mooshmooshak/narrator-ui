import { FACTION_ID, PLAYER_NAME, ROLE_NAME } from '../fakeConstants';

function get(attributes = {}){
    return {
        dayNumber: 1,
        deathTypes: ['Died via day execution'],
        factionID: 'factionID' in attributes ? attributes.factionID : FACTION_ID,
        name: PLAYER_NAME,
        phase: true,
        roleName: ROLE_NAME,
    };
}

function getCleaned(){
    return get({ factionID: null });
}

module.exports = {
    get,
    getCleaned,
};
