import { USER_ID, USER_ID2, USER_NAME } from '../fakeConstants';


function get(attributes = {}){
    return {
        id: attributes.id || USER_ID,
        isModerator: 'isModerator' in attributes ? attributes.isModerator : false,
        name: USER_NAME,
    };
}

function get2(attributes = {}){
    return get({ id: USER_ID2, ...attributes });
}

module.exports = {
    get,
    get2,
};
