import { SETUP_ID_FEATURED, SETUP_NAME } from '../fakeConstants';


function get(attributes = {}){
    return {
        name: attributes.name || SETUP_NAME,
        description: attributes.description || '',
        slogan: attributes.slogan,
        id: Object.keys(attributes).includes('id') ? attributes.id : SETUP_ID_FEATURED,
    };
}

function getFrom(setup){
    return get({
        name: setup.name,
        description: setup.description,
        slogan: setup.slogan,
        id: setup.id,
    });
}

module.exports = {
    get,
    getFrom,
};
