import { PHASE_NAME } from '../../../src/js/util/constants';


function getDay(attributes = {}){
    return getBase({
        ...attributes,
        name: PHASE_NAME.VOTES_OPEN,
    });
}

function getFinished(){
    return getBase({
        name: PHASE_NAME.FINISHED,
    });
}

function getNight(){
    return getBase({
        name: PHASE_NAME.NIGHT,
    });
}

function getTrial(){
    return getBase({
        name: PHASE_NAME.TRIAL,
    });
}

function getUnstarted(){
    return getBase({
        name: PHASE_NAME.UNSTARTED,
    });
}

module.exports = {
    getDay,
    getFinished,
    getNight,
    getTrial,
    getUnstarted,
};

function getBase(attributes){
    const endTimeOffset = attributes.endTimeOffset || 30;
    return {
        dayNumber: 1,
        name: attributes.name,
        endTime: attributes.endTime || new Date().getTime() + (endTimeOffset * 1000), // 30 seconds in the future
        length: attributes.length || 240, // 4 minutes
    };
}
