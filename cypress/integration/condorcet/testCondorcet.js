import threadVotesModel from '../../fixtures/condorcetModels/threadVotesModel';

import condorcet from '../../pages/condorcet';

import { PLAYER_NAME, PLAYER_NAME2, PLAYER_NAME3 } from '../../fixtures/fakeConstants';

import { SKIP_DAY } from '../../../src/js/util/constants';


describe('Test Condorcet', () => {
    context('When the server returns raw votes from sc2mafia', () => {
        it('Will reorder votes to not allow self voting', () => {
            cy.goToCondorcet({
                urlParams: {
                    allowSelfVote: false,
                },
                threadVotes: threadVotesModel.get({
                    options: [PLAYER_NAME, PLAYER_NAME2, PLAYER_NAME3],
                    voterMetadata: {
                        [PLAYER_NAME]: { ranking: [[PLAYER_NAME]] },
                    },
                }),

                condorcetSubmitRequest: requestResponse => {
                    const { body } = requestResponse.request;
                    expect(body).to.deep.equal({
                        [PLAYER_NAME]: [[PLAYER_NAME2, PLAYER_NAME3], [PLAYER_NAME]],
                        [PLAYER_NAME2]: [[PLAYER_NAME, PLAYER_NAME3], [PLAYER_NAME2]],
                        [PLAYER_NAME3]: [[PLAYER_NAME, PLAYER_NAME2], [PLAYER_NAME3]],
                    });
                },
            });
        });

        it('Will stop sending self votes to the server if the param is off', () => {
            cy.goToCondorcet({
                urlParams: {
                    allowSelfVote: false,
                },
                threadVotes: threadVotesModel.get({
                    options: [PLAYER_NAME],
                }),

                condorcetSubmitRequest: requestResponse => {
                    const { body } = requestResponse.request;
                    expect(body).to.deep.equal({ [PLAYER_NAME]: [[PLAYER_NAME]] });
                },
            });
        });

        it('Will allow self votes to the server if the param is on', () => {
            cy.goToCondorcet({
                urlParams: {
                    allowSelfVote: true,
                },
                threadVotes: threadVotesModel.get({
                    options: [PLAYER_NAME],
                }),

                condorcetSubmitRequest: requestResponse => {
                    const { body } = requestResponse.request;
                    expect(body).to.deep.equal({});
                },
            });
        });

        it('Will allow votes from everyone if the param is on', () => {
            cy.goToCondorcet({
                urlParams: {
                    allowEveryone: true,
                },
                threadVotes: threadVotesModel.get({
                    options: [PLAYER_NAME2, PLAYER_NAME3],
                    voterMetadata: {
                        [PLAYER_NAME]: { ranking: [[PLAYER_NAME2]] },
                    },
                }),

                condorcetSubmitRequest: requestResponse => {
                    const { body } = requestResponse.request;
                    expect(body).to.deep.equal({ [PLAYER_NAME]: [[PLAYER_NAME2]] });
                },
            });
        });

        it('Will ignore votes from a non option if the param is off', () => {
            cy.goToCondorcet({
                urlParams: {
                    allowEveryone: false,
                },
                threadVotes: threadVotesModel.get({
                    options: [PLAYER_NAME2, PLAYER_NAME3],
                    voterMetadata: {
                        [PLAYER_NAME]: { ranking: [[PLAYER_NAME2]] },
                    },
                }),

                condorcetSubmitRequest: requestResponse => {
                    const { body } = requestResponse.request;
                    expect(body).to.deep.equal({
                        [PLAYER_NAME2]: [[PLAYER_NAME3], [PLAYER_NAME2]],
                        [PLAYER_NAME3]: [[PLAYER_NAME2], [PLAYER_NAME3]],
                    });
                },
            });
        });

        it('Will allow the submission of skip votes if the param is on', () => {
            cy.goToCondorcet({
                urlParams: {
                    allowSkipVote: true,
                    allowSelfVote: true,
                },
                threadVotes: threadVotesModel.get({
                    options: [PLAYER_NAME2, PLAYER_NAME3, PLAYER_NAME],
                    voterMetadata: {
                        [PLAYER_NAME]: { ranking: [[SKIP_DAY]] },
                    },
                }),

                condorcetSubmitRequest: requestResponse => {
                    const { body } = requestResponse.request;
                    expect(body).to.deep.equal({ [PLAYER_NAME]: [[SKIP_DAY]] });
                },
            });

            condorcet.assertInPersonalList(SKIP_DAY);
        });

        it('Will not allow unspecified options', () => {
            cy.goToCondorcet({
                urlParams: {
                    allowEveryone: true,
                },
                threadVotes: threadVotesModel.get({
                    options: [PLAYER_NAME2],
                    voterMetadata: {
                        [PLAYER_NAME]: { ranking: [[PLAYER_NAME2, 'garbageData']] },
                    },

                    condorcetSubmitRequest: requestResponse => {
                        const { body } = requestResponse.request;
                        expect(body).to.deep.equal({ [PLAYER_NAME]: [[PLAYER_NAME2]] });
                    },
                }),
            });
        });

        it('Will not allow the submission of skip votes if the param is off', () => {
            cy.goToCondorcet({
                urlParams: {
                    allowSkipVote: false,
                    allowSelfVote: true,
                },
                threadVotes: threadVotesModel.get({
                    options: [PLAYER_NAME2, PLAYER_NAME3, PLAYER_NAME],
                    voterMetadata: {
                        [PLAYER_NAME]: { ranking: [[SKIP_DAY]] },
                    },
                }),
                condorcetResponse: [[PLAYER_NAME]],

                condorcetSubmitRequest: requestResponse => {
                    const { body } = requestResponse.request;
                    expect(body).to.deep.equal({ [PLAYER_NAME]: [] });
                },
            });
        });

        it('Will send in everyone if no one has voted', () => {
            cy.goToCondorcet({
                urlParams: {
                    allowEveryone: true,
                },
                threadVotes: threadVotesModel.get({
                    options: [PLAYER_NAME, PLAYER_NAME2, PLAYER_NAME3],
                }),
            });

            condorcet.assertInCurrentRanking(PLAYER_NAME);
            condorcet.assertInCurrentRanking(PLAYER_NAME2);
            condorcet.assertInCurrentRanking(PLAYER_NAME3);
        });
    });
});
