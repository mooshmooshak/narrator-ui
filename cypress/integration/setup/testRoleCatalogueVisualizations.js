import {
    COLOR, FACTION_NAME, ROLE_NAME,
} from '../../fixtures/fakeConstants';

import factionModel from '../../fixtures/baseModels/factionModel';
import factionRoleModel from '../../fixtures/baseModels/factionRoleModel';
import setupModel from '../../fixtures/baseModels/setupModel';

import factionRolesUL from '../../pages/setup/factionRolesUL';
import factionUL from '../../pages/setup/factionUL';


function setupData(){
    return {
        setup: setupModel.get({
            factions: [factionModel.get({
                factionRoles: [factionRoleModel.get()],
            })],
        }),
    };
}

describe('Role Catalogue Visualizations', () => {
    context('Interacting with Faction UL', () => {
        it('Will show the catalogue with the proper colors', () => {
            cy.goToSetup(setupData());

            // one for the faction, one for the randoms
            factionRolesUL.assertColor(COLOR);
        });

        it('Will show faction role information on click after clicking a faction', () => {
            cy.goToSetup(setupData());
            factionUL.click(FACTION_NAME);

            factionRolesUL.click(ROLE_NAME);

            factionRolesUL.assertSize(1);
        });
    });
});
