import {
    COLOR, HIDDEN_ID2,
    HIDDEN_NAME,
    ROLE_NAME,
} from '../../fixtures/fakeConstants';

import factionModel from '../../fixtures/baseModels/factionModel';
import factionRoleModel from '../../fixtures/baseModels/factionRoleModel';
import hiddenModel from '../../fixtures/baseModels/hiddenModel';
import hiddenSpawnModel from '../../fixtures/baseModels/hiddenSpawnModel';
import setupModel from '../../fixtures/baseModels/setupModel';

import userStateSetupResponse from '../../fixtures/userStateSetupReponse';

import factionRolesUL from '../../pages/setup/factionRolesUL';
import factionUL from '../../pages/setup/factionUL';


describe('Hidden Visualizations', () => {
    const newFactions = {
        factions: {
            factionNames: [],
        },
        setup: setupModel.get({
            factions: [factionModel.get({
                factionRoles: [factionRoleModel.get()],
            })],
            hiddens: [hiddenModel.get({
                name: HIDDEN_NAME,
                spawns: [hiddenSpawnModel.get()],
            }), hiddenModel.get({
                id: HIDDEN_ID2,
                name: ROLE_NAME,
                spawns: [hiddenSpawnModel.get()],
            })],
        }),
    };

    context('Hiddens List Manipulation', () => {
        it('Shows factions in pane', () => {
            // NO GIVEN

            cy.goToSetup(newFactions);

            // one for the faction, one for the randoms
            factionUL.assertSize(2);
        });

        it('Should show the setup\'s hiddens when "Randoms" are clicked', () => {
            cy.goToSetup(newFactions);

            factionUL.clickHiddens();

            factionRolesUL.assertSize(1);
            factionRolesUL.assertColor(COLOR);
        });

        it('Should not show "Hidden Singles"', () => {
            cy.goToSetup(newFactions);

            factionUL.clickHiddens();

            factionRolesUL.assertSize(1);
        });

        it('Should keep hiddens list selected after deselecting a single hidden', () => {
            cy.goToSetup(newFactions);

            factionUL.clickHiddens();

            factionRolesUL.click(HIDDEN_NAME);
            factionRolesUL.click(HIDDEN_NAME);

            // i don't really care if this is active or background
            factionUL.assertBackgroundFaction('Randoms');
        });
    });

    context('On upstream setup change', () => {
        it('Stays on the "Randoms" tab', () => {
            cy.goToSetup(newFactions);
            const guiUpdate = {
                factions: {},
                guiUpdate: true,
                type: ['rules'],
                rules: userStateSetupResponse.get().rules,
            };
            factionUL.clickHiddens();

            cy.window().then(async window => {
                await window.handleObject(guiUpdate);

                cy.wait(2000);
                factionRolesUL.assertSize(1);
            });
        });
    });
});
