import setupToolbar from '../../pages/setup/setupToolbar';

import setupModel from '../../fixtures/baseModels/setupModel';


describe('Setup Header Visualizations', () => {
    context('Cloning setup', () => {
        it('Will clone a setup', () => {
            const setup = setupModel.get({ isEditable: false });
            cy.goToSetup({ setup });

            setupToolbar.customizeSetup();

            // thens in when
        });

        it('Will not show clone if the user is not moderator', () => {
            const users = []; // no moderators in game

            cy.goToSetup({ users });

            setupToolbar.assertNoCustomizeButton();
        });

        it('Will not show clone if the setup is editable', () => {
            const setup = setupModel.get({ isEditable: true });

            cy.goToSetup({ setup });

            setupToolbar.assertNoCustomizeButton();
        });
    });
});
