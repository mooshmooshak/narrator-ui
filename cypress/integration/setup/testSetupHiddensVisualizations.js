import { MAX_PLAYER_COUNT } from '../../../src/js/util/constants';

import {
    COLOR,
    FACTION_ID,
    FACTION_NAME2, FACTION_ROLE_ID, FACTION_ROLE_ID2,
    HIDDEN_NAME,
    ROLE_NAME,
    ROLE_NAME2, SETUP_HIDDEN_ID,
} from '../../fixtures/fakeConstants';

import abilityModel from '../../fixtures/baseModels/abilityModel';
import factionModel from '../../fixtures/baseModels/factionModel';
import factionRoleModel from '../../fixtures/baseModels/factionRoleModel';
import hiddenModel from '../../fixtures/baseModels/hiddenModel';
import hiddenSpawnModel from '../../fixtures/baseModels/hiddenSpawnModel';
import roleModel from '../../fixtures/baseModels/roleModel';
import setupHiddenModel from '../../fixtures/baseModels/setupHiddenModel';
import setupModel from '../../fixtures/baseModels/setupModel';

import playerAddedEvent from '../../fixtures/events/playerAddedEvent';
import setupHiddenAddEvent from '../../fixtures/events/setupHiddenAddEvent';
import setupHiddenRemoveEvent from '../../fixtures/events/setupHiddenRemoveEvent';

import factionOverview from '../../pages/setup/factionOverview';
import factionRolesUL from '../../pages/setup/factionRolesUL';
import factionUL from '../../pages/setup/factionUL';
import roleOverview from '../../pages/setup/roleOverview';
import setupHiddenUL from '../../pages/setup/setupHiddenUL';
import playerExitEvent from '../../fixtures/events/playerExitEvent';
import playerModel from '../../fixtures/baseModels/playerModel';


function getSetupData(){
    return {
        setup: setupModel.get({
            factions: [
                factionModel.get({
                    factionRoles: [factionRoleModel.get()],
                }),
            ],
            roles: [
                roleModel.get({
                    abilities: [abilityModel.get()],
                }),
            ],
        }),
    };
}

describe('Setup Hidden Visualizations', () => {
    context('Setup hidden viewing', () => {
        it('Will not show setup hiddens that are below the minPlayerCount', () => {
            const setupData = getSetupData();
            setupData.setup.hiddens.push(hiddenModel.get());
            setupData.setup.setupHiddens.push(setupHiddenModel.get({ minPlayerCount: 2 }));
            cy.goToSetup(setupData);

            setupHiddenUL.assertSize(0);
        });

        it('Will not show setup hiddens that are above the maxPlayerCount', () => {
            const setupDatea = getSetupData();
            setupDatea.setup.hiddens.push(hiddenModel.get());
            setupDatea.setup.setupHiddens.push(setupHiddenModel.get({ maxPlayerCount: 0 }));
            cy.goToSetup(setupDatea);

            setupHiddenUL.assertSize(0);
        });
    });

    context('Setup Hidden clicking', () => {
        it('Will show role information on click', () => {
            const setupData = getSetupData();
            setupData.setup.factions.push(factionModel.get2({
                factionRoles: [factionRoleModel.get2()],
            }));
            setupData.setup.hiddens = [hiddenModel.get({
                name: ROLE_NAME2,
                spawns: [hiddenSpawnModel.get2()],
            })];
            setupData.setup.setupHiddens.push(setupHiddenModel.get());
            cy.goToSetup(setupData);
            factionRolesUL.click(ROLE_NAME);

            setupHiddenUL.click(ROLE_NAME2);

            factionRolesUL.assertFocusedText(ROLE_NAME2);
            factionUL.assertBackgroundFaction(FACTION_NAME2);
        });

        it('Will show hidden information on click', () => {
            const setupData = getSetupData();
            setupData.setup.factions[0].factionRoles.push(factionRoleModel.get2({
                factionID: FACTION_ID,
            }));
            setupData.setup.hiddens = [hiddenModel.get({
                spawns: [hiddenSpawnModel.get(), hiddenSpawnModel.get2()],
            })];
            setupData.setup.setupHiddens.push(setupHiddenModel.get());
            cy.goToSetup(setupData);

            setupHiddenUL.click(HIDDEN_NAME);

            factionRolesUL.assertFocusedText(HIDDEN_NAME);
            factionUL.assertHiddenBackgroundSelected();
            factionOverview.assertInvisible();
        });
    });

    context('RoleList Adding', () => {
        it('Will add a base role with no created hidden to roles list', () => {
            cy.goToSetup(getSetupData());

            factionRolesUL.addUncachedRole(ROLE_NAME);

            setupHiddenUL.assertSize(1);
            setupHiddenUL.assertColor(COLOR);
            setupHiddenUL.assertText(HIDDEN_NAME);

            factionUL.clickHiddens();

            roleOverview.assertVisible();
        });

        it('Will add cached setupHidden to roles list', () => {
            const cachedSetupHiddens = getSetupData();
            cachedSetupHiddens.setup.hiddens = [hiddenModel.get({
                name: ROLE_NAME,
                spawns: [hiddenSpawnModel.get()],
            })];
            cy.goToSetup(cachedSetupHiddens);

            factionRolesUL.addCachedRole(ROLE_NAME, {
                onRequest: request => {
                    const { body } = request.request;
                    expect(body.minPlayerCount).to.equal(0);
                    expect(body.maxPlayerCount).to.equal(MAX_PLAYER_COUNT);
                    expect(body.isExposed).to.equal(false);
                    expect(body.mustSpawn).to.equal(false);
                },
            });

            setupHiddenUL.assertSize(1);
        });

        it('Will add a nonsingle setupHidden to roles list', () => {
            const cachedSetupHiddens = getSetupData();
            cachedSetupHiddens.setup.hiddens = [hiddenModel.get({
                factionRoleIDs: [FACTION_ROLE_ID, FACTION_ROLE_ID2],
            })];
            cachedSetupHiddens.setup.factions[0].factionRoles.push(factionRoleModel.get2());
            cy.goToSetup(cachedSetupHiddens);

            factionRolesUL.addHidden(HIDDEN_NAME);

            setupHiddenUL.assertSize(1);
        });
    });

    context('RoleList Removing', () => {
        it('Will delete a setup hidden', () => {
            const setupHiddenData = getSetupData();
            setupHiddenData.setup.hiddens.push(hiddenModel.get());
            setupHiddenData.setup.setupHiddens.push(setupHiddenModel.get());
            cy.goToSetup(setupHiddenData);

            setupHiddenUL.removeSetupHidden(HIDDEN_NAME);

            setupHiddenUL.assertSize(0);
        });
    });

    context('On a setup hidden add event', () => {
        it('Will show on the UL', () => {
            const setupHiddenAddEventObj = setupHiddenAddEvent.get();
            cy.goToSetup({
                setup: setupModel.get({
                    factions: [factionModel.get({
                        factionRoles: [factionRoleModel.get()],
                    })],
                    hiddens: [hiddenModel.get()],
                    setupHiddens: [],
                }),
            });

            cy.window().then(async window => {
                await window.handleObject(setupHiddenAddEventObj);
                cy.wait(2000);

                setupHiddenUL.assertSize(1);
            });
        });

        it('Will not show more than one setup hidden with the same id', () => {
            const setupHiddenAddEventObj = setupHiddenAddEvent
                .get({ setupHiddenID: SETUP_HIDDEN_ID });
            cy.goToSetup({
                setup: setupModel.get({
                    factions: [factionModel.get({
                        factionRoles: [factionRoleModel.get()],
                    })],
                    hiddens: [hiddenModel.get()],
                    setupHiddens: [
                        setupHiddenModel.get(setupHiddenModel.get()),
                    ],
                }),
            });

            cy.window().then(async window => {
                await window.handleObject(setupHiddenAddEventObj);
                cy.wait(2000);

                setupHiddenUL.assertSize(1);
            });
        });
    });

    context('On a setup hidden remove event', () => {
        it('Will show on the UL', () => {
            const setupHiddenAddEventObj = setupHiddenRemoveEvent.get();
            cy.goToSetup({
                setup: setupModel.get({
                    factions: [factionModel.get({
                        factionRoles: [factionRoleModel.get()],
                    })],
                    hiddens: [hiddenModel.get()],
                    setupHiddens: [setupHiddenModel.get()],
                }),
            });

            cy.window().then(async window => {
                await window.handleObject(setupHiddenAddEventObj);
                cy.wait(2000);

                setupHiddenUL.assertSize(0);
            });
        });
    });

    context('On a new player event', () => {
        it('Should refresh the setup hiddens page', () => {
            const newSetup = setupModel.get({
                factions: [factionModel.get({
                    factionRoles: [factionRoleModel.get()],
                })],
                hiddens: [hiddenModel.get()],
                setupHiddens: [
                    setupHiddenModel.get(),
                ],
            });
            const playerAddedEventObj = playerAddedEvent.get({
                setup: newSetup,
            });
            cy.goToSetup({
                setup: setupModel.get({
                    setupHiddens: [],
                }),
            });

            cy.window().then(async window => {
                await window.handleObject(playerAddedEventObj);
                cy.wait(2000);

                setupHiddenUL.assertSize(1);
            });
        });

        // eslint-disable-next-line max-len
        it('Should refresh setup hiddens on player leave when falling below max player count', () => {
            const newSetup = setupModel.get({
                factions: [factionModel.get({
                    factionRoles: [factionRoleModel.get()],
                })],
                hiddens: [hiddenModel.get()],
                setupHiddens: [
                    setupHiddenModel.get({ maxPlayerCount: 1 }),
                ],
            });
            const player2 = playerModel.get2();
            const playerExitEventObj = playerExitEvent.get({
                playerName: player2.name,
                setup: newSetup,
            });
            cy.goToSetup({
                setup: newSetup,
            });

            cy.window().then(async window => {
                await window.handleObject(playerExitEventObj);
                cy.wait(2000);

                setupHiddenUL.assertSize(1);
            });
        });

        it('Should refresh setup hiddens on player add when rising above min player count', () => {
            const newSetup = setupModel.get({
                factions: [factionModel.get({
                    factionRoles: [factionRoleModel.get()],
                })],
                hiddens: [hiddenModel.get()],
                setupHiddens: [
                    setupHiddenModel.get({ minPlayerCount: 2 }),
                ],
            });
            const playerAddedEventObj = playerAddedEvent.get({
                setup: newSetup,
            });
            cy.goToSetup({
                setup: newSetup,
            });

            cy.window().then(async window => {
                await window.handleObject(playerAddedEventObj);
                cy.wait(2000);

                setupHiddenUL.assertSize(1);
            });
        });
    });
});
