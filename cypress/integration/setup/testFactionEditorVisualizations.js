import {
    ABILITY_NAME,
    COLOR, COLOR2, FACTION_ID,
    FACTION_NAME, FACTION_NAME2, ROLE_NAME, ROLE_NAME2, SETUP_ID,
} from '../../fixtures/fakeConstants';

import abilityModel from '../../fixtures/baseModels/abilityModel';
import factionModel from '../../fixtures/baseModels/factionModel';
import factionRoleModel from '../../fixtures/baseModels/factionRoleModel';
import hiddenModel from '../../fixtures/baseModels/hiddenModel';
import hiddenSpawnModel from '../../fixtures/baseModels/hiddenSpawnModel';
import roleModel from '../../fixtures/baseModels/roleModel';
import setupModel from '../../fixtures/baseModels/setupModel';
import setupHiddenModel from '../../fixtures/baseModels/setupHiddenModel';

import factionEditor from '../../pages/setup/factionEditor';
import factionUL from '../../pages/setup/factionUL';
import setupHiddenUL from '../../pages/setup/setupHiddenUL';
import setupContainer from '../../pages/setup/setupContainer';


function getSetupData(){
    return {
        setup: setupModel.get({
            factions: [factionModel.get({
                modifiers: [{
                    name: 'WIN_PRIORITY',
                    value: 0,
                }],
                factionRoles: [factionRoleModel.get()],
            })],
        }),
    };
}

describe('Faction Editor Visualizations', () => {
    context('Opening editor', () => {
        it('Will open the faction editor', () => {
            cy.goToSetup(getSetupData());

            factionEditor.show();

            factionEditor.assertVisible();
        });
    });

    context('Nav buttons', () => {
        it('Go to abilities tab', () => {
            cy.goToSetup(getSetupData());
            factionEditor.show();

            factionEditor.clickAbilitiesNav();

            factionEditor.assertOnAbilities();
        });

        it('Will not go to abilities tab if no faction created', () => {
            cy.goToSetup(getSetupData());
            setupContainer.turnOnFullCustomizability();
            factionUL.clickNewFaction();

            factionEditor.clickAbilitiesNav();

            factionEditor.assertOnDetails();
        });
    });

    context('Viewing faction details', () => {
        it('Will populate general info', () => {
            cy.goToSetup(getSetupData());

            factionEditor.show();

            factionEditor.assertFactionName(FACTION_NAME);
        });

        it('Will populate roles', () => {
            const { setup } = getSetupData();
            setup.roles.push(roleModel.get2());
            cy.goToSetup({ setup });
            factionEditor.show();

            factionEditor.clickNext();

            factionEditor.assertOnRoles();
            factionEditor.assertContainsFactionRole(ROLE_NAME);
            factionEditor.assertSpawnableColor(COLOR);
            factionEditor.assertNotContainsFactionRole(ROLE_NAME2);
        });

        it('Will populate abilities', () => {
            const { setup } = getSetupData();
            setup.factions[0].abilities.push(abilityModel.get());
            cy.goToSetup({ setup });
            factionEditor.show(setup.factions[0].name);
            factionEditor.clickNext(); // general to roles
            factionEditor.clickNext(); // roles to allies

            factionEditor.clickNext(); // allies to abilities

            factionEditor.assertOnAbilities();
            factionEditor.assertSharedAbility(ABILITY_NAME);
        });
    });

    context('Editing faction details', () => {
        it('Will edit the name of a faction', () => {
            const { setup } = getSetupData();
            cy.goToSetup({ setup });
            setupContainer.turnOnFullCustomizability();
            factionEditor.show();

            factionEditor.assertColorNotEditable();

            factionEditor.editFactionName(FACTION_NAME2, {
                onRequest: req => {
                    expect(req.xhr.url).to.include(`factions/${FACTION_ID}`);
                    expect(req.request.body.name).to.be.equal(FACTION_NAME2);
                },
            });

            factionEditor.assertFactionName(FACTION_NAME2);
            factionUL.assertTeamDoesNotExist(FACTION_NAME);
            factionUL.assertBackgroundFaction(FACTION_NAME2);
        });

        it('Will handle edit name errors from enter', () => {
            const { setup } = getSetupData();
            cy.goToSetup({ setup });
            setupContainer.turnOnFullCustomizability();
            factionEditor.show();

            factionEditor.editFactionName(FACTION_NAME2, {
                response: { errors: ['errors'] },
                statusCode: 422,
            });

            factionEditor.assertFactionName(FACTION_NAME2);
            factionUL.assertTeamDoesNotExist(FACTION_NAME2);
            factionUL.assertBackgroundFaction(FACTION_NAME);
        });
    });

    context('Editing spawnable roles', () => {
        it('Will remove a spawnable role', () => {
            const { setup } = getSetupData();
            setup.roles.push(roleModel.get());
            cy.goToSetup({ setup });
            setupContainer.turnOnFullCustomizability();
            factionEditor.show();
            factionEditor.clickNext();

            factionEditor.deleteFactionRole(ROLE_NAME);

            factionEditor.assertNotContainsFactionRole(ROLE_NAME);
        });

        it('Will add a spawnable role', () => {
            const { setup } = getSetupData();
            setup.roles.push(roleModel.get());
            setup.factions[0].factionRoles.length = 0;
            cy.goToSetup({ setup });
            setupContainer.turnOnFullCustomizability();
            factionEditor.show();
            factionEditor.clickNext();

            factionEditor.addSpawnable(ROLE_NAME);

            factionEditor.assertContainsFactionRole(ROLE_NAME);
        });
    });

    context('Editing allies/enemies', () => {
        it('Will move a faction from the allies to the enemies', () => {
            const { setup } = getSetupData();
            setup.factions[0].enemyIDs = [];
            cy.goToSetup({ setup });
            setupContainer.turnOnFullCustomizability();
            factionEditor.show(setup.factions[0].name);
            factionEditor.clickNext();
            factionEditor.clickNext();

            factionEditor.addEnemy(FACTION_NAME, {
                onRequest: req => {
                    expect(req.xhr.url).to.include(`factions/${FACTION_ID}/enemies/${FACTION_ID}`);
                },
            });

            factionEditor.assertEnemy(FACTION_NAME);
        });

        it('Will move a faction from the enemies to the allies', () => {
            const { setup } = getSetupData();
            const faction = setup.factions[0];
            faction.enemyIDs = [faction.id];
            cy.goToSetup({ setup });
            setupContainer.turnOnFullCustomizability();
            factionEditor.show(faction.name);
            factionEditor.clickNext();
            factionEditor.clickNext();

            factionEditor.addAlly(faction.name, {
                onRequest: req => {
                    expect(req.xhr.url).to.include(`factions/${FACTION_ID}/enemies/${FACTION_ID}`);
                },
            });

            factionEditor.assertAlly(FACTION_NAME);
        });
    });

    context('Editing abilities', () => {
        it('Will add an abilitiy', () => {
            const { setup } = getSetupData();
            setup.factions[0].abilities = [];
            cy.goToSetup({ setup });
            setupContainer.turnOnFullCustomizability();
            factionEditor.show(setup.factions[0].name);
            factionEditor.clickNext();
            factionEditor.clickNext();
            factionEditor.clickNext();

            factionEditor.addAbility(ABILITY_NAME, {
                onRequest: req => {
                    expect(req.request.body.ability).to.be.equal(ABILITY_NAME);
                },
            });

            factionEditor.assertHasAbility(ABILITY_NAME);
        });

        it('Will delete a faction ability', () => {
            const { setup } = getSetupData();
            const faction = setup.factions[0];
            const ability = abilityModel.get();
            faction.abilities = [ability];
            cy.goToSetup({ setup });
            setupContainer.turnOnFullCustomizability();
            factionEditor.show(faction.name);
            factionEditor.clickNext();
            factionEditor.clickNext();
            factionEditor.clickNext();

            factionEditor.removeAbility(ability.name);

            factionEditor.assertNoAbility(ability.name);
        });
    });

    context('Closing editor', () => {
        it('Will close the faction editor on esc press', () => {
            cy.goToSetup(getSetupData());
            factionEditor.show();

            factionEditor.hideByEsc();

            factionEditor.assertHidden();
        });

        it('Will close the faction editor on background click', () => {
            const { setup } = getSetupData();
            cy.goToSetup({ setup });
            factionEditor.show(setup.factions[0].name);

            factionEditor.hideByBackgroundClick();

            factionEditor.assertHidden();
        });
    });

    context('Creating factions', () => {
        it('Will submit a request to create a faction', () => {
            cy.goToSetup();
            setupContainer.turnOnFullCustomizability();
            factionUL.clickNewFaction();

            factionEditor.assertButtonDisabled();
            factionEditor.assertNoDeleteButton();

            factionEditor.setFactionName(FACTION_NAME);
            factionEditor.setFactionColor(COLOR);
            factionEditor.clickAndCreate({
                onRequest: request => {
                    expect(request.request.body).to.deep.equal({
                        name: FACTION_NAME, color: COLOR, description: '', setupID: SETUP_ID,
                    });
                },
            });

            factionEditor.assertOnRoles();
            factionUL.assertSize(1);
        });


        it('Will handle create fails', () => {
            cy.goToSetup();
            setupContainer.turnOnFullCustomizability();
            factionUL.clickNewFaction();

            factionEditor.setFactionName(FACTION_NAME);
            factionEditor.setFactionColor(COLOR);
            factionEditor.clickAndCreate({
                response: { errors: ['errors'] },
                statusCode: 422,
            });

            factionEditor.assertOnDetails();
            factionEditor.assertButtonText('Create');
        });
    });

    context('Deleting factions', () => {
        it('Will close the editor after a successful delete', () => {
            cy.goToSetup(getSetupData());
            setupContainer.turnOnFullCustomizability();
            factionEditor.show(FACTION_NAME);

            factionEditor.deleteFaction();

            factionEditor.assertHidden();
            factionUL.assertNoActiveFactions();
            factionUL.assertTeamDoesNotExist(FACTION_NAME);
        });

        it('Will change the color of multi-faction setup hiddens', () => {
            const data = getSetupData();
            data.setup.factions.push(factionModel.get2({
                factionRoles: [factionRoleModel.get2()],
                modifiers: [{
                    name: 'WIN_PRIORITY',
                    value: 0,
                }],
            }));
            data.setup.setupHiddens = [setupHiddenModel.get()];
            data.setup.hiddens = [hiddenModel.get({
                spawns: [hiddenSpawnModel.get(), hiddenSpawnModel.get2()],
            })];
            cy.goToSetup(data);
            setupContainer.turnOnFullCustomizability();
            factionEditor.show(FACTION_NAME);

            factionEditor.deleteFaction();

            setupHiddenUL.assertColor(COLOR2);
        });

        it('Will handle viewing other factions after deleting current faction', () => {
            const { setup } = getSetupData();
            const faction2 = factionModel.get2();
            faction2.enemyIDs.push(setup.factions[0].id);
            setup.factions.push(faction2);
            cy.goToSetup({ setup });
            setupContainer.turnOnFullCustomizability();
            factionEditor.show(FACTION_NAME);

            factionEditor.deleteFaction();

            factionUL.click(FACTION_NAME2);
            factionUL.assertActiveFaction(FACTION_NAME2);
        });
    });
});
