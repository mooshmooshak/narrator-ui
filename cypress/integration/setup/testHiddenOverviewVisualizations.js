import {
    COLOR, FACTION_ROLE_ID,
    HIDDEN_ID,
    HIDDEN_NAME,
    ROLE_NAME,
} from '../../fixtures/fakeConstants';

import factionModel from '../../fixtures/baseModels/factionModel';
import factionRoleModel from '../../fixtures/baseModels/factionRoleModel';
import hiddenModel from '../../fixtures/baseModels/hiddenModel';
import setupHiddenModel from '../../fixtures/baseModels/setupHiddenModel';
import setupModel from '../../fixtures/baseModels/setupModel';

import factionRolesUL from '../../pages/setup/factionRolesUL';
import factionUL from '../../pages/setup/factionUL';
import roleOverview from '../../pages/setup/roleOverview';


function getSetupData(){
    return {
        setup: setupModel.get({
            factions: [factionModel.get({
                factionRoles: [factionRoleModel.get()],
            })],
            hiddens: [hiddenModel.get({
                name: HIDDEN_NAME,
                factionRoleIDs: [FACTION_ROLE_ID],
            })],
            setupHiddens: [setupHiddenModel.get({
                hiddenID: HIDDEN_ID,
            })],
        }),
    };
}

describe('Role Details Visualizations', () => {
    context('Viewing hidden details', () => {
        it('Will show the roles associated with a hidden', () => {
            const setupData = getSetupData();
            cy.goToSetup(setupData);
            factionUL.clickHiddens();

            factionRolesUL.click(HIDDEN_NAME);

            roleOverview.assertVisible();
            roleOverview.assertHeader(HIDDEN_NAME);
            roleOverview.assertHeaderColor(COLOR);
            roleOverview.assertSpawnable(ROLE_NAME);
        });
    });
});
