import playerModel from '../../fixtures/baseModels/playerModel';
import profileModel from '../../fixtures/baseModels/profileModel';
import userModel from '../../fixtures/baseModels/userModel';

import hostChangeEvent from '../../fixtures/events/hostChangeEvent';
import playerAddedEvent from '../../fixtures/events/playerAddedEvent';
import playerExitEvent from '../../fixtures/events/playerExitEvent';
import playerNameUpdateEvent from '../../fixtures/events/playerNameUpdateEvent';
import playerStatusChangeEvent from '../../fixtures/events/playerStatusChangeEvent';

import gameDetails from '../../pages/lobby/gameDetails';
import participantsPage from '../../pages/lobby/participantsPage';

import {
    GAME_ID,
    PLAYER_NAME, PLAYER_NAME2, PLAYER_NAME3, USER_COLOR, USER_ICON2, USER_ID, USER_ID2,
} from '../../fixtures/fakeConstants';


describe('Participants Container', () => {
    context('On load', () => {
        it('Should show the users as active', () => {
            cy.goToLobby({
                activeUserIDs: [USER_ID, USER_ID2],
                players: [
                    playerModel.get(),
                    playerModel.get2(),
                ],
                users: [
                    userModel.get({ isModerator: true }),
                    userModel.get({ id: USER_ID2 }),
                ],
            });

            participantsPage.click(PLAYER_NAME);
            participantsPage.assertActiveHost(PLAYER_NAME);
            participantsPage.assertColor(PLAYER_NAME, USER_COLOR);

            participantsPage.click(PLAYER_NAME2);
            participantsPage.assertActive(PLAYER_NAME2);
            participantsPage.assertIcon(PLAYER_NAME2, USER_ICON2);
        });

        it('Should show the player count on mobile', () => {
            cy.mobile();
            cy.goToLobby({
                players: [
                    playerModel.get(),
                ],
            });

            participantsPage.assertPlayerHeaderCount(1);
        });
    });

    context('Clicking player ULs', () => {
        it('Should show player details for yourself', () => {
            cy.goToLobby();

            participantsPage.click(PLAYER_NAME);

            participantsPage.assertDetailsVisible(PLAYER_NAME);
        });

        it('Should show player details for others', () => {
            const player2 = playerModel.get2();
            cy.goToLobby({
                players: [playerModel.get(), player2],
            });

            participantsPage.click(player2.name);

            participantsPage.assertDetailsVisible(player2.name);
        });
    });

    context('On new player event', () => {
        it('Should show that a new player was added', () => {
            const playerAddedEventObj = playerAddedEvent.get({
                playerName: PLAYER_NAME2,
                players: [
                    playerModel.get({ name: PLAYER_NAME2 }),
                    playerModel.get2({ name: PLAYER_NAME3 }),
                ],
            });
            const profile = profileModel.getInLobby({ name: PLAYER_NAME });
            cy.goToLobby({ profile });

            cy.window().then(async window => {
                await window.handleObject(playerAddedEventObj);
                cy.wait(2000);

                participantsPage.assertPlayerHeaderCount(3);
                participantsPage.assertNameInUL(PLAYER_NAME);
                participantsPage.assertNameInUL(PLAYER_NAME2);
                participantsPage.assertNameInUL(PLAYER_NAME3);
            });
        });
    });

    context('On exited host event', () => {
        it('Should show that the host left', () => {
            const playerExitEventObject = playerExitEvent.get({
                moderatorIDs: [USER_ID],
                playerName: PLAYER_NAME2,
            });
            cy.goToLobby({
                moderatorIDs: [USER_ID2],
                players: [playerModel.get(), playerModel.get2()],
            });

            participantsPage.click(PLAYER_NAME);

            cy.window().then(async window => {
                await window.handleObject(playerExitEventObject);
                cy.wait(2000);

                participantsPage.assertPlayerHeaderCount(1);
                participantsPage.assertNameNotInUL(PLAYER_NAME2);
                participantsPage.assertHost(PLAYER_NAME);
                gameDetails.assertHostControls();
            });
        });
    });

    context('On host change event', () => {
        it('Should update the player list', () => {
            const hostChangeEventObj = hostChangeEvent.get({
                hostID: USER_ID,
            });
            cy.goToLobby({
                moderatorIDs: [USER_ID2],
                players: [playerModel.get(), playerModel.get2()],
            });

            participantsPage.click(PLAYER_NAME);

            cy.window().then(async window => {
                await window.handleObject(hostChangeEventObj);
                cy.wait(2000);

                participantsPage.assertHost(PLAYER_NAME);
                gameDetails.assertHostControls();
            });
        });
    });

    context('On player name change event', () => {
        it('Will update the list of player names', () => {
            cy.goToLobby();
            const playerNameUpdateEventObj = playerNameUpdateEvent.get();

            cy.window().then(async window => {
                await window.handleObject(playerNameUpdateEventObj);
                cy.wait(2000);

                participantsPage.assertNameInUL(PLAYER_NAME2);
            });
        });
    });

    context('On player status change event', () => {
        it('Should set the player to inactive', () => {
            const player2 = playerModel.get2();
            cy.goToLobby({
                activeUserIDs: [player2.userID],
                players: [playerModel.get(), player2],
            });
            const playerStatusChangeEventObj = playerStatusChangeEvent.get({
                isActive: false,
                userID: USER_ID2,
            });

            participantsPage.click(PLAYER_NAME2);

            cy.window().then(async window => {
                await window.handleObject(playerStatusChangeEventObj);
                cy.wait(2000);

                participantsPage.assertInactive(PLAYER_NAME2);
            });
        });

        it('Should set the player to active', () => {
            const player2 = playerModel.get2();
            cy.goToLobby({
                activeUserIDs: [],
                players: [playerModel.get(), player2],
            });
            const playerStatusChangeEventObj = playerStatusChangeEvent.get({
                isActive: true,
                userID: USER_ID2,
            });

            participantsPage.click(PLAYER_NAME2);

            cy.window().then(async window => {
                await window.handleObject(playerStatusChangeEventObj);
                cy.wait(2000);

                participantsPage.assertActive(PLAYER_NAME2);
            });
        });
    });

    context('Repicking players', () => {
        it('Should submit a host reassignment', () => {
            const player2 = playerModel.get2();
            cy.goToLobby({
                players: [playerModel.get(), player2],
                users: [userModel.get({ isModerator: true }), userModel.get2()],
            });
            participantsPage.click(player2.name);

            participantsPage.clickSetAsHost(player2.name, player2.userID, requestResponse => {
                const { body } = requestResponse.request;
                expect(body.repickTarget).to.be.equal(player2.name);
                expect(body.gameID).to.be.equal(GAME_ID);
            });

            participantsPage.assertDetailsVisible(player2.name);
            // back end needs to say whether repick has happened or not
            // participantsPage.assertNoRepickOption(player2.name);
            participantsPage.assertHost(player2.name);
            gameDetails.assertNoHostControls();
        });

        it('Should show repick for non moderators', () => {
            const hostPlayer = playerModel.get();
            cy.goToLobby({
                players: [hostPlayer, playerModel.get2()],
                users: [
                    userModel.get({ isModerator: true }),
                    userModel.get({ isModerator: false }),
                ],
            }, USER_ID2);

            participantsPage.click(hostPlayer.name);

            participantsPage.assertRepickOption(hostPlayer.name);
        });
    });

    context('Kicking players', () => {
        it('Should remove the user from the game', () => {
            const player2 = playerModel.get2();
            cy.goToLobby({
                players: [playerModel.get(), player2],
                users: [userModel.get({ isModerator: true }), userModel.get2()],
            });
            participantsPage.click(player2.name);

            participantsPage.clickKickUser(player2.userID);

            participantsPage.assertNameNotInUL(player2.name);
        });
    });

    context('On exiting player event', () => {
        it('Should hide the player overview', () => {
            const player2 = playerModel.get2();
            cy.goToLobby({
                players: [playerModel.get(), player2],
            });
            participantsPage.click(player2.name);
            const playerExitEventObj = playerExitEvent.get({
                playerName: player2.name,
            });

            cy.window().then(async window => {
                await window.handleObject(playerExitEventObj);
                cy.wait(2000);

                participantsPage.assertPlayerHeaderCount(1);
                participantsPage.assertNameNotInUL(player2.name);
            });
        });
    });
});
