import ticker from '../../pages/lobby/ticker';

import { USER_COLOR, USER_ICON } from '../../fixtures/fakeConstants';

import messageModel from '../../fixtures/baseModels/messageModel';


describe('Lobby Ticker Visualizations', () => {
    beforeEach(() => {
        cy.mobile();
    });

    context('When lobby messages are present', () => {
        it('It will show new messages in the ticker', () => {
            const lobbyChatMessages = [messageModel.getLobby()];

            cy.goToLobby({ lobbyChatMessages });

            ticker.assertChatIcon(USER_ICON);
            ticker.assertChatIconColor(USER_COLOR);
        });
    });
});
