import { MAX_PLAYER_COUNT, VOTE_SYSTEMS } from '../../../src/js/util/constants';

import { GAME_ID, SETUP_ID, SETUP_ID2 } from '../../fixtures/fakeConstants';

const gameModifier = require('../../fixtures/baseModels/gameModifierModel');
const setupFeaturedModel = require('../../fixtures/baseModels/setupFeaturedModel');
const setupModel = require('../../fixtures/baseModels/setupModel');
const setupModifier = require('../../fixtures/baseModels/setupModifierModel');

const gameDetails = require('../../pages/lobby/gameDetails');
const toasts = require('../../pages/toasts');


describe('Game Details Visualization', () => {
    context('Manipulating general settings', () => {
        it('Will update the day length by seconds', () => {
            cy.goToLobby({
                gameModifiers: {
                    DAY_LENGTH_START: gameModifier.get({
                        name: 'DAY_LENGTH_START',
                        value: 60,
                    }),
                },
            });

            gameDetails.openEditor();
            gameDetails.showAdvancedPhaseSettings();
            gameDetails.setDayLengthInput(120, requestResponse => {
                const { body } = requestResponse.request;
                expect(body.value).to.be.equal(120);
            });

            gameDetails.assertDayLength(120);
        });

        it('Will update the phase duration dropdown', () => {
            cy.goToLobby({
                gameModifiers: {
                    NIGHT_LENGTH: gameModifier.get({
                        name: 'NIGHT_LENGTH',
                        value: 60,
                    }),
                },
            });

            gameDetails.openEditor();
            gameDetails.showAdvancedPhaseSettings();
            gameDetails.setNightLengthDropdown(60 * 60 * 60, 'hr', requestResponse => {
                const { body } = requestResponse.request;
                expect(body.value).to.be.equal(60 * 60 * 60);
            });
        });

        it('Will update the vote system', () => {
            const [voteSystem1, voteSystem2] = VOTE_SYSTEMS;
            cy.goToLobby({
                gameModifiers: {
                    name: 'VOTE_SYSTEM',
                    value: voteSystem1.value,
                },
            });

            gameDetails.openEditor();
            gameDetails.setVoteSystem(voteSystem2);

            gameDetails.assertVoteSystem(voteSystem2.label);
        });

        it('Will update day/night start', () => {
            cy.goToLobby({
                setup: setupModel.get({
                    modifiers: [{ name: 'DAY_START', value: false }],
                }),
            });

            gameDetails.openEditor();
            gameDetails.turnOnDayStart({
                onRequest: requestResponse => {
                    const { body } = requestResponse.request;
                    expect(body.value).to.be.equal(true);
                    expect(body.minPlayerCount).to.be.equal(0);
                    expect(body.maxPlayerCount).to.be.equal(MAX_PLAYER_COUNT);
                },
            });

            gameDetails.assertDayStart(true);
        });

        it('Will prompt to clone frozen setup upon phase start manipulation', () => {
            cy.goToLobby({
                setup: setupModel.get({
                    isEditable: false,
                    setupModifiers: [setupModifier.get({ value: false })],
                }),
            });

            gameDetails.openEditor();
            gameDetails.setDayStart();

            toasts.assertTitle('Cannot edit frozen setup.');
        });

        it('Will not prompt to clone frozen when editing chat roles', () => {
            cy.goToLobby({
                gameModifiers: {
                    name: 'CHAT_ROLES',
                    value: true,
                },
                setup: setupModel.get({
                    isEditable: false,
                }),
            });

            gameDetails.openEditor();
            gameDetails.showAdvancedMiscSettings();
            gameDetails.turnOffChatRoles();

            gameDetails.assertFormat('In person');
        });
    });

    context('Auto Setup Manipulation', () => {
        it('Will change featured setups', () => {
            const newSetupName = 'Featured Setup';
            const featuredSetup1 = setupFeaturedModel.get({
                id: SETUP_ID,
                name: 'Other Setup',
            });
            const featuredSetup2 = setupFeaturedModel.get({
                name: newSetupName,
                id: SETUP_ID2,
            });
            cy.goToLobby({
                setup: setupModel.get({
                    id: featuredSetup1.id,
                }),
                setupsFeatured: [featuredSetup1, featuredSetup2],
            });

            gameDetails.setSetup(featuredSetup2.name, request => {
                const { gameID, setupID } = request.request.body;
                expect(setupID).to.equal(featuredSetup2.id);
                expect(gameID).to.equal(GAME_ID);
            });
        });
    });
});
