import soundSystem from '../../pages/lobby/soundSystem';


describe('Page Load Test', () => {
    context('On load', () => {
        it('Should play music', () => {
            cy.goToSetup();

            cy.window().then(window => {
                window.soundService.setCypressTesting();
            });

            soundSystem.assertNightMusicOn();
        });
    });
});
