import landingPane from '../../pages/landing/landingPane';
import routesPage from '../../pages/routesPage';

import gameModel from '../../fixtures/baseModels/gameModel';
import firebaseStateModel from '../../fixtures/baseModels/firebaseState';
import setupFeaturedModel from '../../fixtures/baseModels/setupFeaturedModel';


describe('Game entry test', () => {
    const firebaseState = firebaseStateModel.getPreviousProfile();

    context('Joining game', () => {
        it('Will go to setup upon successful join', () => {
            const games = [gameModel.get({ isStarted: false })];
            cy.goToHome({ firebaseState, games });

            landingPane.joinGame();

            routesPage.assertOnLobby();
        });
    });

    context('Creating game', () => {
        it('Will load a featured setup game', () => {
            const featuredSetup = setupFeaturedModel.get();
            cy.goToHome({
                featuredSetups: [featuredSetup], firebaseState,
            });

            landingPane.createGame({
                onRequest: request => {
                    expect(request.request.body.setupID).to.be.eql(featuredSetup.id);
                },
            });

            routesPage.assertOnLobby();
        });
    });
});
