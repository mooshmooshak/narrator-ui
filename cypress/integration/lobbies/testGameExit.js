import kickEvent from '../../fixtures/events/kickEvent';

import routesPage from '../../pages/routesPage';
import toasts from '../../pages/toasts';


describe('Game exit test', () => {
    context('On kick', () => {
        it('Will go to back to home page', () => {
            cy.goToHome();
            const kickObject = kickEvent.get();

            cy.window().then(async window => {
                await window.handleObject(kickObject);

                toasts.assertTextOld('You\'ve been kicked from the lobby!');
                expect(window.socket).to.be.an('null');
                routesPage.assertOnHome();
            });
        });
    });
});
