import setupFeaturedResponse from '../../fixtures/responses/setupFeaturedResponse';

import landingPane from '../../pages/landing/landingPane';
import toasts from '../../pages/toasts';


describe('Page load test', () => {
    context('Page load test', () => {
        it('will show the create game button', () => {
            let fixtureCount = 0;
            cy.goToHome({
                onResponse: () => {
                    fixtureCount++;
                },
            });

            landingPane.assertCreateGameButtonVisible();
            cy.wait(100).then(() => {
                expect(fixtureCount).to.be.eq(1);
            });
        });

        it('Will show the create game modal on click', () => {
            cy.goToHome();

            landingPane.clickCreateGame();

            landingPane.assertCreateGameModalOpen();
        });
    });

    context('Auth errors', () => {
        it('Will show an error when the auth token does not exist', () => {
            const authToken = new Array(128 + 1).join('A');
            cy.server();
            cy.route({
                method: 'GET',
                url: `api/users?auth_token=${authToken}`,
                status: 422,
                response: { errors: ['Auth token not found.'] },
                failOnStatusCode: false,
            });
            cy.route({
                method: 'GET',
                url: 'api/setups',
                response: setupFeaturedResponse.get(),
            });

            cy.visit(`http://localhost:4502/?auth_token=${authToken}`);

            toasts.assertTextOld('Woops! This game link is no longer active.');
        });
    });
});
