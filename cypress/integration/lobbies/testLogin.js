import firebaseStateModel from '../../fixtures/baseModels/firebaseState';

import tabs from '../../pages/landing/tabs';


describe('Login', () => {
    context('Logged in view', () => {
        it('Will show that a user is not logged in', () => {
            const firebaseState = firebaseStateModel.getNoProfile();

            cy.goToHome({ firebaseState });

            tabs.assertLoggedOutNav();
        });

        it('Will show that a user is logged in', () => {
            const firebaseState = firebaseStateModel.getPreviousProfile();

            cy.goToHome({ firebaseState });

            tabs.assertLoggedInNav();
        });
    });

    context('Interacting with log in', () => {
        it('Will log out a user', () => {
            const firebaseState = firebaseStateModel.getPreviousProfile();
            cy.goToHome({ firebaseState });

            tabs.logout();

            tabs.assertLoggedOutNav();
        });
    });
});
