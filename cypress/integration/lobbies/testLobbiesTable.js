const lobbyEvent = require('../../fixtures/events/lobbyEvent');

const gameModel = require('../../fixtures/baseModels/gameModel');

const lobbiesTable = require('../../pages/landing/lobbiesTable');


describe('Lobbies table', () => {
    context('Lobby events', () => {
        it('Will show the new lobby in the table upon event', () => {
            cy.goToHome();
            const newLobbyObject = lobbyEvent.getCreate();

            cy.window().then(async window => {
                await window.handleObject(newLobbyObject);

                lobbiesTable.assertLength(1);
            });
        });

        it('Will remove the deleted lobby upon event', () => {
            const games = [gameModel.get()];
            cy.goToHome({ games });
            const deletedLobbyEventObject = lobbyEvent.getDelete();

            cy.window().then(async window => {
                await window.handleObject(deletedLobbyEventObject);

                lobbiesTable.assertLength(0);
            });
        });
    });
});
