import deprecatedAbilitiesModel from '../../../fixtures/baseModels/deprecatedAbilitiesModel';
import phaseModel from '../../../fixtures/baseModels/phaseModel';
import playerModel from '../../../fixtures/baseModels/playerModel';
import profileModel from '../../../fixtures/baseModels/profileModel';

import actionPane from '../../../pages/gamePlay/actionPane';
import tabs from '../../../pages/gamePlay/tabs';

import { PLAYER_NAME, PLAYER_NAME2 } from '../../../fixtures/fakeConstants';


describe('Test no ability visualisations', () => {
    context('When no available abilities can be used', () => {
        it('Will show the living', () => {
            const players = [
                playerModel.getDead(),
                playerModel.get2(),
            ];
            cy.goToGame({
                abilities: deprecatedAbilitiesModel.getEmpty(),
                players,
                phase: phaseModel.getNight(),
            });

            tabs.clickActions();
            actionPane.hoverLeft(PLAYER_NAME2);

            actionPane.assertHeader('The Living');
            actionPane.assertVisible([PLAYER_NAME2]);
            actionPane.assertNotVisible([PLAYER_NAME]);
            actionPane.assertActionButtonText('Skip to Daytime');
            actionPane.assertRightNotHovered(PLAYER_NAME2);
        });

        it('Will show you are jailed', () => {
            cy.goToGame({
                abilities: deprecatedAbilitiesModel.getEmpty(),
                profile: profileModel.getInGame({
                    isJailed: true,
                }),
                phase: phaseModel.getNight(),
            });

            tabs.clickActions();

            actionPane.assertHeader('You are jailed!');
            actionPane.assertActionButtonText('Skip to Daytime');
        });
    });
});
