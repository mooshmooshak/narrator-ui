import deprecatedAbilitiesModel from '../../../fixtures/baseModels/deprecatedAbilitiesModel';
import phaseModel from '../../../fixtures/baseModels/phaseModel';

import actionPane from '../../../pages/gamePlay/actionPane';
import tabs from '../../../pages/gamePlay/tabs';

import { PLAYER_NAME2 } from '../../../fixtures/fakeConstants';


function getAbilities(){
    return deprecatedAbilitiesModel.get('Compare');
}

describe('Test Parity Check Visualizations', () => {
    context('Submitting parity check action', () => {
        it('Will submit an action', () => {
            cy.goToGame({
                abilities: getAbilities(),
                phase: phaseModel.getNight(),
            });
            tabs.clickActions();

            actionPane.clickLeftSubmit(PLAYER_NAME2, {
                onRequest: request => {
                    const action = request.request.body;
                    expect(action.command).to.equal('compare');
                },
            });

            actionPane.assertHeader('Compare Alignment');
        });

        it('Will cancel an action', () => {
            const actions = {
                actionList: [{
                    command: 'compare',
                    playerNames: [PLAYER_NAME2],
                    text: '',
                }],
            };
            cy.goToGame({
                abilities: getAbilities(),
                actions,
                phase: phaseModel.getNight(),
            });
            tabs.clickActions();

            actionPane.clickLeftUnsubmit(PLAYER_NAME2);

            actionPane.assertNotClicked(PLAYER_NAME2);
        });
    });
});
