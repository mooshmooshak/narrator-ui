import actionPane from '../../../pages/gamePlay/actionPane';
import tabs from '../../../pages/gamePlay/tabs';

import phaseEndBidEvent from '../../../fixtures/events/phaseEndBidEvent';

import phaseModel from '../../../fixtures/baseModels/phaseModel';
import playerModel from '../../../fixtures/baseModels/playerModel';

import { PLAYER_NAME, PLAYER_NAME2 } from '../../../fixtures/fakeConstants';


describe('Test end night visualizations', () => {
    context('On end night submit', () => {
        it('Will show living people if no one submitted an end night action', () => {
            const phase = phaseModel.getNight();
            const players = [playerModel.getDead(), playerModel.get2()];
            cy.goToGame({ phase, players });
            tabs.clickActions();

            actionPane.clickEndNight();

            actionPane.assertHeader('Waiting on');
            actionPane.assertVisible([PLAYER_NAME2]);
            actionPane.assertNotVisible([PLAYER_NAME]);
        });

        it('Will show the people who submitted a bid to end the night', () => {
            const phase = phaseModel.getNight();
            const players = [playerModel.get({
                endedNight: false,
            }), playerModel.get2()];
            cy.goToGame({ phase, players });
            tabs.clickActions();

            actionPane.clickEndNight();

            actionPane.assertHeader('Waiting on');
            actionPane.assertVisible([PLAYER_NAME2]);
            actionPane.assertNotVisible([PLAYER_NAME]);
        });
    });

    context('On other user end night submit', () => {
        it('Will show a change in the ended night people', () => {
            const phase = phaseModel.getNight();
            const players = [playerModel.get({
                endedNight: true,
            }), playerModel.get2()];
            cy.goToGame({ phase, players });
            const phaseEndBidEventObj = phaseEndBidEvent.get({
                players: players.map(player => ({
                    ...player,
                    endedNight: true,
                })),
            });
            tabs.clickActions();

            cy.window().then(async window => {
                await window.handleObject(phaseEndBidEventObj);
                cy.wait(2000);

                actionPane.assertHeader('Waiting on');
                actionPane.assertNotVisible([PLAYER_NAME, PLAYER_NAME2]);
            });
        });
    });
});
