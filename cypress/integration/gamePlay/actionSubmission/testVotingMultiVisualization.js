import tabs from '../../../pages/gamePlay/tabs';
import votePane from '../../../pages/gamePlay/votePane';

import { PLAYER_NAME, PLAYER_NAME2, PLAYER_NAME3 } from '../../../fixtures/fakeConstants';

import actionModel from '../../../fixtures/baseModels/actionModel';
import actionsModel from '../../../fixtures/baseModels/actionsModel';
import gameModifierModel from '../../../fixtures/baseModels/gameModifierModel';
import phaseModel from '../../../fixtures/baseModels/phaseModel';
import playerModel from '../../../fixtures/baseModels/playerModel';
import profileModel from '../../../fixtures/baseModels/profileModel';
import setupModel from '../../../fixtures/baseModels/setupModel';
import setupModifierModel from '../../../fixtures/baseModels/setupModifierModel';
import voteInfoModel from '../../../fixtures/baseModels/voteInfoModel';

import trialStartEvent from '../../../fixtures/events/trialStartEvent';
import voteUpdateEvent from '../../../fixtures/events/voteUpdateEvent';

import { NOT_VOTING, SKIP_DAY } from '../../../../src/js/util/constants';


const VOTE = 'Vote';

const abilities = {
    type: [],
};


describe('Test Multi Voting Visualizations', () => {
    const gameModifiers = {
        VOTE_SYSTEM: gameModifierModel.get({
            name: 'VOTE_SYSTEM',
            value: 3,
        }),
        TRIAL_LENGTH: gameModifierModel.get({
            name: 'TRIAL_LENGTH',
            value: 1,
        }),
    };

    context('Viewing vote pane before trial', () => {
        it('Will show trial metadata at vote phase start', () => {
            cy.mobile();
            const phase = phaseModel.getDay({
                length: 60,
                endTimeOffset: 59,
            });
            const players = [playerModel.get(), playerModel.get2(), playerModel.get3()]
                .map(playerModel.getInGame);
            cy.goToGame({ gameModifiers, phase, players });

            tabs.clickVotePane();

            votePane.assertPreTrialHeader();
            votePane.assertMinVoteSubheader(2);
            votePane.assertNoVoteButton(NOT_VOTING);
            votePane.assertNoVoteCancelPending(NOT_VOTING, players[0].name);
        });

        it('Will not show dead players under "Not Voting"', () => {
            const phase = phaseModel.getDay();
            const [alivePlayer, deadPlayer] = [playerModel.get2(), playerModel.getDead()];
            cy.goToGame({ players: [alivePlayer, deadPlayer], phase });

            tabs.clickVotePane();

            votePane.assertNotVotingFor(deadPlayer.name, NOT_VOTING);
        });

        it('Will show decreased vote minimum', () => {
            const phase = phaseModel.getDay({
                length: 60,
                endTimeOffset: 29,
            });
            const players = [playerModel.get(), playerModel.get2()].map(playerModel.getInGame);
            cy.goToGame({ phase, players });

            tabs.clickVotePane();

            votePane.assertMinVoteSubheader(1);
        });

        it('Will progressively show decreased vote minimum', () => {
            const phase = phaseModel.getDay({
                length: 5,
                endTimeOffset: 5, // starts off at 2, then goes to 1, and then succeeds
            });
            const players = [playerModel.get3(), playerModel.get(), playerModel.get2()]
                .map(playerModel.getInGame);
            cy.goToGame({ phase, players });

            tabs.clickVotePane();

            votePane.assertMinVoteSubheader(1);
        });

        it('Will show vote counts', () => {
            const players = [playerModel.get(), playerModel.get2(), playerModel.get3()]
                .map(playerModel.getInGame);
            const voteInfo = voteInfoModel.get({
                allowedTargets: [PLAYER_NAME, PLAYER_NAME2, SKIP_DAY],
                voterToVotes: {
                    [players[0].name]: [players[1].name],
                },
            });
            cy.goToGame({ players, voteInfo });

            tabs.clickVotePane();
            votePane.clickSortByName();

            votePane.assertVoteCount(players[1].name, players[0].votePower);
            votePane.assertVoteCount(players[0].name, 0);
            votePane.assertVoteCount(SKIP_DAY, 0);
            votePane.assertVoteCount(NOT_VOTING, 2);
        });

        it('Will sort by name', () => {
            const phase = phaseModel.getDay();
            const playerName = `z${PLAYER_NAME}`;
            const players = [
                playerModel.get({ name: playerName }), playerModel.get2(),
                playerModel.get3(),
            ].map(playerModel.getInGame);
            const voteInfo = voteInfoModel.get({
                allowedTargets: [players[0].name, players[1].name, SKIP_DAY],
                voterToVotes: {
                    [players[0].name]: [players[1].name],
                },
            });
            cy.goToGame({ phase, players, voteInfo });

            tabs.clickVotePane();
            votePane.clickSortByName();

            votePane.assertSortByNameHighlighted();
            votePane.assertFirstVoteListsPosition(players[1].name);
        });

        it('Will sort by vote count', () => {
            const phase = phaseModel.getDay();
            const players = [playerModel.get(), playerModel.get2(), playerModel.get3()]
                .map(playerModel.getInGame);
            const voteInfo = voteInfoModel.get({
                allowedTargets: [players[0].name, players[1].name, SKIP_DAY],
                voterToVotes: {
                    [players[0].name]: [SKIP_DAY],
                    [players[1].name]: [SKIP_DAY],
                    [players[2].name]: [SKIP_DAY],
                },
            });
            cy.goToGame({ phase, players, voteInfo });

            tabs.clickVotePane();
            votePane.clickSortByVoteCount();

            votePane.assertSortByVoteCountHighlighted();
            votePane.assertFirstVoteListsPosition(SKIP_DAY);
        });

        it('Will not allow interactions with the "Not Voting" ballot', () => {
            const profile = profileModel.getInGame();
            const players = [playerModel.get({ name: profile.name })].map(playerModel.getInGame);
            const voteInfo = voteInfoModel.get({
                allowedTargets: [PLAYER_NAME, PLAYER_NAME2, SKIP_DAY],
                voterToVotes: {
                    [players[0].name]: [],
                },
            });
            cy.goToGame({ players, profile, voteInfo });

            tabs.clickVotePane();

            votePane.assertNotUnvoteable(NOT_VOTING, profile.name);
        });
    });

    context('Viewing vote pane during trial', () => {
        const phase = phaseModel.getTrial();
        it('Will show someone on trial', () => {
            const voteInfo = voteInfoModel.get({
                trialedPlayerName: PLAYER_NAME,
            });
            cy.goToGame({ phase, voteInfo });

            tabs.clickVotePane();

            votePane.assertOnTrial(voteInfo.trialedPlayerName);
        });

        it('Will open and close the vote popout', () => {
            const voteInfo = voteInfoModel.get({
                trialedPlayerName: PLAYER_NAME,
            });
            cy.goToGame({ phase, voteInfo });

            tabs.clickVotePane();
            votePane.closeVotePopout();

            votePane.assertVotePopoutClosed();

            votePane.openVotePopout();

            votePane.assertVotePopoutOpen();
        });

        it('Will show a pending vote', () => {
            const players = [playerModel.get(), playerModel.get2()].map(playerModel.getInGame);
            const [player1, player2] = players;
            const profile = profileModel.getInGame({
                name: player1.name,
                actions: [actionModel.get({
                    command: VOTE,
                    targets: [player2.name],
                })],
            });
            const voteInfo = voteInfoModel.get({
                allowedTargets: [player2.name],
                trialedPlayerName: player2.name,
                voterToVotes: { [profile.name]: [] },
            });

            cy.goToGame({
                phase, players, profile, voteInfo,
            });

            tabs.clickVotePane();

            votePane.assertNoVoteButton(player2.name);
            votePane.assertVotePending(player2.name, profile.name);
            votePane.assertVoteCount(player2.name, 0);
        });

        it('Will show a pending cancelled vote', () => {
            const players = [playerModel.get(), playerModel.get2(), playerModel.get3()]
                .map(playerModel.getInGame);
            const [player1, player2, player3] = players;
            const profile = profileModel.getInGame({
                name: player1.name,
                actions: [],
            });
            const voteInfo = voteInfoModel.get({
                allowedTargets: players.map(p => p.name),
                trialedPlayerName: player2.name,
                voterToVotes: {
                    [profile.name]: [player2.name],
                    [player3.name]: [player2.name],
                },
            });

            cy.goToGame({
                phase, players, profile, voteInfo,
            });

            tabs.clickVotePane();

            votePane.assertVoteButtonExists(player2.name);
            votePane.assertVoteCancelPending(player2.name, profile.name);
            votePane.assertNoVoteCancelPending(player2.name, player3.name);
            votePane.assertVoteCount(player2.name, 2);
        });
    });

    context('Self vote interaction', () => {
        const phase = phaseModel.getDay();
        const profile = profileModel.getInGame();
        const voteInfo = voteInfoModel.get({
            allowedTargets: [profile.name],
        });

        it('Will not show a vote button if self vote is disabled', () => {
            const setup = setupModel.getDefaultStarted({
                setupModifiers: [setupModifierModel.get({ name: 'SELF_VOTE', value: false })],
            });
            cy.goToGame({
                phase, profile, setup, voteInfo,
            });

            tabs.clickVotePane();

            votePane.assertNoVoteButton(profile.name);
        });

        it('Will show a vote button if self vote is enabled', () => {
            const setup = setupModel.getDefaultStarted({
                setupModifiers: [setupModifierModel.get({ name: 'SELF_VOTE', value: true })],
            });
            cy.goToGame({
                phase, profile, setup, voteInfo,
            });

            tabs.clickVotePane();

            votePane.assertVoteButtonExists(profile.name);
        });
    });

    context('On vote update', () => {
        const phase = phaseModel.getDay();
        const players = [playerModel.get(), playerModel.get2()]
            .map(playerModel.getInGame);
        it('Will add a vote via an update', () => {
            const voteInfo = voteInfoModel.get({
                allowedTargets: players.map(p => p.name),
            });
            cy.goToGame({ players, phase, voteInfo });
            const voteEvent = voteUpdateEvent.get({
                voterName: players[1].name,
                voteTargets: [players[0].name],
            });

            tabs.clickVotePane();

            cy.window().then(async window => {
                await window.handleObject(voteEvent);
                cy.wait(2000);

                votePane.assertVotingFor(voteEvent.voterName, players[0].name);
            });
        });
    });

    context('On trial start', () => {
        it('Will update to a trial view', () => {
            const phase = phaseModel.getDay();
            const trialEvent = trialStartEvent.get();

            cy.goToGame({ phase });

            cy.window().then(async window => {
                await window.handleObject(trialEvent);
                cy.wait(2000);

                votePane.assertOnTrial(trialEvent.trialedUser.name);
            });
        });
    });

    context('Clicking vote actions', () => {
        const phase = phaseModel.getDay();
        it('Will submit multiple skip votes', () => {
            const profile = profileModel.getInGame({
                actions: [actionModel.get({
                    command: VOTE,
                    targets: [PLAYER_NAME2],
                })],
            });
            const playerName = profile.name;
            const players = [playerModel.get({ name: playerName })];
            const voteInfo = voteInfoModel.get({
                allowedTargets: [PLAYER_NAME2, PLAYER_NAME3],
                voterToVotes: {
                    [playerName]: [PLAYER_NAME2],
                },
            });
            cy.goToGame({
                abilities, gameModifiers, phase, profile, players, voteInfo,
            });
            tabs.clickVotePane();

            votePane.assertNoVoteButton(PLAYER_NAME2);

            votePane.vote(PLAYER_NAME3, {
                onRequest: requestResponse => {
                    const { body } = requestResponse.request;
                    expect(body).to.be.eql({
                        command: VOTE.toLowerCase(), // doesn't need to be lower case but w/e
                        targets: [PLAYER_NAME2, PLAYER_NAME3],
                    });
                },
                response: {
                    actions: actionsModel.get({
                        actions: [actionModel.get({
                            command: VOTE,
                            targets: [PLAYER_NAME2, PLAYER_NAME3],
                        })],
                    }),
                    voteInfo: {
                        ...voteInfo,
                        voterToVotes: {
                            [playerName]: [PLAYER_NAME2, PLAYER_NAME3],
                        },
                    },
                },
            });

            votePane.assertVotingFor(playerName, PLAYER_NAME2);
            votePane.assertVotingFor(playerName, PLAYER_NAME3);

            votePane.removeVote(PLAYER_NAME3, profile.name, {
                onRequest: requestResponse => {
                    const { body } = requestResponse.request;
                    expect(body).to.be.eql({
                        command: VOTE.toLowerCase(), // doesn't need to be lower case but w/e
                        targets: [PLAYER_NAME2],
                    });
                },
                response: {
                    actions: actionsModel.get({
                        actions: [actionModel.get({
                            command: VOTE,
                            targets: [PLAYER_NAME2],
                        })],
                    }),
                    voteInfo: {
                        ...voteInfo,
                        voterToVotes: {
                            [playerName]: [PLAYER_NAME2],
                        },
                    },
                },
            });

            votePane.assertNoVoters(PLAYER_NAME3);
            votePane.assertVotingFor(playerName, PLAYER_NAME2);
        });

        it('Will add a vote and then remove a vote', () => {
            const profile = profileModel.getInGame();
            const players = [playerModel.get({ name: profile.name }), playerModel.get2()]
                .map(playerModel.getInGame);
            const voteInfo = voteInfoModel.get({
                allowedTargets: [players[1].name],
            });
            cy.goToGame({
                abilities, gameModifiers, phase, players, profile, voteInfo,
            });

            tabs.clickVotePane();

            votePane.vote(players[1].name, {
                response: {
                    actions: actionsModel.get({
                        actions: [actionModel.get({
                            command: VOTE,
                            targets: [players[1].name],
                        })],
                    }),
                    voteInfo: {
                        ...voteInfo,
                        voterToVotes: {
                            [profile.name]: [players[1].name],
                        },
                    },
                },
            });

            votePane.stopVoting(players[1].name, profile.name);

            votePane.assertNoVoters(players[1].name);
        });

        it('Will remove a vote and then add a vote', () => {
            const players = [playerModel.get(), playerModel.get2()]
                .map(playerModel.getInGame);
            const profile = profileModel.getInGame({
                name: players[0].name,
                actions: [actionModel.get({
                    command: VOTE,
                    targets: [players[1].name],
                })],
            });
            const voteInfo = voteInfoModel.get({
                allowedTargets: [players[1].name],
                voterToVotes: { [profile.name]: [players[1].name] },
            });
            cy.goToGame({
                abilities, gameModifiers, phase, players, profile, voteInfo,
            });

            tabs.clickVotePane();

            votePane.stopVoting(players[1].name, profile.name);

            votePane.vote(players[1].name, {
                onRequest: requestResponse => {
                    const { body } = requestResponse.request;
                    expect(body).to.be.eql({
                        command: VOTE.toLowerCase(), // doesn't need to be lower case but w/e
                        targets: [players[1].name],
                    });
                },
                response: {
                    actions: actionsModel.get({
                        actions: [actionModel.get({
                            command: VOTE,
                            targets: [players[1].name],
                        })],
                    }),
                    voteInfo: {
                        ...voteInfo,
                        voterToVotes: {
                            [profile.name]: [players[1].name],
                        },
                    },
                },
            });

            votePane.assertVotingFor(profile.name, players[1].name);
        });

        it('Will remove a vote', () => {
            const profile = profileModel.getInGame({
                actions: [actionModel.get({
                    command: VOTE,
                    targets: [PLAYER_NAME2],
                })],
            });
            const players = [playerModel.get({ name: profile.name }),
                playerModel.get2(), playerModel.get3(),
            ].map(playerModel.getInGame);
            const [player, player2, player3] = players;

            const voteInfo = voteInfoModel.get({
                allowedTargets: [player2.name, player3.name],
                voterToVotes: {
                    [player.name]: [player2.name],
                },
            });
            cy.goToGame({
                abilities, gameModifiers, phase, players, profile, voteInfo,
            });
            tabs.clickVotePane();

            votePane.stopVoting(player2.name, profile.name);

            votePane.assertNoVoters(player2.name);
            votePane.assertNoVoters(player3.name);
        });
    });

    context('Clicking vote actions during trial', () => {
        const phase = phaseModel.getTrial();
        it('Will submit a pending unvotes during a trial', () => {
            const players = [playerModel.get(), playerModel.get2(), playerModel.get3()]
                .map(playerModel.getInGame);
            const [, player2, player3] = players;
            const profile = profileModel.getInGame({
                name: players[0].name,
                actions: [actionModel.get({
                    command: VOTE,
                    targets: [player2.name],
                })],
            });
            const voteInfo = voteInfoModel.get({
                trialedPlayerName: player2.name,
                allowedTargets: [player2.name, player3.name],
                voterToVotes: { [profile.name]: [player2.name] },
            });
            cy.goToGame({
                abilities, gameModifiers, phase, players, profile, voteInfo,
            });

            tabs.clickVotePane();

            votePane.stopVoting(player2.name, profile.name);

            votePane.assertVoteCancelPending(player2.name, profile.name);
        });
    });
});
