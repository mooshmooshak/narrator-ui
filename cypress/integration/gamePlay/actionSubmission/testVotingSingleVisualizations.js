import { SKIP_DAY } from '../../../../src/js/util/constants';

import tabs from '../../../pages/gamePlay/tabs';
import votePane from '../../../pages/gamePlay/votePane';

import actionsModel from '../../../fixtures/baseModels/actionsModel';
import actionModel from '../../../fixtures/baseModels/actionModel';
import phaseModel from '../../../fixtures/baseModels/phaseModel';
import playerModel from '../../../fixtures/baseModels/playerModel';
import profileModel from '../../../fixtures/baseModels/profileModel';
import voteInfoModel from '../../../fixtures/baseModels/voteInfoModel';


const VOTE = 'Vote';

describe('Test Single Voting Visualizations', () => {
    const phase = phaseModel.getDay();
    context('Viewing vote pane', () => {
        it('Will show the correct pane on single vote', () => {
            const gameModifiers = [{
                name: 'TRIAL_LENGTH',
                value: 0,
            }];
            cy.goToGame({ gameModifiers });

            tabs.clickVotePane();

            votePane.assertEliminationHeader();
        });
    });

    context('Clicking vote actions', () => {
        it('Will submit a skip vote to the server', () => {
            const profile = profileModel.getInGame();
            const playerName = profile.name;
            const players = [playerModel.getInGame({ name: playerName })];
            const voteInfo = voteInfoModel.get({
                allowedTargets: [SKIP_DAY],
            });
            cy.goToGame({ phase, players, voteInfo });

            tabs.clickVotePane();
            votePane.vote('Skip Day', {
                onRequest: requestResponse => {
                    const { body } = requestResponse.request;
                    expect(body).to.be.eql({ command: 'vote', targets: [SKIP_DAY] });
                },
                response: {
                    actions: actionsModel.get({
                        actions: [actionModel.get({
                            command: VOTE,
                            targets: [SKIP_DAY],
                        })],
                    }),
                    voteInfo: {
                        ...voteInfo,
                        voterToVotes: {
                            [playerName]: [SKIP_DAY],
                        },
                    },
                },
            });

            votePane.assertVotingFor(profile.name, SKIP_DAY);
            votePane.assertVoteCount(SKIP_DAY, players[0].votePower);
        });

        it('Will change votes again after a change vote', () => {
            const players = [playerModel.get(), playerModel.get2()].map(playerModel.getInGame);
            const [, player2] = players;
            const profile = profileModel.getInGame({
                name: players[0].name,
                actions: [actionModel.get({
                    command: VOTE,
                    targets: [SKIP_DAY],
                })],
            });
            const voteInfo = voteInfoModel.get({
                allowedTargets: [SKIP_DAY, player2.name],
            });
            cy.goToGame({
                phase, players, profile, voteInfo,
            });
            tabs.clickVotePane();

            votePane.vote(player2.name, {
                onRequest: requestResponse => {
                    const { body } = requestResponse.request;
                    expect(body).to.be.eql({
                        command: VOTE.toLowerCase(), // shouldn't need to be lower
                        targets: [player2.name],
                    });
                },
                response: {
                    actions: actionsModel.get({
                        actions: [actionModel.get({
                            command: VOTE,
                            targets: [player2.name],
                        })],
                    }),
                    voteInfo: {
                        ...voteInfo,
                        voterToVotes: {
                            [profile.name]: [player2.name],
                        },
                    },
                },
            });

            votePane.assertVotingFor(profile.name, player2.name);

            votePane.vote(SKIP_DAY, {
                onRequest: requestResponse => {
                    const { body } = requestResponse.request;
                    expect(body).to.be.eql({
                        command: VOTE.toLowerCase(), // shouldn't need to be lower
                        targets: [SKIP_DAY],
                    });
                },
                response: {
                    actions: actionsModel.get({
                        actions: [actionModel.get({
                            command: VOTE,
                            targets: [SKIP_DAY],
                        })],
                    }),
                    voteInfo: {
                        ...voteInfo,
                        voterToVotes: {
                            [profile.name]: [SKIP_DAY],
                        },
                    },
                },
            });

            votePane.assertVotingFor(profile.name, SKIP_DAY);
        });

        it('Will not show an option for you to vote for yourself', () => {
            const profile = profileModel.getInGame();
            const voteInfo = voteInfoModel.get({
                allowedTargets: [profile.name],
            });
            cy.goToGame({ phase, profile, voteInfo });

            votePane.assertNoVoteButton(profile.name);
        });
    });
});
