import actionPane from '../../../pages/gamePlay/actionPane';
import tabs from '../../../pages/gamePlay/tabs';
import toasts from '../../../pages/toasts';

import deprecatedAbilitiesModel from '../../../fixtures/baseModels/deprecatedAbilitiesModel';
import phaseModel from '../../../fixtures/baseModels/phaseModel';

import { PLAYER_NAME2, PLAYER_NAME3 } from '../../../fixtures/fakeConstants';


function getAbilities(){
    return deprecatedAbilitiesModel.get('Votesteal');
}

describe('Test Vote Transfer Visualizations', () => {
    context('Viewing elector ability', () => {
        it('Will show the elector header nicely', () => {
            const abilities = getAbilities();
            cy.goToGame({
                abilities,
                phase: phaseModel.getNight(),
            });

            tabs.clickActions();

            actionPane.assertHeader('Redirect Vote');
            toasts.assertHiddenOld();
        });

        it('Will properly color elector\'s submitted actions', () => {
            const abilities = getAbilities();
            const actions = {
                actionList: [{
                    command: 'votesteal',
                    playerNames: [PLAYER_NAME2, PLAYER_NAME3],
                    text: '',
                }],
            };
            cy.goToGame({
                abilities,
                actions,
                phase: phaseModel.getNight(),
            });

            tabs.clickActions();

            actionPane.assertLeftClicked(PLAYER_NAME2);
            actionPane.assertRightClicked(PLAYER_NAME3);
            actionPane.assertVisible([PLAYER_NAME2, PLAYER_NAME3]);
        });
    });

    context('Clicking elector actions', () => {
        it('Will correctly highlight left side', () => {
            const abilities = getAbilities();
            cy.goToGame({
                abilities,
                phase: phaseModel.getNight(),
            });
            tabs.clickActions();

            actionPane.clickLeft(PLAYER_NAME2);

            actionPane.assertLeftClicked(PLAYER_NAME2);
        });
    });

    context('Hovering elector actions', () => {
        it('Will correctly highlight left side', () => {
            const abilities = getAbilities();
            cy.goToGame({
                abilities,
                phase: phaseModel.getNight(),
            });
            tabs.clickActions();

            actionPane.hoverLeft(PLAYER_NAME2);

            actionPane.assertRightNotHovered(PLAYER_NAME2);
        });
    });
});
