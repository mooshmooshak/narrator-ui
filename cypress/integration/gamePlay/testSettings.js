import phaseModel from '../../fixtures/baseModels/phaseModel';
import playerModel from '../../fixtures/baseModels/playerModel';

import settingsPane from '../../pages/gamePlay/settingsPane';
import tabs from '../../pages/gamePlay/tabs';


describe('Test Settings', () => {
    context('When viewing the settings', () => {
        it('Will have defaults colored', () => {
            cy.goToGame();

            tabs.clickSettings();

            settingsPane.assertNightMusicOn();
        });
    });

    context('On force end via skip', () => {
        it('Will send a mass skip day request', () => {
            const phase = phaseModel.getDay();
            const players = [playerModel.get(), playerModel.get2()];
            // maybe do something about saying this is a host only action
            cy.goToGame({
                phase, players,
            });
            tabs.clickSettings();

            settingsPane.clickForceEndPhase(requestResponse => {
                const body = requestResponse.request.body;
                expect(body.seconds).to.be.equal(0);
            });
        });
    });
});
