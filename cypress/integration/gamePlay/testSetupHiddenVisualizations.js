import gameInfo from '../../pages/gamePlay/gameInfo';
import setupHiddensPane from '../../pages/gamePlay/setupHiddensPane';
import tabs from '../../pages/gamePlay/tabs';

import hiddenSpawnModel from '../../fixtures/baseModels/hiddenSpawnModel';
import roleModel from '../../fixtures/baseModels/roleModel';
import setupHiddenModel from '../../fixtures/baseModels/setupHiddenModel';
import setupModel from '../../fixtures/baseModels/setupModel';

import {
    FACTION_NAME, HIDDEN_NAME, ROLE_ID, ROLE_NAME, ROLE_NAME2,
} from '../../fixtures/fakeConstants';


describe('Setup Hidden Visualizations', () => {
    context('Viewing setup hiddens', () => {
        it('Will show a setup hidden in the pan', () => {
            const setup = setupModel.getDefaultStarted();
            setup.setupHiddens = [setupHiddenModel.get()];
            cy.goToGame({ setup });

            tabs.clickGameOverview();

            setupHiddensPane.assertSetupHiddenCount(1);
        });
    });

    context('Clicking on setup hiddens', () => {
        it('Will show hidden details on desktop', () => {
            cy.goToGame();
            tabs.clickGameOverview();

            setupHiddensPane.clickFirst();

            gameInfo.assertSetupHiddenName(HIDDEN_NAME);
        });

        it('Will show hidden details in the pane on mobile', () => {
            cy.mobile();
            cy.goToGame();
            tabs.clickGameOverview();

            setupHiddensPane.clickFirst();

            gameInfo.assertSetupHiddenName(HIDDEN_NAME);
        });

        it('Will show hidden single details in the pane on mobile', () => {
            cy.mobile();
            const setup = setupModel.getDefaultStarted();
            setup.hiddens[0].spawns.length = 1;
            setup.setupHiddens = [setupHiddenModel.get()];
            cy.goToGame({ setup });
            tabs.clickGameOverview();

            setupHiddensPane.clickFirst();

            gameInfo.assertSetupHiddenName(ROLE_NAME);
        });

        it('Will show faction details in the pane', () => {
            const setup = setupModel.getDefaultStarted();
            setup.hiddens[0].spawns = [hiddenSpawnModel.get()];
            setup.hiddens[0].name = ROLE_NAME;
            setup.setupHiddens = [setupHiddenModel.get()];
            cy.goToGame({ setup });
            tabs.clickGameOverview();

            setupHiddensPane.clickFirst();
            gameInfo.clickTeamSubheader();

            gameInfo.assertFactionClicked(FACTION_NAME);
            gameInfo.assertSubheaderHidden();
        });

        it('Will show nested role details from a faction click', () => {
            const setup = setupModel.getDefaultStarted();
            setup.hiddens[0].spawns = [hiddenSpawnModel.get()];
            setup.setupHiddens = [setupHiddenModel.get()];
            cy.goToGame({ setup });
            tabs.clickGameOverview();

            setupHiddensPane.clickFirst();
            gameInfo.clickTeamSubheader();
            gameInfo.clickRole(ROLE_NAME);

            gameInfo.assertRoleClicked(ROLE_NAME);
        });

        it('Will show role details in the pane', () => {
            const setup = setupModel.getDefaultStarted();
            setup.hiddens[0].spawns = [hiddenSpawnModel.get()];
            setup.hiddens[0].name = ROLE_NAME;
            setup.setupHiddens = [setupHiddenModel.get()];
            cy.goToGame({ setup });
            tabs.clickGameOverview();

            setupHiddensPane.clickFirst();
            gameInfo.clickTeamSubheader();
            gameInfo.clickRole(ROLE_NAME);

            gameInfo.assertRoleClicked(ROLE_NAME);
        });

        it('Will not show multiple same spawning from hiddens', () => {
            const setup = setupModel.getDefaultStarted();
            setup.hiddens[0].spawns = [hiddenSpawnModel.get(), hiddenSpawnModel.get2()];
            setup.hiddens[0].name = HIDDEN_NAME;
            setup.roles = [roleModel.get()];
            setup.setupHiddens = [setupHiddenModel.get(), setupHiddenModel.get()];
            cy.goToGame({ setup });
            tabs.clickGameOverview();

            setupHiddensPane.clickFirst();
            gameInfo.clickRole(ROLE_NAME);

            gameInfo.assertRoleClicked(ROLE_NAME);
            // 2 setup hiddens with the same hidden, but I don't want to see duplicates
            // 1 is the single clickable hidden that can take you to the info on hiddens
            gameInfo.assertSpawnedByCount(1);
        });

        it('Will show nested role details in the pane', () => {
            const setup = setupModel.getDefaultStarted();
            setup.hiddens[0].spawns = [hiddenSpawnModel.get()];
            setup.hiddens[0].name = ROLE_NAME;
            setup.setupHiddens = [setupHiddenModel.get()];
            setup.factions.forEach(faction => faction.factionRoles
                .forEach(factionRole => {
                    factionRole.roleID = ROLE_ID;
                }));
            cy.goToGame({ setup });
            tabs.clickGameOverview();

            setupHiddensPane.clickFirst();
            gameInfo.clickTeamSubheader();
            gameInfo.clickRole(ROLE_NAME);
            gameInfo.clickRole(ROLE_NAME2);

            gameInfo.assertRoleClicked(ROLE_NAME2);
        });
    });
});
