import chatModel from '../../fixtures/baseModels/chatModel';

import chatPane from '../../pages/gamePlay/chatPane';
import chatTabs from '../../pages/gamePlay/chatTabs';

import integrationTypes from '../../support/integrationTypes';


describe('Chat Visualizations', () => {
    context('Integrations', () => {
        it('Will show the chat bar if there are no integrations', () => {
            const integrations = [];
            const chats = chatModel.get();
            cy.goToGame({ chats, integrations });

            chatTabs.clickLast();

            chatPane.assertInputVisible();
        });

        it('Will not show the chat bar if sc2mafia is an integration', () => {
            const integrations = [integrationTypes.SC2MAFIA];
            const chats = chatModel.get();
            cy.goToGame({ chats, integrations });

            chatTabs.clickLast();

            chatPane.assertInputInvisible();
        });
    });
});
