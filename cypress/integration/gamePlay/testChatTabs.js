import chatTabs from '../../pages/gamePlay/chatTabs';
import chatPane from '../../pages/gamePlay/chatPane';
import ticker from '../../pages/ticker';

import chatModel from '../../fixtures/baseModels/chatModel';
import messageModel from '../../fixtures/baseModels/messageModel';

import { MESSAGE_TEXT } from '../../fixtures/fakeConstants';


describe('Test chat tabs', () => {
    context('When tabs are interacted with', () => {
        it('Will open the correct chat', () => {
            cy.mobile();
            cy.goToGame({
                chats: chatModel.get({
                    messages: [messageModel.get()],
                }),
            });
            ticker.clickChatBubble();

            chatTabs.clickLast();

            chatPane.assertMessageVisible(MESSAGE_TEXT);
        });
    });
});
