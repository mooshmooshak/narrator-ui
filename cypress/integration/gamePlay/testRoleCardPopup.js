import { FACTION_NAME, ROLE_NAME } from '../../fixtures/fakeConstants';

const dayStartEvent = require('../../fixtures/events/dayStartEvent');

const eventPopup = require('../../pages/gamePlay/eventPopup');


describe('Rolecard Popup Visualisations', () => {
    context('On Day Start', () => {
        it('Will show a popup', () => {
            const roleCardUpdateEventObj = dayStartEvent.get();
            cy.goToGame();

            cy.window().then(async window => {
                window.storageService.saveSeenFeedback(0);
                await window.handleObject(roleCardUpdateEventObj);

                eventPopup.assertVisible();
                eventPopup.assertTitle(`${FACTION_NAME} ${ROLE_NAME}`);
            });
        });
    });
});
