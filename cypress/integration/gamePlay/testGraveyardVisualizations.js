import gameInfo from '../../pages/gamePlay/gameInfo';
import graveyardPane from '../../pages/gamePlay/graveyardPane';
import setupHiddensPane from '../../pages/gamePlay/setupHiddensPane';
import tabs from '../../pages/gamePlay/tabs';

import graveModel from '../../fixtures/baseModels/graveModel';
import playerModel from '../../fixtures/baseModels/playerModel';
import setupHiddenModel from '../../fixtures/baseModels/setupHiddenModel';
import setupModel from '../../fixtures/baseModels/setupModel';

import {
    FACTION_NAME, HIDDEN_NAME, PLAYER_NAME, ROLE_NAME,
} from '../../fixtures/fakeConstants';

import { MAX_PLAYER_COUNT } from '../../../src/js/util/constants';


describe('Graveyard visualizations', () => {
    context('Viewing graveyard', () => {
        it('Will show the graveyard contents', () => {
            const players = [playerModel.getDead()];
            cy.goToGame({ players });

            tabs.clickGameOverview();

            graveyardPane.assertCount(1);
            graveyardPane.assertVisibleRoleForPlayer(PLAYER_NAME, ROLE_NAME);
        });
    });

    context('Clicking on grave', () => {
        it('Will show grave details', () => {
            const players = [playerModel.getDead()];
            cy.goToGame({ players });
            tabs.clickGameOverview();

            graveyardPane.clickFirst();

            gameInfo.assertGraveClicked(PLAYER_NAME);
        });

        it('Will show cleaned dead players', () => {
            const players = [playerModel.get({ grave: graveModel.getCleaned() })];
            cy.goToGame({ players });
            tabs.clickGameOverview();

            graveyardPane.clickFirst();

            gameInfo.assertGraveClicked(PLAYER_NAME);
        });

        it('Will show grave details after clicking a hidden', () => {
            const players = [playerModel.getDead()];
            cy.goToGame({ players });
            tabs.clickGameOverview();

            setupHiddensPane.click(HIDDEN_NAME);
            graveyardPane.clickFirst();

            gameInfo.assertGraveClicked(PLAYER_NAME);
            gameInfo.assertContainsDeath();
        });

        it('Will show faction from the grave overview', () => {
            const players = [playerModel.getDead()];
            cy.goToGame({ players });
            tabs.clickGameOverview();
            graveyardPane.clickFirst();

            gameInfo.clickTeamSubheader();

            gameInfo.assertFactionClicked(FACTION_NAME);
            gameInfo.assertNotContains('Died on');
        });

        it('Will show nested hidden info from the grave overview', () => {
            const players = [playerModel.getDead()];
            cy.goToGame({ players });
            tabs.clickGameOverview();

            graveyardPane.clickFirst();
            gameInfo.clickTeamSubheader();
            gameInfo.clickRole(ROLE_NAME);
            gameInfo.clickHidden(HIDDEN_NAME);
            gameInfo.clickRole(ROLE_NAME);
            gameInfo.clickHidden(HIDDEN_NAME);

            gameInfo.assertHiddenClicked(HIDDEN_NAME);
        });

        it('Will show grave the grave if there are a lot of roles', () => {
            cy.mobile();
            const setup = setupModel.getDefaultStarted({
                setupHiddens: new Array(MAX_PLAYER_COUNT).fill(setupHiddenModel.get()),
            });
            const players = [playerModel.getDead()];
            cy.goToGame({ players, setup });

            tabs.clickGameOverview();
            graveyardPane.clickFirst();

            gameInfo.assertGraveClicked(PLAYER_NAME);
        });
    });
});
