/* global Cypress:readonly */
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
import threadVotesModel from '../fixtures/condorcetModels/threadVotesModel';

import actionsModel from '../fixtures/baseModels/actionsModel';
import baseAbilityModel from '../fixtures/baseModels/baseAbilityModel';
import firebaseModel from '../fixtures/baseModels/firebaseState';
import gameModel from '../fixtures/baseModels/gameModel';
import phaseModel from '../fixtures/baseModels/phaseModel';
import playerModel from '../fixtures/baseModels/playerModel';
import profileModel from '../fixtures/baseModels/profileModel';
import setupModel from '../fixtures/baseModels/setupModel';
import voteInfoModel from '../fixtures/baseModels/voteInfoModel';

import authTokenResponse from '../fixtures/responses/authTokenResponse';
import chatResponse from '../fixtures/chatResponse';
import setupFeaturedResponse from '../fixtures/responses/setupFeaturedResponse';
import userStateSetupResponse from '../fixtures/userStateSetupReponse';

import { GAME_ID, PLAYER_NAME, USER_ID } from '../fixtures/fakeConstants';


Cypress.Commands.add('isNotActionable', selector => {
    cy.get(selector).click();
    cy.once('fail', err => {
        expect(err.message).to.include('cy.click()` failed because this element');
        expect(err.message).to.include('is being covered by another element');
    });
});

Cypress.Commands.add('goToHome', (args = {}) => {
    const userID = args.userID || USER_ID;
    const featuredSetups = args.featuredSetups || setupFeaturedResponse.get();
    const games = args.games || [];

    cy.server();
    cy.route({
        method: 'GET',
        url: 'api/profiles',
        response: { response: profileModel.getNoGame() },
    }).as('profileInitResponse');
    cy.route({
        method: 'GET',
        url: 'api/setups/featured',
        response: { response: featuredSetups, errors: [] },
        onResponse: args.onResponse,
    }).as('featuredSetupsResponse');
    cy.route({
        method: 'GET',
        url: 'api/users/id',
        response: { response: { id: userID } },
    }).as('userResponse');
    cy.route({
        method: 'GET',
        url: 'api/games',
        response: { response: games },
    });
    cy.visit('http://localhost:4502', {
        onLoad: w => stubFirebase(w, args.firebaseState),
    });
    cy.wait(['@featuredSetupsResponse']);
});

function goToLobby(userState = {}, userID = USER_ID){
    const activeUserIDs = userState.activeUserIDs || [userID];
    const lobbyChatMessages = userState.lobbyChatMessages || [];
    const setupsFeatured = userState.setupsFeatured || setupFeaturedResponse.get();

    cy.server();
    cy.route({
        method: 'GET',
        url: 'api/profiles',
        response: { response: userState.profile || profileModel.getInLobby({ userID }) },
    });
    cy.route({
        method: 'GET',
        url: 'api/setups/featured',
        response: { response: setupsFeatured },
    });
    cy.route({
        method: 'GET',
        url: `api/setups?userID=${userID}`,
        response: { response: userState.userSetups || [] },
    });
    cy.route({
        method: 'GET',
        url: 'api/users*',
        response: authTokenResponse.get(userID),
    });
    cy.route({
        method: 'POST',
        url: 'api/user_integrations*',
        response: authTokenResponse.get(userID),
    });
    cy.route({
        method: 'GET',
        url: `api/channels/browser/activeUserIDs?game_id=${GAME_ID}`,
        response: { response: activeUserIDs },
    }).as('activeBrowserUserIDs');
    cy.route({
        method: 'GET',
        url: 'api/baseAbilities',
        response: { response: [baseAbilityModel.get()] },
    });
    cy.route({
        method: 'GET',
        url: `api/chats/${GAME_ID}/lobby`,
        response: { response: lobbyChatMessages },
    }).as('chatResponse');

    const responseObj = userStateSetupResponse.get({
        isStarted: false,
        moderatorIDs: userState.moderatorIDs,
    });
    Object.keys(userState || [])
        .forEach(key => {
            responseObj[key] = userState[key];
        });

    const { setup } = responseObj;
    setup.hiddens = setup.hiddens || [];
    setup.setupHiddens = setup.setupHiddens || [];
    const players = userState.players || [playerModel.get()];

    cy.route({
        method: 'GET',
        url: `api/games/${GAME_ID}`,
        response: {
            response: gameModel.get({
                integrations: userState.integrations,
                isStarted: false,
                moderatorIDs: userState.moderatorIDs,
                modifiers: userState.gameModifiers,
                players,
                setup,
                users: userState.users,
            }),
        },
    }).as('gameResponse');
    cy.route({
        method: 'GET',
        url: 'api/games/userState',
        response: { response: responseObj },
    }).as('userStateResponse');

    const authToken = new Array(128 + 1).join('A');
    const firebaseState = firebaseModel.getPreviousProfile();
    cy.visit(`http://localhost:4502/?auth_token=${authToken}`, {
        onLoad: w => stubFirebase(w, firebaseState),
    });
    cy.wait(['@userStateResponse', '@chatResponse', '@activeBrowserUserIDs', '@gameResponse']);
}

Cypress.Commands.add('goToLobby', (userState, userID) => {
    goToLobby(userState, userID);
});

Cypress.Commands.add('goToSetup', (userState, userID) => {
    goToLobby(userState, userID);
    cy.get('.topNavBarItems').within(() => {
        cy.contains('Setup').click();
    });
});

Cypress.Commands.add('goToMobileSetup', (userState, userID) => {
    cy.mobile();
    goToLobby(userState, userID);
    cy.get('.navMenuIcon').click();
    cy.get('.drawerNavBarItems').within(() => {
        cy.contains('Setup').click();
    });
});

Cypress.Commands.add('goToGame', (userState = {}, userID) => {
    userID = userID || USER_ID;
    const activeUserIDs = userState.activeUserIDs || [userID];
    const abilities = userState.abilities || {
        type: ['kill'],
        kill: {
            players: [{ playerName: PLAYER_NAME }],
        },
    };
    const actions = userState.actions || actionsModel.get({
        actions: [],
    });
    const authToken = new Array(128 + 1).join('A');
    const chats = userState.chats || chatResponse.getEmpty();
    const graveyard = userState.graveyard || [];
    const integrations = userState.integrations || [];
    const phase = userState.phase || phaseModel.getDay();
    const players = userState.players || [];
    const profile = userState.profile || profileModel.getInGame({ actions: actions.actions });
    const setup = userState.setup || setupModel.getDefaultStarted();
    cy.server();
    cy.route({
        method: 'GET',
        url: `api/games/${GAME_ID}`,
        response: {
            response: gameModel.get({
                integrations,
                isStarted: true,
                modifiers: userState.gameModifiers,
                phase,
                players,
                setup,
                voteInfo: userState.voteInfo || voteInfoModel.get(),
            }),
        },
    });
    cy.route({
        method: 'GET',
        url: 'api/profiles',
        response: { response: profile },
    });
    cy.route({
        method: 'GET',
        url: 'api/setups',
        response: { response: setupFeaturedResponse.get() },
    });
    cy.route({
        method: 'GET',
        url: 'api/users/id',
        response: { response: { id: userID } },
    }).as('userResponse');
    cy.route({
        method: 'GET',
        url: `api/users?auth_token=${authToken}`,
        response: authTokenResponse.get(userID),
    }).as('userResponse');
    cy.route({
        method: 'POST',
        url: 'api/user_integrations*',
        response: authTokenResponse.get(userID),
    });
    cy.route({
        method: 'GET',
        url: `api/channels/browser/activeUserIDs?game_id=${GAME_ID}`,
        response: { response: activeUserIDs },
    });

    cy.route({
        method: 'GET',
        url: `api/chats?gameID=${GAME_ID}`,
        response: { response: chats },
    }).as('chatResponse');

    const responseObj = userStateSetupResponse.getStartedGame();
    Object.keys(userState || [])
        .forEach(key => {
            responseObj[key] = userState[key];
        });
    responseObj.actions = actions;
    responseObj.playerLists = abilities;
    responseObj.setup = setup;
    responseObj.graveYard = graveyard;
    responseObj.type.push('graveYard');
    responseObj.type.push('playerLists');
    responseObj.type.push('actions');
    cy.route({
        method: 'GET',
        url: 'api/games/userState',
        response: { response: responseObj },
    }).as('userStateResponse');

    const firebaseState = firebaseModel.getPreviousProfile();
    cy.visit(`http://localhost:4502/?auth_token=${authToken}`, {
        onLoad: w => stubFirebase(w, firebaseState),
    });
    cy.wait(['@userStateResponse', '@chatResponse', '@userResponse']);
});

Cypress.Commands.add('mobile', () => {
    cy.viewport(411, 731);
});

Cypress.Commands.add('goToCondorcet', goToCondorcet);

function goToCondorcet(params = {}){
    cy.server();
    cy.route({
        method: 'GET',
        url: '/api/channels/sc2mafia/condorcet?threadID=1',
        response: { response: params.threadVotes || threadVotesModel.get() },
    }).as('getVotesResponse');

    cy.route({
        method: 'POST',
        url: '/api/condorcet',
        response: { response: params.condorcetResponse || [] },
        onRequest: params.condorcetSubmitRequest,
    }).as('condorcetResponse');

    const urlParams = params.urlParams || {};
    urlParams.thread = 1;
    const url = `http://localhost:4502/condorcet${encodeURL(urlParams)}`;
    cy.visit(url);

    cy.wait(['@getVotesResponse', '@condorcetResponse']);
}

function encodeURL(params){
    if(!params)
        return '';
    return Object.keys(params).map((key, index) => {
        const value = params[key];
        if(index)
            return `&${key}=${value}`;
        return `?${key}=${value}`;
    }).join('');
}

function stubFirebase(cypressWindow, initialUser = null){
    const passedInFunctions = [];
    cypressWindow.firebase = {
        auth: () => {
            const onAuthStateChanged = user => passedInFunction => {
                passedInFunctions.push(passedInFunction);
                cypressWindow.user = user;
                Promise.resolve()
                    .then(() => passedInFunction(user));
                return () => {
                    const index = passedInFunctions.indexOf(passedInFunction);
                    if(index > -1)
                        passedInFunctions.splice(index, 1);
                };
            };
            return ({
                onAuthStateChanged: onAuthStateChanged(initialUser),
                signInAnonymously: async() => {
                    const anonymousProfile = firebaseModel.getAnonymousProfile();
                    if(passedInFunctions.length)
                        onAuthStateChanged(anonymousProfile)(passedInFunctions[0]);
                },
                signOut: () => Promise.resolve(),
            });
        },
    };
}

// export function drag(dragSelector, dropSelector){
//     // Based on this answer: https://stackoverflow.com/a/55436989/3694288
//     cy.get(dragSelector).should('exist')
//         .get(dropSelector).should('exist');
//
//     const draggable = Cypress.$(dragSelector)[0]; // Pick up this
//     const droppable = Cypress.$(dropSelector)[0]; // Drop over this

// const coords = droppable.getBoundingClientRect();
// draggable.dispatchEvent(new MouseEvent('mousedown'));
// draggable.dispatchEvent(new MouseEvent('mousemove', { clientX: 10, clientY: 0 }));
// draggable.dispatchEvent(new MouseEvent('mousemove', {
//     // I had to add (as any here --> maybe this can help solve the issue??)
//     clientX: coords.left + 10,
//     clientY: coords.top + 10, // A few extra pixels to get the ordering right
// }));
// draggable.dispatchEvent(new MouseEvent('mouseup'));
//     return cy.get(dropSelector);
// }
//
// Cypress.Commands.add('drag', drag);
