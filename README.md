# The Narrator [Web Server]
## Front End

### Dev Setup
1. clone the repo
`git clone git@gitlab.com:the-narrator/narrator-ui.git`
1. Download nodejs
* https://nodejs.org/en/download/
2. run `npm install`

### Running front end
1. run `npm run sass` to compile css changes.  This will listen for css changes, so you only ned to run it once if you're not actively editing css files.
2. run `npm run dev` to bring up front end on localhost:4502
