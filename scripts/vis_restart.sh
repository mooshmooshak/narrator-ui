CI_COMMIT_REF_NAME=$1
# compile scss to css files
[ -d src/css ] || mkdir src/css

npx sass src/scss/index.scss src/css/index.css
npx sass src/scss/index-tablet.scss src/css/index-tablet.css
npx sass src/scss/index-desktop.scss src/css/index-desktop.css

npx sass src/scss/condorcet.scss src/css/condorcet.css
npx sass src/scss/everest.scss src/css/everest.css
npx sass src/scss/deatheditor.scss src/css/deatheditor.css

# minify html files
npx html-minifier public/index.html --html5 --collapse-whitespace --remove-comments --removeRedundantAttributes -o public/index2.html
mv public/index2.html public/index.html

# minify css files
mkdir public/css/dist
npx csso src/css/index.css public/css/dist/index.css
npx csso src/css/index-tablet.css public/css/dist/index-tablet.css
npx csso src/css/index-desktop.css public/css/dist/index-desktop.css

npx csso src/css/condorcet.css public/css/dist/condorcet.css
npx csso src/css/everest.css public/css/dist/everest.css
npx csso src/css/deatheditor.css public/css/dist/deatheditor.css

[ -d src/css ] || mkdir src/css

npm install --production
npm run buildProd
mv dist public
rm /home/voss/"$CI_COMMIT_REF_NAME"/back-end/public
ln -s /home/voss/"$CI_COMMIT_REF_NAME"/ui/public /home/voss/"$CI_COMMIT_REF_NAME"/back-end/public
