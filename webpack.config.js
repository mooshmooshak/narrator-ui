const path = require('path');
const webpack = require('webpack');


const hostTarget = getHostTarget(process.env.API_TARGET);

module.exports = {
    entry: {
        index: './src/js/index.js',
    },
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                options: { presets: ['@babel/env'] },
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
        ],
    },
    resolve: { extensions: ['*', '.js', '.jsx'] },
    output: {
        path: path.resolve(__dirname, 'dist/'),
        publicPath: '/dist/',
    },
    devServer: {
        contentBase: path.join(__dirname, 'public/'),
        port: 4502,
        publicPath: hostTarget.publicPath,
        hotOnly: true,
        proxy: {
            '/api': {
                target: hostTarget.apiTarget,
            },
            '/napi/*': {
                target: hostTarget.napiTarget,
                ws: true,
            },
            '/': {
                target: hostTarget.indexTarget,
                // eslint-disable-next-line no-unused-vars
                bypass: function(req, res, proxyOptions){
                    const { url } = req;
                    if(['css', 'js', 'img', 'audio', 'rolepics']
                        .find(linkStart => url.startsWith(`/${linkStart}/`)))
                        return url;
                    if(url.startsWith('/favicon'))
                        return url;
                    if(url.startsWith('/manifest.json'))
                        return url;

                    const gameCode = getGameCode(url);
                    if(url.includes(`/${gameCode}/`))
                        return url.replace(`/${gameCode}`, '');
                    if(url.endsWith(`/${gameCode}`))
                        return url.replace(`/${gameCode}`, '/');
                    return 'index.html';
                },
            },
        },
    },
    plugins: [new webpack.HotModuleReplacementPlugin()],
};

function getGameCode(url){
    const gameCode = url.substring(1, 5);
    if(/^[A-Z][A-Z][A-Z][A-Z]$/.test(gameCode) && (url.charAt(5) === '/' || !url.charAt(5)))
        return gameCode;
}

function getHostTarget(target){
    if(target === 'prodlocal')
        return {
            publicPath: 'http://narrator.systeminplace.net/dist/',
            apiTarget: 'http://narrator.systeminplace.net',
            napiTarget: 'ws://narrator.systeminplace.net',
            indexTarget: 'http://narrator.systeminplace.net',
        };
    return {
        publicPath: 'http://localhost:3000/dist/',
        apiTarget: 'http://localhost:4501',
        napiTarget: 'ws://localhost:4501',
        indexTarget: 'http://localhost:4502',
    };
}
