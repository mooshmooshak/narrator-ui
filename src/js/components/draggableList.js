import React from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';


const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};

const grid = 8;

// eslint-disable-next-line no-unused-vars
const getListStyle = isDraggingOver => ({
    padding: grid,
});

const getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',

    // change background colour if dragging
    // background: isDragging ? 'lightgreen' : 'grey',

    // styles we need to apply on draggables
    ...draggableStyle,
});

export default function DraggableList({ className, onNewOrder = () => {}, items }){
    function onDragEnd(result){
        if(!result.destination)
            return;
        const newOrdering = reorder(
            items,
            result.source.index,
            result.destination.index,
        );

        onNewOrder(newOrdering.map(item => item.value));
    }
    const droppableClassName = `${className ? `${className} ` : ''}draggableList`;
    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="droppable" className={droppableClassName}>
                {(provided, snapshot) => (
                    <div
                        className={droppableClassName}
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        style={getListStyle(snapshot.isDraggingOver)}
                    >
                        {items.map(({
                            className: draggableClassName, label, styleOverrides, value,
                        }, index) => (
                            <Draggable key={value} draggableId={value} index={index}>
                                {(providedInner, snapshotInner) => (
                                    <div
                                        className={draggableClassName}
                                        ref={providedInner.innerRef}
                                        {...providedInner.draggableProps}
                                        {...providedInner.dragHandleProps}
                                        style={{
                                            ...getItemStyle(
                                                snapshotInner.isDragging,
                                                providedInner.draggableProps.style,
                                            ),
                                            ...styleOverrides,
                                        }}
                                    >
                                        {label}
                                    </div>
                                )}
                            </Draggable>
                        ))}
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
        </DragDropContext>
    );
}
