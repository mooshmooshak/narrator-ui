import React from 'react';
import { connect } from 'react-redux';

import { onShowHiddenAction } from '../actions/payloads/panelViewActions';

import { getColor } from '../util/hiddenUtils';


function FactionRoleClickable({ hidden, showHidden, ...setup }){
    return (
        <span
            className="roleHover"
            onClick={() => showHidden(hidden.id)}
            style={{ color: getColor(hidden, setup) }}
        >
            {hidden.name}
        </span>
    );
}

const mapStateToProps = ({ setupState }) => ({
    ...setupState,
});

const mapDispatchToProps = {
    showHidden: onShowHiddenAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(FactionRoleClickable);
