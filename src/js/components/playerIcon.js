import React from 'react';


export default function PlayerIcon({ color, icon }){
    return (
        <div className="playerIconContainer">
            <i
                className={`playerIcon ${icon}`}
                style={{ color }}
            />
            <div className="playerIconSpacer" />
        </div>
    );
}
