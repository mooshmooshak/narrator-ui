import React from 'react';
import { MESSAGE_TYPE } from '../../util/constants';
import ChatMessage from './chatMessage';


export default function ChatContainer({
    className, messages, onChange, onSubmit, textInputValue,
}){
    messages = [...messages];
    messages.reverse();
    const chatContainerClass = className ? ` ${className}` : '';
    return (
        <div className={`chatContainer${chatContainerClass}`}>
            <ul className="chatUL borderedSection">
                {messages.map(messageMapper).filter(x => x)}
            </ul>
            <input
                className="chatInput"
                onChange={e => onChange(e.target.value)}
                onKeyUp={e => {
                    if(e.key === 'Enter' && textInputValue)
                        onSubmit();
                }}
                placeholder="Chat"
                value={textInputValue}
            />
        </div>
    );
}

function messageMapper(message){
    switch (message.messageType){
    case MESSAGE_TYPE.LOBBY_CHAT_MESSAGE:
        return (
            <ChatMessage key={message.id} {...message} />
        );
    default:
    }
}
