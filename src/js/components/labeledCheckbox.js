import React from 'react';


export default function LabeledCheckbox({ label, value, onChange }){
    return (
        <div className="labeledCheckbox">
            <input
                checked={value}
                className="labeledInput"
                onChange={e => onChange(e.target.checked)}
                type="checkbox"
            />
            <span className="labeledModifierText">{label}</span>
        </div>
    );
}
