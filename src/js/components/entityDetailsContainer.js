import React from 'react';
import Button from './baseElements/button';


export default function EntityDetailsContainer({
    headerText, subheaderText, headerColor, imgName = 'citizen', items, deprecatedInputControls,
    onSubheaderClick, openEditor,
}){
    return (
        <div className="entityDetailsContainer">
            <div className="entityDetailsHeader">
                <img className="entityDetailsImg" src={`/rolepics/${imgName}.png`} alt="" />
                <div className="entityDetailsHeaderTextContainer">
                    <span style={{ color: headerColor }} className="entityDetailsHeaderText">
                        {headerText}
                    </span>
                    <span
                        className="entityDetailsSubheaderText"
                        onClick={onSubheaderClick}
                        style={{ color: headerColor }}
                    >
                        {subheaderText}
                    </span>
                </div>
            </div>
            <ul className="entityDetailsList">
                {items.map((item, index) => (
                    // eslint-disable-next-line react/no-array-index-key
                    <li key={index}>{item}</li>
                ))}
            </ul>
            {openEditor && (
                <Button
                    className="entityDetailsEditorButton"
                    onClick={openEditor}
                >
                    Edit <i className="fas fa-cogs" />
                </Button>
            )}
            {deprecatedInputControls}
        </div>
    );
}
