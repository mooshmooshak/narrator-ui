import React, { useEffect, useState } from 'react';


export default function LabeledNumberInput({ label, value, onChange }){
    const [displayedValue, setDisplayedValue] = useState(0);

    useEffect(() => {
        setDisplayedValue(value.toString());
    }, [value]);

    return (
        <div className="labeledCheckbox">
            <input
                className="labeledInput"
                onChange={e => {
                    if(e.target.value)
                        onChange(parseInt(e.target.value, 10));
                    setDisplayedValue(e.target.value);
                }}
                type="number"
                value={displayedValue}
            />
            <span className="labeledModifierText">{label}</span>
        </div>
    );
}
