import React from 'react';
import DoubleListSubContainer from './doubleListSubContainer';


export default function DoubleListContainer({ list1, list2 }){
    list1 = sortByText(list1);
    list2 = sortByText(list2);
    return (
        <div className="doubleListContainer">
            <DoubleListSubContainer label={list1.label} items={list1.items} />
            <DoubleListSubContainer label={list2.label} items={list2.items} />
        </div>
    );
}

function sortByText(list){
    const items = [...list.items].sort((i1, i2) => i1.text.localeCompare(i2.text));
    return {
        ...list,
        items,
    };
}
