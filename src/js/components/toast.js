import React from 'react';
import Alert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';

export default function Toast({
    isOpen, setClosed, type, text,
}){
    function handleClose(event, reason){
        if(reason === 'clickaway')
            return;

        setClosed();
    }

    return (
        <Snackbar open={isOpen} autoHideDuration={4000} onClose={handleClose}>
            <Alert severity={type} onClose={handleClose}>
                {text}
            </Alert>
        </Snackbar>
    );
}
