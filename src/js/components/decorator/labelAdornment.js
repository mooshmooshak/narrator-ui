import React from 'react';


export default function LabelAdornment({ className = '', label, children }){
    const containerClassName = className ? [className] : [];
    containerClassName.push('labelAdornmentContainer');

    return (
        <span className={containerClassName.join(' ')}>
            <div className="labelAdornmentLabel">
                {label}
            </div>
            {children}
        </span>
    );
}
