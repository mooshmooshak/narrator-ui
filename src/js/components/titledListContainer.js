import React from 'react';


export default function TitledListContainer({
    onItemClick, onActionIconClick, titleText, items, children, containerClassName = '',
}){
    return (
        <div className={`titledListContainer ${containerClassName}`}>
            <span className="titledListHeader">{titleText}</span>
            <ul className="titledListItems darkListAccent">
                {
                    items.map(item => (
                        <li
                            className={`${item.className || ''} titledListItem`}
                            onClick={() => onItemClick(item)}
                            key={item.id}
                        >
                            <span style={{ color: item.color }}>
                                { item.name }
                            </span>
                            { item.iconClass && (
                                <div className="titledListItemActionIconContainer">
                                    <i
                                        className={getIconClassName(item)}
                                        onClick={e => {
                                            e.stopPropagation();
                                            onActionIconClick(item);
                                        }}
                                    />
                                </div>
                            )}
                        </li>
                    ))
                }
            </ul>
            {children}
        </div>
    );
}

function getIconClassName({ iconClass }){
    return `fa titledListItemActionIcon fa fa-lg ${iconClass}`;
}
