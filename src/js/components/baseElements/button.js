import React from 'react';
import { Button as MaterialButton } from '@material-ui/core';


export default function Button({
    className = '', disabled, onClick, children, text, color = 'primary',
}){
    if(className)
        className += ' ';
    return (
        <MaterialButton
            className={`${className}narratorButton`}
            disabled={disabled}
            color={color}
            onClick={onClick}
            variant="contained"
        >
            {text || children}
        </MaterialButton>
    );
}
