import React from 'react';
import TextField from '@material-ui/core/TextField';


export default function TextInput({
    className, containerClassName, disabled, label, onBlur, onChange, onKeyUp, type = '', value,
}){
    function onInternalChange(newValue){
        if(type === 'number')
            return onChange(parseInt(newValue, 10));
        return onChange(newValue);
    }

    return (
        <div className={containerClassName}>
            <TextField
                className={className}
                disabled={disabled}
                label={label}
                onBlur={onBlur}
                onChange={e => onInternalChange(e.target.value)}
                onKeyUp={e => (onKeyUp ? onKeyUp(e.key) : undefined)}
                type={type}
                value={value}
            />
        </div>
    );
}
