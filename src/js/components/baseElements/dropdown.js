import React from 'react';
import {
    FormControl, InputLabel, MenuItem, Select,
} from '@material-ui/core';
import { KeyboardArrowDown } from '@material-ui/icons';


export default function Dropdown({
    className = '', disableUnderline, items, label, onChange, value,
}){
    const MenuProps = {
        // PaperProps: {
        //     style: {
        //         maxHeight: 280,
        //         width: 257,
        //         outlineStyle: 'solid',
        //         outlineWidth: 'thin',
        //         outlineColor: '#D8D8D8',
        //     },
        // },
        // getContentAnchorEl: null,
        // anchorOrigin: {
        //     vertical: 'bottom',
        //     horizontal: 'left',
        // },
    };

    return (
        <div className={className}>
            <FormControl className="dropdownFormControl">
                {!!label && (<InputLabel>{label}</InputLabel>)}
                <Select
                    value={value}
                    IconComponent={KeyboardArrowDown}
                    disableUnderline={disableUnderline}
                    MenuProps={MenuProps}
                    className="dropdown"
                    onChange={e => onChange(e.target.value)}
                >
                    {items.map(item => (
                        <MenuItem key={JSON.stringify(item.value)} value={item.value}>
                            {item.label}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        </div>
    );
}
