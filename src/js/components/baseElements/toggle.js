import React from 'react';
import LabelAdornment from '../decorator/labelAdornment';


export default function({
    className, disabled, items = ['OFF', 'ON'], label, onChange, isRightSelected,
}){
    const [leftText, rightText] = items;
    const leftSelected = !isRightSelected ? 'selectedToggle' : '';
    const rightSelected = isRightSelected ? 'selectedToggle' : '';

    const toggleContainerClassname = disabled ? [] : ['enabledToggleContainer'];
    toggleContainerClassname.push('toggleContainer');

    return (
        <LabelAdornment className={className} label={label}>
            <div className={toggleContainerClassname.join(' ')}>
                <div
                    className={`toggle ${leftSelected}`}
                    onClick={() => onChange(false)}
                >
                    {leftText}
                </div>
                <div
                    className={`toggle ${rightSelected}`}
                    onClick={() => onChange(true)}
                >
                    {rightText}
                </div>
            </div>
        </LabelAdornment>
    );
}
