import React from 'react';
import { connect } from 'react-redux';

import { onShowFactionAction } from '../actions/payloads/panelViewActions';


function FactionClickable({ faction, showFaction }){
    return (
        <span
            className="roleHover"
            onClick={() => showFaction(faction.id)}
            style={{ color: faction.color }}
        >
            {faction.name}
        </span>
    );
}

const mapStateToProps = () => ({});

const mapDispatchToProps = {
    showFaction: onShowFactionAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(FactionClickable);
