/* eslint-env browser */
const colors = require('./colors');


function getColor(hidden, setup){
    const { factionRoleMap, factionMap } = setup || window.gameState.setup;
    const factionRoleIDs = getFactionRoleIDs(hidden);
    const factionIDs = factionRoleIDs
        .map(factionRoleID => factionRoleMap[factionRoleID].factionID);
    const roleSet = new Set(factionIDs);
    if(roleSet.size === 1)
        return factionMap[factionIDs[0]].color;
    return colors.white;
}

function getFactionRoleIDs(hidden){
    return Array.from(new Set(hidden.spawns.map(spawn => spawn.factionRoleID)));
}

function getSpawnableFactionRoleIDs(hidden, playerCount){
    return hidden.spawns
        .filter(({ minPlayerCount, maxPlayerCount }) => playerCount >= minPlayerCount
            && playerCount <= maxPlayerCount)
        .map(({ factionRoleID }) => factionRoleID);
}

module.exports = {
    getColor,
    getFactionRoleIDs,
    getSpawnableFactionRoleIDs,
};
