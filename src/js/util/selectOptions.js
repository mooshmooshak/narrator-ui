module.exports = {
    RIGHT_REGULAR_SELECT: 'right_playerList_selected_red',
    LEFT_REGULAR_SELECT: 'left_playerList_selected',
    TARGETABLE_LEFT_DIV: 'left_playerList',
    TARGETABLE_RIGHT_DIV: 'right_playerList',
    WITCH_TARGET: 'right_playerList_selected_green',
};
