function getGradientColor(i, listLength){
    const mid = listLength / 2;

    let red;
    let green;
    if(i < mid){
        red = '255';
        green = parseInt((i * 255) / mid, 10);
    }else{
        i++;
        red = parseInt((((mid * 2) - i) * 255) / mid, 10);
        green = '255';
    }

    return `rgb(${red},${green},0)`;
}

module.exports = {
    getGradientColor,
};
