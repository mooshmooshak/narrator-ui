export function isModerator(userID, users){
    const user = users.find(u => u.id === userID);
    if(user)
        return user.isModerator;
    return false;
}
