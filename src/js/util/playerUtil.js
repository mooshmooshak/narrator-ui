/* eslint-env browser */


import { keyBy } from 'lodash';

const colors = [
    '#F3A4EE',
    '#f20af7',
    '#ed423f',
    '#ff7f21',
    '#ffba5c',
    '#e0e91f',
    '#2af4f7',
    '#CBE1A6',
    '#6cf700',
    '#35900C',
    '#31aaf7',
    '#0d55f7',
    '#B6A6F3',
    '#BD36F3',
    '#9E6D68',
    '#ADACAC',
];

const icons = [
    'fas fa-ambulance',
    'fab fa-android',
    'fas fa-balance-scale',
    'fas fa-band-aid',
    'fas fa-beer',
    'fas fa-binoculars',
    'fab fa-bitcoin',
    'fas fa-bug',
    'fas fa-bus',
    'fas fa-chess-knight',
    'fas fa-crow',
    'fas fa-glass-martini',
    'fas fa-eye', 'fas fa-fighter-jet', 'fas fa-fire',
    'fas fa-gamepad', 'fab fa-gitkraken', 'fab fa-grunt', 'fas fa-motorcycle', 'fas fa-paw',
    'fas fa-pills', 'fas fa-piggy-bank', 'fas fa-poo', 'fab fa-qq', 'fab fa-react', 'fas fa-robot',
    'fas fa-rocket', 'fas fa-snowflake', 'fas fa-chess-rook', 'fas fa-chess-pawn',
];

export function getPlayerColor(playerHash, gameID){
    gameID = gameID || window.gameState.gameID;
    const colorIndex = (gameID + playerHash) % colors.length;
    return colors[colorIndex];
}

export function getPlayerIcon(playerHash, gameID){
    gameID = gameID || window.gameState.gameID;
    const iconIndex = (gameID + playerHash) % icons.length;
    return icons[iconIndex];
}

export function getUserColor(gameID, userID){
    const colorIndex = (gameID + userID) % colors.length;
    return colors[colorIndex];
}

export function getUserIcon(gameID, userID){
    const iconIndex = (gameID + userID) % icons.length;
    return icons[iconIndex];
}

export function getPlayerName(userID, playerMap, users){
    playerMap = keyBy(Object.values(playerMap), 'userID');
    const player = playerMap[userID];
    if(player)
        return player.name;
    const userMap = keyBy(users, 'id');
    const user = userMap[userID];
    if(user)
        return user.name;
    return `@${userID}`;
}
