export function filterSetupHiddensByPlayerCount(playerCount, setupHiddens){
    return setupHiddens
        .filter(({ minPlayerCount, maxPlayerCount }) => playerCount >= minPlayerCount
            && playerCount <= maxPlayerCount);
}
