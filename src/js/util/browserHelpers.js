/* eslint-env browser, jquery */


export function equalStringLists(l1, l2){
    if(l1.length !== l2.length)
        return false;

    for(let i = 0; i < l1.length; i++)
        if(l1[i] !== l2[i])
            return false;


    return true;
}

export function flattenList(inputList, levels = 1){
    const result = inputList.reduce((acc, val) => acc.concat(val), []);
    if(levels >= 1)
        return flattenList(result, levels - 1);
    return result;
}

export function getURLParameters(){
    const parameters = {};
    let tmp = [];
    window.location.search
        .substr(1)
        .split('&')
        .forEach(item => {
            tmp = item.split('=');
            const key = tmp[0];
            let value = tmp[1];
            if(value === 'false')
                value = false;
            parameters[key] = value;
        });
    return parameters;
}

export function goToUrl(link){
    window.location.href = link;
}

export function hashStringToInt(str, seed){
    if(typeof(str) === 'number')
        return str;
    let hval = (seed === undefined) ? 0x811c9dc5 : seed;

    for(let i = 0; i < str.length; i++){
        hval ^= str.charCodeAt(i); // eslint-disable-line no-bitwise
        hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24); // eslint-disable-line no-bitwise
    }
    return hval >>> 0; // eslint-disable-line no-bitwise
}

export function intersection(set1, set2){
    const newSet = new Set();
    set1.forEach(elem => {
        if(set2.has(elem))
            newSet.add(elem);
    });
    return newSet;
}

export function isAlpha(str){
    return str.match(/^[a-zA-Z]+$/i) !== null;
}

export function isAlphaNumeric(str){
    return str.match(/^[a-zA-Z0-9]+$/i) !== null;
}

export function isLoggedIn(){
    return window.user && !window.user.isAnonymous;
}

export function isMobile(){
    return $('body').width() < 601;
}

export function isOnLobby(){
    const store = require('../reducers/store');
    return !store.default.getState().profileState.gameID;
}

export function log(err, message){
    if(!err){
        err = message;
        message = null;
    }

    console.log(err); // eslint-disable-line no-console
    if(message)
        console.log(message); // eslint-disable-line no-console
}

export function setPseudoURL(pseudoURL){
    window.history.pushState('', '', pseudoURL);
}

export function shuffleArray(array){
    let currentIndex = array.length; let temporaryValue; let
        randomIndex;

    // While there remain elements to shuffle...
    while (currentIndex !== 0){
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

export function storageEnabled(){
    return typeof(Storage) !== 'undefined';
}
