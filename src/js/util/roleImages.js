const setHelpers = require('./browserHelpers');


const AVAILABLE_PICTURES = new Set(['amnesiac', 'armorsmith', 'arsonist', 'assassin', 'baker',
    'blackmailer', 'bodyguard', 'citizen', 'cultist', 'cultleader', 'detective', 'doctor',
    'driver', 'electromaniac', 'executioner', 'framer', 'jailor', 'janitor', 'jester', 'lookout',
    'mafioso', 'massmurderer', 'mayor', 'poisoner', 'serialkiller', 'stripper', 'survivor',
    'tombstone', 'veteran', 'vigilante', 'witch']);


function contains(imageName){
    return AVAILABLE_PICTURES.has(imageName);
}

function intersection(set){
    return setHelpers.intersection(set, AVAILABLE_PICTURES);
}

module.exports = {
    contains,
    intersection,
};
