/* eslint-env jquery, browser */
/* global firebase */
const helpers = require('./browserHelpers');

function get(endpoint, headers){
    return userRequest(endpoint, undefined, headers, 'GET');
}

function post(endpoint, args, headers){
    return userRequest(endpoint, args, headers, 'POST');
}

function put(endpoint, args, headers){
    return userRequest(endpoint, args, headers, 'PUT');
}

function del(endpoint, args, headers){
    return userRequest(endpoint, args, headers, 'DELETE');
}

async function userRequest(endpoint, args, headers, method){
    const url = `/api/${endpoint}`;
    const async = true;


    if(args && typeof(args) === 'object')
        args = JSON.stringify(args);

    const request = new XMLHttpRequest();
    request.open(method, url, async);
    if(!headers){
        const authRequest = await getBrowserAuthRequest();
        request.setRequestHeader('auth', authRequest.token);
        request.setRequestHeader('authtype', authRequest.wrapperType);
    }

    if(helpers.getURLParameters().auth_token)
        request.setRequestHeader('auth_token', helpers.getURLParameters().auth_token);

    return new Promise(((resolve, reject) => {
        request.onload = function(){
            if(request.status !== 200 && request.status !== 204){
                if(request.status === 500)
                    helpers.log(request.responseText);
                let data;
                try{
                    data = JSON.parse(request.responseText);
                }catch(err){
                    data = request.responseText;
                }
                return reject(data);
            }
            if(request.responseText){
                const response = JSON.parse(request.responseText);
                resolve(response);
            }else{
                resolve();
            }
        };
        if(args)
            request.send(args);
        else
            request.send();
    }));
}

async function getBrowserAuthRequest(){
    const firebaseToken = await getToken();
    return {
        wrapperType: 'firebase',
        token: firebaseToken,
    };
}

function getToken(){
    if(typeof(firebase) === 'undefined')
        return '';
    return new Promise(resolve => {
        const unsubscribe = firebase.auth().onAuthStateChanged(user => {
            unsubscribe();
            if(!user)
                return resolve(null);
            user.getIdToken().then(idToken => {
                resolve(idToken);
            }, () => {
                resolve(null);
            });
        });
    });
}

module.exports = {
    get,
    post,
    put,
    delete: del,

    getBrowserAuthRequest,
    getToken,
};
