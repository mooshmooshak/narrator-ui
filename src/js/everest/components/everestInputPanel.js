import React, { useEffect, useState } from 'react';

import Dropdown from '../../components/baseElements/dropdown';
import TextInput from '../../components/baseElements/textInput';

import { getFeaturedSetups } from '../../services/setupService';


export default function EverestInputPanel({ onSetupChange, playerCount, setPlayerCount }){
    const [dropdownState, setDropdownState] = useState('');
    const [featuredSetups, setFeaturedSetups] = useState([]);
    const [inputValue, setInputValue] = useState(playerCount);

    async function handleChange(setupID){
        setDropdownState(setupID);
        onSetupChange(setupID);
    }

    useEffect(() => {
        getFeaturedSetups().then(fSetups => {
            setFeaturedSetups(fSetups);
            if(!fSetups.length)
                return;
            const setupID = fSetups[0].id;
            handleChange(setupID);
        });
    }, []);

    const featuredSetupItems = featuredSetups.map(s => ({
        label: s.name,
        value: s.id,
    }));

    return (

        <div className="everestInputContainer">
            <Dropdown
                className=""
                items={featuredSetupItems}
                label=""
                onChange={handleChange}
                value={dropdownState}
            />
            <TextInput
                type="outlined"
                value={inputValue}
                onChange={newInputValue => {
                    setInputValue(newInputValue);
                    const newValue = parseInt(newInputValue, 10);
                    if(Number.isInteger(newValue))
                        setPlayerCount(newValue);
                }}
            />
        </div>
    );
}
