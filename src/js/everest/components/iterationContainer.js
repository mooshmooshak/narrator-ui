import React from 'react';
import TitledListContainer from '../../components/titledListContainer';

export default function IterationContainer({ setup, iteration }){
    const setupHiddenIDOrder = setup.setupHiddens.map(sh => sh.id);
    iteration = [...iteration]
        .sort((i1, i2) => setupHiddenIDOrder.indexOf(i1.setupHiddenID)
            - setupHiddenIDOrder.indexOf(i2.setupHiddenID));
    const items = iteration.map(spawn => {
        const factionRole = setup.factionRoleMap[spawn.factionRoleID];
        return {
            name: factionRole.name,
            color: setup.factionMap[factionRole.factionID].color,
            id: spawn.setupHiddenID,
        };
    });
    return (
        <TitledListContainer
            items={items}
        />
    );
}
