import React, { useEffect } from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';

// import '../App.css'; place holder for when i do dynamic css

import { Route, Switch } from 'react-router-dom';

import ActionDialog from './components/actionDialog';
import HelpBanner from './landingWindow/components/helpBanner';
import LobbiesContainer from './landingWindow/components/lobbiesContainer';
import LobbyContainer from './lobbyWindow/components/lobbyContainer';
import LoginContainer from './landingWindow/components/loginContainer';
import CondorcetContainer from './condorcet/condorcetContainer';
import EverestContainer from './everest/everestContainer';


import { onGetFeaturedSetupsAction, onGetUserSetupsAction } from './actions/payloads/setupsActions';
import { onFirebaseUserUpdateAction, onWebSocketUpdate } from './actions/payloads/sessionActions';

import { isAlpha } from './util/browserHelpers';

import {
    getGame,
    getGameStateAndChat,
    recoverProfileWithAuthTokenInURL,
    recoverProfileWithPreviousLogIn,
} from './services/gameService';
import { joinByGameID } from './services/playerService';
import { get as getProfile } from './services/profileService';
import { getFeaturedSetups, getUserSetups } from './services/setupService';
/* eslint-env browser */


function App({
    onFirebaseUserUpdate,
    onFeaturedSetupsUpdate, onUserSetupsUpdate, gameID, userID, setWebSocketOpen,
}){
    useEffect(() => {
        loadProfile(onFirebaseUserUpdate);
    }, []);

    // in the future, might want to think about not loading this when in game mode
    useEffect(() => {
        loadSetups(userID, onFeaturedSetupsUpdate, onUserSetupsUpdate);
    }, [userID]);

    // do one time
    useEffect(() => {
        if(userID)
            connectWebSocket(setWebSocketOpen);
    }, [userID]);

    return (
        <>
            <Switch>
                <Route exact path="/condorcet" component={CondorcetContainer} />
                <Route exact path="/everest" component={EverestContainer} />
                <Route exact path="/signin" component={LoginContainer} />
                <Route path="/" component={gameID ? LobbyContainer : LobbiesContainer} />
            </Switch>
            <ActionDialog />
            <HelpBanner />
        </>
    );
}

const mapStateToProps = ({ session, profileState }) => ({
    gameID: profileState.gameID,
    userID: session.userID,
});

const mapDispatchToProps = {
    onFeaturedSetupsUpdate: onGetFeaturedSetupsAction,
    onFirebaseUserUpdate: onFirebaseUserUpdateAction,
    onUserSetupsUpdate: onGetUserSetupsAction,
    setWebSocketOpen: () => onWebSocketUpdate(true),
};

export default hot(module)(connect(mapStateToProps, mapDispatchToProps)(App));

async function loadProfile(onFirebaseUserUpdate){
    let profile = await recoverProfileWithAuthTokenInURL();
    if(!profile)
        profile = await recoverProfileWithPreviousLogIn();

    let game;
    if(profile && !profile.gameID && looksLikeGameID(window.location.pathname.substring(1)))
        game = await joinByGameID(window.location.pathname.substring(1));
    else if(profile && profile.gameID)
        game = await getGame(profile.gameID);

    if(game)
        return getGameStateAndChat(game, profile.gameID);

    await require('./services/authService').ensureLoggedIn();
    require('./index').goToGlobalLobbyPage();
    onFirebaseUserUpdate(!window.user.isAnonymous);
    return getProfile();
}

function loadSetups(userID, onFeaturedSetupsUpdate, onUserSetupsUpdate){
    if(userID === undefined)
        return;
    getFeaturedSetups()
        .then(onFeaturedSetupsUpdate);
    getUserSetups(userID)
        .then(onUserSetupsUpdate);
}

function connectWebSocket(setWebSocketOpen){
    require('./index')
        .connectWebSocket()
        .then(setWebSocketOpen);
}

function looksLikeGameID(url){
    if(!url)
        return false;
    return url.length === 4 && isAlpha(url);
}
