import React, { useEffect } from 'react';

import CodeGeneratorContainer from './components/codeGeneratorContainer';
import RankingsContainer from './components/rankingsContainer';
import StandingList from './components/standingList';

import store from '../reducers/store';
import { getURLParameters, shuffleArray } from '../util/browserHelpers';
import { onThreadLoad } from './condorcetActions';


export default function CondorcetContainer(){
    useEffect(() => {
        async function loadThreadData(){
            const channelService = require('../services/condorcetServices/channelService');
            const urlParams = getURLParameters();
            const { options, serverResponse, voterMetadata } = urlParams.thread
                ? await channelService.getVotes(urlParams.thread)
                : {
                    serverResponse: { response: urlParams.options.split(',').map(v => [v]) },
                    options: urlParams.options.split(','),
                    voterMetadata: getRandomVotes(urlParams.voters.split(','),
                        urlParams.options.split(',')),
                };
            store.dispatch(onThreadLoad({
                standing: serverResponse.response.length
                    ? serverResponse.response
                    : [options],
                options,
                voterMetadata,
            }));
        }
        loadThreadData();
    }, []);
    return (
        <>
            <StandingList />
            <RankingsContainer />
            <CodeGeneratorContainer />
        </>
    );
}

function getRandomVotes(voters, options){
    const allVotes = {};
    voters.forEach(voter => {
        const votes = options.filter(v => v !== voter);
        shuffleArray(votes);
        if(options.includes(voter))
            votes.push(voter);
        allVotes[voter] = { ranking: votes.map(v => [v]) };
    });
    return allVotes;
}
