import React from 'react';

import DraggableList from '../../components/draggableList';

import { getGradientColor } from '../../util/colorHelpers';
import { flattenList } from '../../util/browserHelpers';


export default function GradientList({ onNewOrder, standing }){
    const items = [];
    const itemCount = flattenList(standing).length;
    let seen = 0;
    standing.forEach(layer => {
        layer.forEach(playerName => {
            items.push({
                className: 'gradientChild',
                label: `${seen + 1}. ${playerName}`,
                styleOverrides: { color: getGradientColor(seen, itemCount) },
                value: playerName,
            });
        });
        seen += layer.length;
    });
    return (
        <DraggableList
            onNewOrder={onNewOrder}
            items={items}
        />
    );
}
