import React from 'react';
import { connect } from 'react-redux';

import GradientContainer from './gradientContainer';
import {
    onCodeGenClickAction,
    onNewStandingAction, onVoterRankingChangeAction,
} from '../condorcetActions';
import { getStanding } from '../../services/condorcetServices/channelService';


function RankingsContainer({
    onCodeGenClick, onNewOrder, onNewStanding, options, voterMetadata,
}){
    const voters = Object.keys(voterMetadata).filter(v => voterMetadata[v].ranking);
    voters.sort();
    return (
        <div className="rankingsContainer">
            {voters.map(voter => (
                <GradientContainer
                    buttonClassName="rankingButton"
                    containerClassName="rankingChildContainer"
                    key={voter}
                    onButtonClick={() => onCodeGenClick(voter)}
                    standing={voterMetadata[voter].ranking}
                    title={`${voter}'s Ranking`}
                    onNewOrder={async newRanking => {
                        onNewOrder(voter, newRanking);
                        const newStanding = await getStanding({
                            ...voterMetadata,
                            [voter]: { ranking: newRanking.map(item => [item]) },
                        }, options);
                        onNewStanding(newStanding.response);
                    }}
                />
            ))}
        </div>
    );
}

const mapStateToProps = state => state.rankings;

const mapDispatchToProps = {
    onCodeGenClick: onCodeGenClickAction,
    onNewOrder: onVoterRankingChangeAction,
    onNewStanding: onNewStandingAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(RankingsContainer);
