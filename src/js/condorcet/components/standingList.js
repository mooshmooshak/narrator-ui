import React from 'react';
import { connect } from 'react-redux';

import GradientContainer from './gradientContainer';


function StandingList({ standing }){
    const title = 'Current Consensus';
    return (
        <GradientContainer
            containerClassName="standingsContainer"
            standing={standing}
            title={title}
        />
    );
}

const mapStateToProps = ({ rankings }) => rankings;

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(StandingList);
