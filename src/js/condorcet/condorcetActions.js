export const CondorcetActions = {
    ON_CODE_GEN_CLICK: 'ON_CODE_GEN_CLICK',
    ON_NEW_STANDING: 'ON_NEW_STANDING',
    ON_NEW_ORDER: 'ON_NEW_ORDER',
    ON_THREAD_LOAD: 'ON_THREAD_LOAD',
};

export function onCodeGenClickAction(voter){
    return {
        type: CondorcetActions.ON_CODE_GEN_CLICK,
        payload: voter,
    };
}

export function onThreadLoad(threadData){
    return {
        type: CondorcetActions.ON_THREAD_LOAD,
        payload: threadData,
    };
}

export function onVoterRankingChangeAction(voter, newOrder){
    return {
        type: CondorcetActions.ON_NEW_ORDER,
        payload: { voter, newOrder },
    };
}

export function onNewStandingAction(standing){
    return {
        type: CondorcetActions.ON_NEW_STANDING,
        payload: standing,
    };
}
