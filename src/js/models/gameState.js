const { get } = require('lodash');

const { PHASE_NAME } = require('../util/constants');
/* eslint-env jquery */


function GameState(userID){
    this.scrollToBottom = true;

    this.userID = userID;
    this.moderatorIDs = new Set();

    this.started = false;
    this.commandsIndex = 0;
    this.timer = null;
    this.isAlive = true;
    this.isObserver = false;

    this.activeTeams = [];

    this.deadPlayers = [];

    this.chats = {};
    this.chatKeys = {};
    this.unreadChats = {};

    this.voteInfo = {};

    this.activeUserIDs = new Set();
    this.setup = null;
}

GameState.prototype.endedNight = function(){
    // ideally checking if the name is in the playerMap would NEVER happen.  TODO test refactor
    return this.profile && this.playerMap && this.playerMap[this.profile.name]
        && this.playerMap[this.profile.name].endedNight;
};

GameState.prototype.isDay = function(){
    return this.isStarted && this.phase && this.phase.name !== 'NIGHT_ACTION_SUBMISSION'
        && !this.isOver();
};

GameState.prototype.isHost = function(){
    return this.moderatorIDs.has(this.userID);
};

GameState.prototype.isOver = function(){
    return this.isStarted && this.phase && this.phase.name === PHASE_NAME.FINISHED;
};

GameState.prototype.getSetupModifierValue = function(ruleName){
    const modifiers = get(this, ['setup', 'modifiers'], []);
    const modifier = modifiers.find(m => m.name === ruleName);
    return modifier && modifier.value;
};

GameState.prototype.getGameModifierValue = function(ruleName){
    return this.modifiers
        && this.modifiers[ruleName].value;
};

module.exports = {
    GameState,
};
