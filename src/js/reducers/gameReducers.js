import { keyBy, omit } from 'lodash';
import { GameActions } from '../actions/payloads/gameActions';
import { ProfileActions } from '../actions/payloads/profileActions';

const initialState = {
    activeUserIDs: new Set(),
    modifiers: {},
    playerMap: {},
    phase: {},
    users: [],
    voteInfo: {
        allowedTargets: [],
        voterToVotes: {},
    },
};

export default function(state = initialState, action){
    switch (action.type){
    case GameActions.ON_GAME_MODIFIER_UPDATE:
        return {
            ...state,
            modifiers: {
                ...state.modifiers,
                [action.payload.name]: {
                    ...state.modifiers[action.payload.name],
                    value: action.payload.value,
                },
            },
        };
    case GameActions.ON_GAME_UPDATE:
        return {
            ...state,
            ...action.payload,
            playerMap: keyBy(action.payload.players, 'name'),
        };
    case GameActions.ON_GAME_USER_ACTIVITY_CHANGE:
        return {
            ...state,
            activeUserIDs: handleGameUserActivityChange(state.activeUserIDs, action.payload),
        };
    case GameActions.ON_GAME_USER_ACTIVITY_LOAD:
        return {
            ...state,
            activeUserIDs: action.payload,
        };
    case GameActions.ON_HOST_UPDATE:
        return {
            ...state,
            users: state.users.map(user => ({
                ...user,
                isModerator: action.payload.has(user.id),
            })),
        };
    case GameActions.ON_PLAYERS_UPDATE:
        return {
            ...state,
            playerMap: {
                ...state.playerMap,
                ...keyBy(action.payload, 'name'),
            },
        };
    case GameActions.ON_PLAYER_EXIT:
        return onPlayerExit(state, action.payload);
    case GameActions.ON_PLAYER_NAME_UPDATE:
        return {
            ...state,
            playerMap: updatePlayerName(state.playerMap,
                action.payload.playerID, action.payload.playerName),
        };
    case GameActions.ON_TRIAL_START:
        return {
            ...state,
            voteInfo: {
                ...state.voteInfo,
                trialedPlayerName: action.payload.trialedUser.name,
            },
        };
    case GameActions.ON_VOTE_DELETE:
        return {
            ...state,
            voteInfo: {
                ...state.voteInfo,
                voterToVotes: {
                    ...state.voteInfo.voterToVotes,
                    [action.payload]: [],
                },
            },
        };
    case GameActions.ON_VOTE_UPDATE:
        return {
            ...state,
            voteInfo: {
                ...state.voteInfo,
                voterToVotes: {
                    ...state.voteInfo.voterToVotes,
                    [action.payload.voterName]: action.payload.voteTargets,
                },
            },
        };
    case GameActions.ON_VOTES_UPDATE:
        return {
            ...state,
            voteInfo: action.payload,
        };
    case ProfileActions.LOBBY_LEAVE:
        return initialState;
    default:
        return state;
    }
}

function handleGameUserActivityChange(activeUserIDs, { isActive, userID }){
    activeUserIDs = new Set([...activeUserIDs]);
    if(isActive)
        activeUserIDs.add(userID);
    else
        activeUserIDs.delete(userID);
    return activeUserIDs;
}

function updatePlayerName(playerMap, playerID, playerName){
    const oldPlayer = Object.values(playerMap).find(p => p.id === playerID);
    const filteredPlayerMap = omit(playerMap, oldPlayer.name);
    return {
        ...filteredPlayerMap,
        [playerName]: {
            ...oldPlayer,
            name: playerName,
        },
    };
}

function onPlayerExit(state, playerName){
    const oldPlayer = state.playerMap[playerName];
    return {
        ...state,
        playerMap: omit(state.playerMap, playerName), // playerName
        users: state.users.filter(user => !oldPlayer || user.id !== oldPlayer.userID),
    };
}
