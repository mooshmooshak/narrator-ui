import { DialogActions } from '../actions/payloads/dialogActions';

const initialState = {
    description: undefined,
    isVisible: false,
    onConfirm: () => {},
    title: undefined,
};

export default function actionDialogReducer(state = initialState, action){
    switch (action.type){
    case DialogActions.SHOW_ACTION_DIALOG:
        return { ...state, ...action.payload, isVisible: true };
    case DialogActions.HIDE_ACTION_DIALOG:
        return initialState;
    default:
        return state;
    }
}
