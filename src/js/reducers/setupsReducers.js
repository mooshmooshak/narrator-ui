import { SetupsActions } from '../actions/payloads/setupsActions';

const initialState = {
    featuredSetups: [],
    userSetups: [],
};

export default function setupsReducer(state = initialState, action){
    switch (action.type){
    case SetupsActions.ON_GET_FEATURED_SETUPS:
        return {
            ...state,
            featuredSetups: action.payload,
        };
    case SetupsActions.ON_GET_USER_SETUPS:
        return {
            ...state,
            userSetups: action.payload,
        };
    default:
        return state;
    }
}
