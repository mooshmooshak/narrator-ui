import { omit } from 'lodash';

import { GameActions } from '../actions/payloads/gameActions';
import { ProfileActions } from '../actions/payloads/profileActions';


const initialState = {};

export default function profileReducer(state = initialState, { payload, type }){
    switch (type){
    case GameActions.ON_GAME_UPDATE:
        return {
            ...state,
            gameID: payload.id,
        };
    case ProfileActions.ACTIONS_UPDATE:
        return {
            ...state,
            roleCard: {
                ...state.roleCard,
                actions: payload,
            },
        };
    case ProfileActions.ACTION_REMOVE:
        return {
            ...state,
            roleCard: {
                actions: state.roleCard.actions.filter((a, index) => index !== payload),
            },
        };
    case ProfileActions.LOBBY_LEAVE:
        return {
            ...omit(state, 'gameID'),
        };
    case ProfileActions.UPDATE_PROFILE:
        return {
            ...state,
            ...payload,
        };
    default:
        return state;
    }
}
