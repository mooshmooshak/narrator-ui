import React from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';

import NavBarItem from '../../components/nav/navBarItem';

import { DISCORD_LINK, FORUMS_LINK } from '../../util/constants';
import { goToUrl } from '../../util/browserHelpers';

import { logout } from '../../services/authService';

import { onFirebaseUserUpdateAction } from '../../actions/payloads/sessionActions';


function LandingNavBarItems({ closeDrawer, isLoggedIn, onSignOut }){
    return (
        <>
            <NavBarItem iconClassName="fa fa-comments" onClick={() => goToUrl(FORUMS_LINK)}>
                Forums
            </NavBarItem>
            <NavBarItem iconClassName="fab fa-discord" onClick={() => goToUrl(DISCORD_LINK)}>
                Discord
            </NavBarItem>
            {getSignInItem(closeDrawer, isLoggedIn, onSignOut)}
        </>
    );
}

const mapStateToProps = ({ session }) => ({
    isLoggedIn: session.isLoggedIn,
});

const mapDispatchToProps = {
    onSignOut: () => onFirebaseUserUpdateAction(false),
};

export default connect(mapStateToProps, mapDispatchToProps)(LandingNavBarItems);

function getSignInItem(closeDrawer, isLoggedIn, onSignOut){
    const history = useHistory();

    function goToSignIn(){
        history.push('/signin');
        closeDrawer();
    }

    async function signOut(){
        await logout();
        onSignOut();
        closeDrawer();
    }

    const className = isLoggedIn ? 'fas fa-sign-out-alt' : 'fa fa-sign-in-alt';
    const onClick = isLoggedIn ? signOut : goToSignIn;
    const text = isLoggedIn ? 'Log Out' : 'Log In';

    return (
        <NavBarItem
            iconClassName={className}
            onClick={onClick}
        >
            {text}
        </NavBarItem>
    );
}
