import React from 'react';
import ChatContainer from '../../components/chat/chatContainer';
import GlobalChatHeader from './globalChatHeader';

export default function GlobalChatContainer({ openGlobalUsers }){
    return (
        <div className="globalChatContainer homePageContainer">
            <GlobalChatHeader openGlobalUsers={openGlobalUsers} />
            <ChatContainer
                className="globalChat"
                onChange={() => {}}
                onSubmit={() => {}}
                messages={[]}
            />
        </div>
    );
}
