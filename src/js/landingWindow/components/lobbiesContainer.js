import React, { useState } from 'react';
import { connect } from 'react-redux';

import { Modal } from '@material-ui/core';

import Button from '../../components/baseElements/button';
import NavBar from '../../components/nav/navBar';

import GameCreateContainer from './gameCreate/gameCreateContainer';
import GlobalChatContainer from './globalChatContainer';
import GlobalUsers from './globalUsers';
import LandingNavBarItems from './landingNavBarItems';
import LobbiesTable from './lobbiesTable';

import { createSetup } from '../../services/setupService';


function LobbiesContainer({ setups, userName }){
    const [isCreatGameModelOpen, setIsCreateGameModalOpen] = useState(false);
    const [isUserModelOpen, setIsUserModalOpen] = useState(false);
    return (
        <>
            <NavBar headerText="Lobbies">
                <LandingNavBarItems />
            </NavBar>
            <div className="lobbiesContainer fullPageContainer">
                <Button
                    className="createGameButton"
                    onClick={async() => {
                        if(!setups.length)
                            await createSetup();
                        setIsCreateGameModalOpen(true);
                    }}
                    text="Create Game"
                />
                <LobbiesTable />
                {false
            && (
                <div className="globalChatUserContainer">
                    <GlobalChatContainer openGlobalUsers={() => setIsUserModalOpen(true)} />
                    <GlobalUsers />
                </div>
            )}
                <Modal open={isUserModelOpen} onClose={() => setIsUserModalOpen(false)}>
                    <div className="globalUsersPositionContainer">
                        <GlobalUsers />
                    </div>
                </Modal>
                <Modal
                    className="gameCreatorPositionContainer"
                    open={isCreatGameModelOpen}
                    onClose={() => setIsCreateGameModalOpen(false)}
                >
                    <div className="gameCreatorLayoutContainer">
                        <GameCreateContainer setups={setups} userName={userName} />
                    </div>
                </Modal>
            </div>
        </>
    );
}

const mapStateToProps = ({ setupsState, session }) => ({
    setups: [...setupsState.userSetups, ...setupsState.featuredSetups],
    userName: session.userName,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(LobbiesContainer);
