import React from 'react';

import { TableRow, TableCell } from '@material-ui/core';

import { isFull, LOBBY_TABLE_DEFINITION } from '../util/lobbyUtil';
import { PHASE_NAME } from '../../util/constants';


export default function LobbyRow({ game, joinAsPlayer }){
    return (
        <TableRow className={getColorClassName(game)}>
            { LOBBY_TABLE_DEFINITION(joinAsPlayer).map(([header, cellFunc, isWideHeader]) => (
                <TableCell
                    className={isWideHeader ? 'lobbyExtraColumns' : ''}
                    key={header}
                >
                    {cellFunc(game)}
                </TableCell>
            ))}
        </TableRow>
    );
}

function getColorClassName(game){
    if(game.phase.name === PHASE_NAME.FINISHED)
        return 'lobbyRowFinished';
    if(game.isStarted)
        return 'lobbyRowStarted';
    if(isFull(game))
        return 'lobbyRowUnstartedFull';
    return 'lobbyRowUnstarted';
}
