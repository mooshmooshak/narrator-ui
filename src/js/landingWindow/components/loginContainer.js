import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';

import Button from '../../components/baseElements/button';
import TextInput from '../../components/baseElements/textInput';

import NavBar from '../../components/nav/navBar';

import LandingNavBarItems from './landingNavBarItems';

import { createUserWithEmailAndPassword, loginWithEmail } from '../../services/authService';
import { onFirebaseUserUpdateAction } from '../../actions/payloads/sessionActions';


function LoginContainer({ isLoggedIn, onSignIn }){
    const history = useHistory();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [errorText, setErrorText] = useState('');

    useEffect(() => {
        if(isLoggedIn)
            history.push('/');
    }, [isLoggedIn]);

    function login(){
        loginWithEmail(username, password)
            .then(onSignIn)
            .catch(setErrorText);
    }

    function createAccount(){
        if(!username || !password)
            return;
        createUserWithEmailAndPassword(username, password)
            .then(onSignIn)
            .catch(setErrorText);
    }

    function submitLogin(key){
        if(key === 'Enter')
            login();
        return true;
    }

    return (
        <>
            <NavBar headerText="Login">
                <LandingNavBarItems />
            </NavBar>
            <div className="loginContainer fullPageContainer">
                <div className="loginInputsContainer">
                    <TextInput
                        containerClassName="loginInput"
                        label="Email"
                        onChange={setUsername}
                        onKeyUp={submitLogin}
                        value={username}
                    />
                    <TextInput
                        containerClassName="loginInput"
                        label="Password"
                        onChange={setPassword}
                        onKeyUp={submitLogin}
                        type="password"
                        value={password}
                    />
                    {errorText && (<span className="loginErrorText">{errorText}</span>)}
                    <Button
                        className="loginButton"
                        onClick={login}
                        text="Log In"
                    />
                    <hr />
                    <Button
                        className="signUpButton"
                        onClick={createAccount}
                        text="Create New Account"
                    />
                </div>
            </div>
        </>
    );
}

const mapStateToProps = ({ session }) => ({
    isLoggedIn: session.isLoggedIn,
});

const mapDispatchToProps = {
    onSignIn: () => onFirebaseUserUpdateAction(true),
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
