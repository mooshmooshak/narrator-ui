import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import {
    TableRow, TableCell, TableBody, TableHead, Table, TableContainer,
} from '@material-ui/core';
import { getOnlineUsers } from '../../services/userService';


function GlobalUsers({ isWebSocketConnected, userID }){
    const [usersData, setUsers] = useState();
    const users = usersData || [];
    useEffect(() => {
        if(userID !== undefined && !usersData && isWebSocketConnected)
            getOnlineUsers().then(setUsers);
    }, [usersData, userID, isWebSocketConnected]);
    return (
        <div className="globalUsersLayoutContainer homePageContainer">
            <span className="globalUsersHeader">Online Users ({users.length})</span>
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Status</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {users.map(user => (
                            <TableRow key={user.id}>
                                <TableCell>{user.name}</TableCell>
                                <TableCell>Online</TableCell>
                            </TableRow>
                        ))}

                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}

const mapStateToProps = ({ session }) => ({
    isWebSocketConnected: session.isWebSocketConnected,
    userID: session.userID,
});

export default connect(mapStateToProps)(GlobalUsers);
