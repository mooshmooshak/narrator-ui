import React, { useEffect, useState } from 'react';

import Dropdown from '../../../components/baseElements/dropdown';


const IRL_FORMAT = 'irl';
const VIDEO_FORMAT = 'video';
const TEXT_FORMAT = 'text';

const FORMAT_OPTIONS = [{
    label: 'Text',
    value: TEXT_FORMAT,
}, {
    label: 'Video',
    value: VIDEO_FORMAT,
}, {
    label: 'In person',
    value: IRL_FORMAT,
}];

export default function GameFormatDropdown({ className = '', onChange, value }){
    const [dropdownValue, setDropdownValue] = useState(TEXT_FORMAT);
    useEffect(() => {
        setDropdownValue(value ? TEXT_FORMAT : IRL_FORMAT);
    }, [value]);

    function onInternalChange(newValue){
        onChange(newValue === TEXT_FORMAT);
        setDropdownValue(newValue);
    }

    return (
        <Dropdown
            className={className}
            label="Format"
            items={FORMAT_OPTIONS}
            onChange={onInternalChange}
            value={dropdownValue}
        />
    );
}
