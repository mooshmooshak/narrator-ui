import React, { useEffect, useState } from 'react';

import Button from '../../../components/baseElements/button';

import SetupDropdown from './setupDropdown';
import GameFormatDropdown from './gameFormatDropdown';
import LengthDropdown, { GAME_LENGTH_OPTIONS } from './lengthDropdown';
import TextInput from '../../../components/baseElements/textInput';

import { ensureLoggedIn } from '../../../services/authService';
import { createGame } from '../../../services/gameService';
import { upsert as upsertGameModifier } from '../../../services/gameModifierService';


export default function GameCreateContainer({ setups, userName = '' }){
    const defaultSetupID = setups.length ? setups[0].id : '';
    const [gameOptions, setGameOptions] = useState({
        format: false,
        gameLengthKey: {
            dayLength: GAME_LENGTH_OPTIONS[0].dayLength,
            nightLength: GAME_LENGTH_OPTIONS[0].nightLength,
        },
        // minPlayerCount: 3,
        // maxPlayerCount: 50,
        setupID: defaultSetupID,
    });
    const [hostName, setHostName] = useState();
    useEffect(() => {
        setGameOptions({ ...gameOptions, setupID: defaultSetupID });
    }, [defaultSetupID]);
    useEffect(() => {
        setHostName(userName);
    }, [userName]);

    async function hostGame(){
        await ensureLoggedIn();
        await createGame(hostName, gameOptions.setupID);
        const { dayLength, nightLength } = gameOptions.gameLengthKey;
        return Promise.all([
            upsertGameModifier('DAY_LENGTH_START', dayLength),
            upsertGameModifier('NIGHT_LENGTH', nightLength),
            upsertGameModifier('CHAT_ROLES', gameOptions.format),
        ]);
    }

    function updateGameOptions(mergedValue){
        setGameOptions({
            ...gameOptions,
            ...mergedValue,
        });
    }

    return (
        <>
            <span className="gameCreatorLayoutHeader">Game Settings</span>
            {/* <div className="gameCreatorItem"> */}
            {/* <span>Player Count: </span> */}
            {/* <div className="gameCreatePlayerCountContainer"> */}
            {/*    <TextInput */}
            {/*        className="gameCreatePlayerCount" */}
            {/*        value={gameOptions.minPlayerCount} */}
            {/*    /> */}
            {/*    <span>-</span> */}
            {/*    <TextInput */}
            {/*        className="gameCreatePlayerCount" */}
            {/*        value={gameOptions.maxPlayerCount} */}
            {/*    /> */}
            {/* </div> */}
            {/* </div> */}
            <SetupDropdown
                className="gameCreatorItem"
                onChange={setupID => updateGameOptions({ setupID })}
                setups={setups}
                setupID={gameOptions.setupID}
            />
            <LengthDropdown
                className="gameCreatorItem"
                dayLength={gameOptions.gameLengthKey.dayLength}
                nightLength={gameOptions.gameLengthKey.nightLength}
                setLengthState={gameLengthKey => updateGameOptions({ gameLengthKey })}
            />
            <GameFormatDropdown
                className="gameCreatorItem"
                onChange={format => updateGameOptions({ format })}
                value={gameOptions.format}
            />
            <TextInput
                containerClassName="gameCreatorItem"
                className="gameCreatorItem"
                label="Player name"
                onChange={setHostName}
                value={hostName === undefined ? userName : hostName}
            />
            <Button onClick={hostGame} className="gameCreateSubmit">Create Game</Button>
        </>
    );
}
