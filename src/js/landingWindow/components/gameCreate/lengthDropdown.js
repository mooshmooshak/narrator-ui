import React from 'react';

import Dropdown from '../../../components/baseElements/dropdown';


const TWO_MINUTES = 60 * 2;
const FIVE_MINUTES = 60 * 5;
const FOUR_MINUTES = 60 * 4;
const TEN_MINUTES = 10 * 60;
const ONE_DAY = 60 * 60 * 24;
const TWO_DAYS = ONE_DAY * 2;

export const GAME_LENGTH_OPTIONS = [{
    label: '5min / 2min',
    dayLength: FIVE_MINUTES,
    nightLength: TWO_MINUTES,
}, {
    label: '10min / 4min',
    dayLength: TEN_MINUTES,
    nightLength: FOUR_MINUTES,
}, {
    label: '48hr / 24hr',
    dayLength: TWO_DAYS,
    nightLength: ONE_DAY,
}].map(({ label, dayLength, nightLength }) => ({
    label,
    dayLength,
    nightLength,
    value: `${dayLength},${nightLength}`,
}));

function splitDayNight(value){
    const [dayLength, nightLength] = value.split(',').map(length => parseInt(length, 10));
    return { dayLength, nightLength };
}

export default function LengthDropdown({
    className = '', setLengthState, dayLength, nightLength,
}){
    const option = GAME_LENGTH_OPTIONS
        .find(o => o.dayLength === dayLength && o.nightLength === nightLength);
    const value = option ? option.value : '';
    return (
        <Dropdown
            className={className}
            label="Length"
            items={GAME_LENGTH_OPTIONS}
            onChange={e => setLengthState(splitDayNight(e))}
            value={value}
        />
    );
}
