import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import {
    TableContainer, Table, TableBody, TableRow, TableCell, TableHead,
} from '@material-ui/core';
import LobbyRow from './lobbyRow';

import { getGames as getLobbies } from '../../services/gameService';

import { joinAsPlayerAction } from '../../actions/gameUserActions';
import { onLobbiesLoadAction } from '../../actions/payloads/lobbyActions';

import { LOBBY_TABLE_DEFINITION } from '../util/lobbyUtil';
import { PHASE_NAME } from '../../util/constants';


function LobbiesTable({
    joinAsPlayer, lobbies, setLobbies, userName,
}){
    useEffect(() => {
        getLobbies().then(setLobbies);
    }, []);

    const sortedLobbies = [...lobbies].sort(gameSort);

    return (
        <TableContainer className="lobbiesTable homePageContainer">
            <Table className="lobbiesTableColor">
                <TableHead>
                    <TableRow>
                        {LOBBY_TABLE_DEFINITION(joinAsPlayer)
                            .map(([headerName, , isWideHeader], index) => (
                                <TableCell
                                    // eslint-disable-next-line react/no-array-index-key
                                    key={index}
                                    className={isWideHeader ? 'lobbyExtraColumns' : ''}
                                >
                                    {headerName}
                                </TableCell>
                            ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {sortedLobbies.map(lobby => (
                        <LobbyRow
                            joinAsPlayer={() => joinAsPlayer(lobby.lobbyID, userName)}
                            game={lobby}
                            key={lobby.id}
                        />
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

const mapStateToProps = ({ lobbiesState, session }) => ({
    lobbies: lobbiesState.lobbies,
    userName: session.userName,
});

const mapDispatchToProps = {
    setLobbies: onLobbiesLoadAction,
    joinAsPlayer: joinAsPlayerAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(LobbiesTable);

function gameSort(g1, g2){
    if(g1.phase.name === g2.phase.name)
        return 0;
    if(g1.phase.name === PHASE_NAME.FINISHED)
        return 1;
    if(g2.phase.name === PHASE_NAME.FINISHED)
        return -1;
    if(g1.phase.name !== PHASE_NAME.UNSTARTED)
        return 1;
    if(g2.phase.name !== PHASE_NAME.UNSTARTED)
        return -1;
}
