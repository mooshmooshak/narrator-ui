/* eslint-env browser, jquery */
/* global SimpleBar */
const helpers = require('../util/browserHelpers');
const playerUtil = require('../util/playerUtil');


function fillEleWithMessage(li, message, content, gameID){
    if(message.messageType === 'ChatMessage' || helpers.isOnLobby()){
        const playerHash = helpers.hashStringToInt(message.speakerName || '',
            window.gameState.gameID);
        const color = playerUtil.getPlayerColor(playerHash, gameID);
        const fpic = $('<i>').addClass('chatPic');
        fpic.css('color', color);

        const speaker = $('<span>').addClass('speakerLabel');
        speaker.html(message.sender);
        speaker.find('font').css('color', color);

        li.attr('name', speaker.text());
        const icon = playerUtil.getPlayerIcon(playerHash, gameID);
        fpic.addClass(icon);

        const mText = $('<span>').addClass('speakerText');
        mText.html(message.text);

        if(!content || content.children().last().attr('name') !== speaker.text()){
            li.append(fpic);
            li.append(speaker);
        }
        li.append(mText);
    }else{
        li.append($('<span>').html(message.text));
    }
}

function hideChatElements(){
    if(helpers.isMobile()){
        $('#chat_tabs_wrapper').hide();
        $('.chat_pane').hide();
        $('#puppetList').hide();
        hideChatInput();
        if(helpers.isLoggedIn() || !helpers.isOnLobby()){
            $('.chat_ticker').show();
            $('#chat_ticker_pane').show();
        }
    }
}

function hideChatInput(){
    $('body').removeClass('chat_active puppetsShowing');
    $('#lobbyChatPane').hide();
}

function refresh(){
    const { chatQueue } = require('../services/chatService');
    if(!chatQueue.length)
        return setTimeout(refresh, 30);

    const chatUL = $('#messages');

    const sl = new SimpleBar(chatUL[0]);
    const content = $(sl.getContentElement());
    const scrollElement = $(sl.getScrollElement());

    function chatScroll(){
        const liHeight = chatUL.find('li').last().height();
        const scrollTop = $(this).scrollTop();
        // eslint-disable-next-line max-len
        window.gameState.scrollToBottom = scrollTop + $(this).innerHeight() + liHeight >= this.scrollHeight;
    }

    scrollElement.unbind();

    let wasDay = null;
    let lastDay;
    for(let i = 0; i < chatQueue.length; i++){
        let message = chatQueue[i];
        const messageType = message.messageType;

        if(lastDay === undefined || lastDay !== message.day || wasDay !== message.isDay){
            lastDay = message.day;
            wasDay = message.isDay;
            let headerText;
            if(wasDay)
                headerText = 'Day';
            else
                headerText = 'Night';
            headerText += ' ';
            if(messageType === 'DeathAnnouncement' && message.nightDeath)
                headerText += (lastDay - 1);
            else
                headerText += lastDay;
            if(!hasHeaderText(content, headerText) && headerText !== 'Day 0'){
                message = {
                    text: `<div class='headerLabel'><u>${headerText}</u></div>`,
                };
                i--;
            }
        }

        const li = $('<li>');

        fillEleWithMessage(li, message, content);

        content.append(li);
    }
    sl.recalculate();
    chatQueue.length = 0;

    if(window.gameState.scrollToBottom){
        const element = scrollElement[0];
        if(element)
            element.scrollTop = element.scrollHeight;
    }

    setTimeout(() => {
        scrollElement.on('scroll', chatScroll);
        setTimeout(refresh, 30);
    }, 10);
}

function setScrolledToBottom(){
    window.gameState.scrollToBottom = true;
}

module.exports = {
    fillEleWithMessage,
    hideChatElements,
    hideChatInput,
    refresh,
    setScrolledToBottom,
};

function hasHeaderText(ul, text){
    let hasText = false;
    ul.children('li').each(function(){
        if($(this).text() === text)
            hasText = true;
    });
    return hasText;
}
