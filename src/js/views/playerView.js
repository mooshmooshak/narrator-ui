const targetables = {};

function getTargetable(name){
    return targetables[name];
}

function saveTargetable(targetable){
    targetables[targetable.player.name] = targetable;
}

module.exports = {
    getTargetable,
    saveTargetable,
};
