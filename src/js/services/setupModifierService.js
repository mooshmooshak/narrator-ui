import store from '../reducers/store';
import { onSetupModifierUpdateAction } from '../actions/payloads/setupActions';
import { MAX_PLAYER_COUNT } from '../util/constants';

const requests = require('../util/browserRequests');


export async function upsert(name, value){
    const args = {
        name, value, minPlayerCount: 0, maxPlayerCount: MAX_PLAYER_COUNT,
    };

    const browserResponse = await requests.post('setupModifiers', args);
    name = browserResponse.response.name;
    value = browserResponse.response.value;
    store.dispatch(onSetupModifierUpdateAction(name, value));
}
