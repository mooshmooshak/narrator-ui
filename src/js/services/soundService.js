/* eslint-env browser, jquery */
/* global responsiveVoice */
const helpers = require('../util/browserHelpers');


let cypressTesting = false;

function defaultSound(key){
    if(noSound())
        return false;
    if(helpers.isOnLobby())
        return false;

    const val = getSettingsValue(key);
    if(val !== null)
        return val;

    if(window.gameState.isPrivateInstance && helpers.isMobile())
        return window.gameState.isHost() && key !== 'chat_ping';
    return true;
}

function isPlaying(){
    return responsiveVoice.isPlaying();
}

function play(){
    if(!defaultSound('music_enabled') && !window.gameState.started)
        return pause();
    if(!defaultSound('day_music_enabled') && window.gameState.isDay)
        return pause();
    if(!defaultSound('night_music_enabled') && !window.gameState.isDay)
        return pause();

    const playEle = $('#backgroundMusic');
    const attrSrc = playEle.attr('src');
    if(window.gameState.isDay() && attrSrc.includes('nighttime')){
        playEle.attr('src', '/audio/daytime.mp3');
        playEle.prop('volume', 0.3);
    }else if(!window.gameState.isDay() && attrSrc.includes('daytime')){
        playEle.attr('src', '/audio/nighttime.mp3');
        playEle.prop('volume', 0.9);
    }

    if(!noSound())
        $('#backgroundMusic')[0].play();
}

function pause(){
    return $('#backgroundMusic')[0].pause();
}

function speak(s){
    if(noSound())
        return;
    if(!responsiveVoice || !defaultSound('narration_enabled'))
        return;

    if(s.indexOf('>') !== -1){
        const d = $('<div>');
        d.html(s);
        s = d.text();
    }

    window.gameState.speech = window.gameState.speech
        .then(() => new Promise((resolve => {
            // pauseMusic();
            responsiveVoice.cancel();
            responsiveVoice.speak(s, 'UK English Male', {
                pitch: 1,
                rate: 0.83,
                onend: resolve,
                onerror: function(e){
                    helpers.log(e);
                    resolve();
                },
            });
        })));

    // if(responsiveVoice.isPlaying() || true){
    //
    // }else{
    //     window.gameState.speech = new Promise((resolve => {
    //         responsiveVoice.cancel();
    //         responsiveVoice.speak(s, 'UK English Male', {
    //             pitch: 1,
    //             rate: 0.83,
    //             onend: resolve,
    //             onerror: function(e){
    //                 console.log(e);
    //                 resolve();
    //             },
    //         });
    //     }));
    // }
}

function setCypressTesting(){
    cypressTesting = true;
    return play();
}

module.exports = {
    defaultSound,
    isPlaying,
    play,
    pause,
    speak,
    setCypressTesting,
};

function getSettingsValue(key){
    if(helpers.storageEnabled()){
        const val = localStorage.getItem(key);
        if(val === 'false')
            return false;
        return val;
    }
    return window.gameState[key];
}

function noSound(){
    return !cypressTesting && window.location.host.includes('localhost');
}
