import store from '../reducers/store';

import { onLobbyCreateAction, onLobbyDeleteAction } from '../actions/payloads/lobbyActions';


export function onNewLobby(lobby){
    store.dispatch(onLobbyCreateAction(lobby));
}

export function onLobbyDelete({ gameID }){
    store.dispatch(onLobbyDeleteAction(gameID));
}
