const requests = require('../util/browserRequests');


export function upsert(name, value){
    const args = { name, value };
    return requests.post('gameModifiers', args);
}
