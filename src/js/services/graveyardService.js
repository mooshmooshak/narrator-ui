function transformGraveyard(game){
    const graveyardMap = game.graveyardMap = {};
    game.graveyard.forEach(deadPerson => {
        graveyardMap[deadPerson.name] = deadPerson;
    });
}

module.exports = {
    transformGraveyard,
};
