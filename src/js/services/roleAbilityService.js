/* eslint-env browser */
const requests = require('../util/browserRequests');


async function upsertRoleAbilityModifier(roleID, abilityID, modifierName, value){
    const args = {
        name: modifierName,
        value,
    };
    await requests.post(
        `roles/${roleID}/abilities/${abilityID}/modifiers`, args,
    );
}

module.exports = {
    upsertRoleAbilityModifier,
};
