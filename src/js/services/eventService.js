import store from '../reducers/store';
import {
    onPlayersUpdateAction,
    onTrialStart,
    onVotesUpdateAction,
    onVoteUpdateAction,
} from '../actions/payloads/gameActions';
import { onProfileUpdateAction } from '../actions/payloads/profileActions';
/* eslint-env browser, jquery */
const index = require('../index');

const chatService = require('./chatService');
const gameUserService = require('./gameUserService');
const lobbyService = require('./lobbiesService');
const moderatorService = require('./moderatorService');
const playerService = require('./playerService');
const setupHiddenService = require('./setupHiddenService');
const setupService = require('./setupService');

const roleInfoView = require('../views/roleInfoView');


export function handle(object){
    switch (object.event){
    case 'lobbyChatMessageEvent':
        return chatService.onNewLobbyMessage(object);
    case 'lobbyCreate':
        return lobbyService.onNewLobby(object);
    case 'lobbyDelete':
        return lobbyService.onLobbyDelete(object);

    case 'hostChange':
        return moderatorService.onHostChange(new Set([object.hostID]));

    case 'kicked':
        return playerService.onKick(object);

    case 'playerAdded':
        return playerService.onPlayerAdd(object);
    case 'playerExit':
        return playerService.onPlayerExit(object);
    case 'playerNameUpdate':
        return playerService.onPlayerNameUpdate(object);
    case 'playerStatusChange':
        return gameUserService.onUserStatusChange(object);

    case 'setupChange':
        return setupService.postSetupChangeRefresh(object.setup);
    case 'setupHiddenAdd':
        return setupHiddenService.onSetupHiddenAdd(object.setupHidden);
    case 'setupHiddenRemove':
        return setupHiddenService.onSetupHiddenRemove(object);

    case 'timerUpdate':
        window.gameState.timer = object.endTime;
        return;

    case 'dayStart':
    case 'nightStart':
    case 'votePhaseReset':
    case 'votePhaseStart':
        return onPhaseStart(object);

    case 'trialStart':
        return store.dispatch(onTrialStart(object));

    case 'gameEnd':
        return onGameEnd(object);

    case 'phaseEndBid':
        return playerService.onPhaseEndBid(object);

    case 'playerUpdate':
        return playerService.onPlayerUpdate(object.player);

    case 'voteUpdate':
        return store.dispatch(onVoteUpdateAction(object));

    default:
        break;
    }
}


function onGameStart(){
    window.gameState.isStarted = true;
    return index.goToGamePlayPage();
}

function onGameEnd(object){
    window.gameState.phase = object.phase;
    window.gameState.timer = null;
    $('#actions_pane').show();
    $('body').addClass('gameOver');
    $('#nav_tab_5 span').text('Exit');
    $('.settings_button').unbind().click(index.leaveGame);

    // window.left = null;
    // window.right = null;
    // window.selected = null;
    index.refreshPlayers();

    $('#end_night_button').text('Leave Game');
}

function onPhaseStart(object){
    if(!window.gameState.isStarted)
        onGameStart(object);
    // setup has to be saved first because profile may reference new enemy faction ids
    // these faction ids might exist in this new setup
    if(object.setup)
        setupService.postSetupChangeRefresh(object.setup);
    if(object.phase){
        window.gameState.phase = object.phase;
        index.refreshActionButtonText();
    }

    const updatedPlayers = object.players || [];
    updatedPlayers.forEach(player => {
        const playerName = player.name;
        window.gameState.playerMap[playerName].votePower = player.votePower;
    });
    store.dispatch(onPlayersUpdateAction(updatedPlayers));

    Object.values(window.gameState.playerMap).forEach(player => {
        player.endedNight = false;
    });
    if(object.profile){
        store.dispatch(onProfileUpdateAction(object.profile));
        roleInfoView.refresh();
        index.refreshPlayers();
    }
    index.pushRoleCardPopup(); // this should be on the onRoleCardUpdate event
    store.dispatch(onVotesUpdateAction(object.voteInfo));
}
