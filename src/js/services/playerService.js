import store from '../reducers/store';
import {
    onPlayersUpdateAction, onPlayerExitAction, onPlayerNameUpdateAction,
} from '../actions/payloads/gameActions';
import { onLobbyLeaveAction } from '../actions/payloads/profileActions';
import { onSetupChangeAction } from '../actions/payloads/setupActions';
/* eslint-env jquery, browser */
const helpers = require('../util/browserHelpers');
const requests = require('../util/browserRequests');


export async function addBots(botCount){
    const setupService = require('./setupService');
    try{
        const serverResponse = await requests.post('players/bots', { botCount });
        store.dispatch(onPlayersUpdateAction(serverResponse.response.players));
        setupService.transformSetup(serverResponse.response.setup);
        store.dispatch(onSetupChangeAction(serverResponse.response.setup));
    }catch(err){
        require('../index').setWarning(err.errors[0]);
    }
}

export async function joinByGameID(joinID, playerName){
    const authService = require('./authService');
    await authService.ensureLoggedIn();
    const { response: game } = await requests.post('players', { joinID, playerName });
    return game;
}

export async function deletePlayer(playerID, playerName, gameID){
    const serverResponse = await requests.delete(
        `players/bots?gameID=${gameID}&playerID=${playerID}`,
    );
    const { setup } = serverResponse.response;
    store.dispatch(onPlayerExitAction(playerName));
    require('./setupService').postSetupChangeRefresh(setup);
}

export async function kickUser(userID){
    const serverResponse = await requests.delete(`players/kick?userID=${userID}`);
    const { moderatorIDs } = serverResponse.response;
    onPlayerExit({
        moderatorIDs: new Set(moderatorIDs),
        setup: serverResponse.response.setup,
        userID,
    });
}

export function leave(){
    return requests.delete('players');
}

export function onKick(){
    const index = require('../index');
    index.goToGlobalLobbyPage();
    index.setWarning('You\'ve been kicked from the lobby!');
    helpers.setPseudoURL('');
    store.dispatch(onLobbyLeaveAction());
}

export function onPhaseEndBid(phaseEndBidObj){
    phaseEndBidObj.players.forEach(player => {
        window.gameState.playerMap[player.name].endedNight = player.endedNight;
    });
    if(window.gameState.endedNight())
        require('../index').refreshPlayers();
}

export function onPlayerAdd(playerAddObj){
    savePlayers(playerAddObj.players);
    store.dispatch(onPlayersUpdateAction(playerAddObj.players));
    require('./setupService').postSetupChangeRefresh(playerAddObj.setup);
    // not adding userID to gameState.users yet because game doesn't use users
}

export function onPlayerExit({ setup, moderatorIDs, playerName }){
    require('./moderatorService').onHostChange(new Set(moderatorIDs));
    store.dispatch(onPlayerExitAction(playerName));
    require('./setupService').postSetupChangeRefresh(setup);
}

export function onPlayerNameUpdate({ playerID, playerName }){
    store.dispatch(onPlayerNameUpdateAction(playerID, playerName));
}

export function onPlayerUpdate(player){
    window.gameState.playerMap = {
        ...window.gameState.playerMap,
        [player.name]: player,
    };
    store.dispatch(onPlayersUpdateAction([player]));
    require('../index').refreshPlayers();
}

export function savePlayers(players){
    window.gameState.playerMap = window.gameState.playerMap || {};
    players.forEach(player => {
        window.gameState.playerMap[player.name] = player;
    });
}

export async function updatePlayerName(playerID, name){
    await requests.put(`players/${playerID}`, { name });
    onPlayerNameUpdate({ playerID, playerName: name });
}
