import { MAX_PLAYER_COUNT } from '../util/constants';

const requests = require('../util/browserRequests');


export async function createHidden({ name, setupID }){
    const { response } = await requests.post('hiddens', { setupID, name });
    return response;
}

export async function addSpawns(spawns){
    spawns = spawns.map(spawn => ({
        ...spawn,
        minPlayerCount: 0,
        maxPlayerCount: MAX_PLAYER_COUNT,
    }));
    const { response } = await requests.post('hiddenSpawns', spawns);
    return response;
}

export function deleteSpawn(hiddenSpawnID){
    return requests.delete(`hiddenSpawns/${hiddenSpawnID}`);
}
