import store from '../reducers/store';
import {
    onFactionCreateAction, onFactionDeleteAction,
    onFactionEnemyAddAction, onFactionEnemyDeleteAction, onFactionUpdateAction,
} from '../actions/payloads/setupActions';
import { onShowFactionRoleAction, onShowFactionAction } from '../actions/payloads/panelViewActions';

const requests = require('../util/browserRequests');


export async function create(setupID, { name, color, description }){
    const serverResponse = await requests.post('factions', {
        color,
        description,
        name,
        setupID,
    });
    const newFaction = serverResponse.response;
    store.dispatch(onFactionCreateAction(newFaction));
    return newFaction;
}

export async function addEnemy(factionID, enemyFactionID){
    await requests.post(`factions/${factionID}/enemies/${enemyFactionID}`);
    store.dispatch(onFactionEnemyAddAction(factionID, enemyFactionID));
}

export async function deleteEnemy(setupID, factionID, enemyFactionID){
    await requests.delete(`setups/${setupID}/factions/${factionID}/enemies/${enemyFactionID}`);
    store.dispatch(onFactionEnemyDeleteAction(factionID, enemyFactionID));
}

export async function addSheriffCheckable(factionID, checkableFactionID){
    await requests.post(`factions/${factionID}/checkables/${checkableFactionID}`);
}

export async function deleteFaction(factionID){
    await requests.delete(`factions/${factionID}`);
    store.dispatch(onFactionDeleteAction(factionID));
}

export function openFirst(){
    const setup = store.getState().setupState;
    const factionIDs = Object.keys(setup.factionMap);
    if(!factionIDs.length)
        return;
    const factionID = parseInt(factionIDs[0], 10);
    const factionRoleIDs = Object.keys(setup.factionRoleMap);

    if(!factionRoleIDs.length)
        return store.dispatch(onShowFactionAction(factionID));

    const factionRoleID = parseInt(factionRoleIDs[0], 10);
    store.dispatch(onShowFactionRoleAction(factionRoleID, factionID));
}

export async function update(setupID, factionID, args){
    await requests.put(`setups/${setupID}/factions/${factionID}`, args);
    store.dispatch(onFactionUpdateAction(factionID, args));
}
