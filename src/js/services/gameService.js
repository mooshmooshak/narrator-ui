import store from '../reducers/store';

import { onGameUpdate } from '../actions/payloads/gameActions';
import { onSetupChangeAction } from '../actions/payloads/setupActions';
/* eslint-env browser */

const helpers = require('../util/browserHelpers');
const requests = require('../util/browserRequests');

const roleInfoView = require('../views/roleInfoView');


export async function createGame(hostName, setupID){
    const args = {
        name: hostName,
        setupID,
    };
    const serverResponse = await requests.post('games', args);
    return getGameStateAndChat(serverResponse.response);
}

export async function getGameStateAndChat(game, gameID){
    const index = require('../index');
    const chatService = require('./chatService');
    const gameUserService = require('./gameUserService');
    const playerService = require('./playerService');
    const setupService = require('./setupService');

    if(!game)
        game = await getGame(gameID);
    setupService.transformSetup(game.setup);
    store.dispatch(onSetupChangeAction(game.setup));
    store.dispatch(onGameUpdate(game));

    const { gameState } = window;
    gameState.activeUserIDs = await gameUserService.getActiveUserIDs(game.id);
    gameState.gameID = game.id;
    gameState.moderatorIDs = new Set(game.users
        .filter(({ isModerator }) => isModerator)
        .map(({ id }) => id));
    gameState.integrations = game.integrations;
    gameState.isStarted = game.isStarted;
    playerService.savePlayers(game.players);
    gameState.phase = game.phase;
    gameState.modifiers = game.modifiers;
    gameState.setup = game.setup;
    if(gameState.isStarted)
        refreshGameViews();
    else
        index.goToSetupPage();
    index.refreshHost();

    const userState = await requests.get('games/userState');
    await index.handleObject(userState.response);

    require('./factionService').openFirst();

    if(game.isStarted){
        const chats = await chatService.getChats(game.id);
        await index.handleObject(chats);
    }
    return true; // indicating successful game join
}

export async function getGames(){
    const serverResponse = await requests.get('games', {});
    return serverResponse.response;
}

export async function getGame(gameID){
    const serverResponse = await requests.get(`games/${gameID}`);
    return serverResponse.response;
}

export function getByJoinID(joinID){
    return requests.get(`games?join_id=${joinID}`);
}

export async function recoverProfileWithAuthTokenInURL(){
    const authToken = helpers.getURLParameters().auth_token;
    if(!looksLikeAuthToken(authToken))
        return false;

    const authService = require('./authService');
    try{
        await authService.getUserIDWithAuthToken(authToken);
    }catch(err){
        if(err === authService.UNKNOWN_AUTH_TOKEN)
            return require('../index').setWarning('Woops! This game link is no longer active.');
        throw err;
    }
    const userService = require('./userService');
    if(!userService.isSignedInAnonymously()){
        await authService.logout();
        await authService.createUserAnonymously();
    }
    const authIntegrationService = require('./authIntegrationService');
    await authIntegrationService.addIntegration();

    helpers.setPseudoURL('/');

    const profileService = require('./profileService');
    return profileService.get();
}

export async function recoverProfileWithPreviousLogIn(){
    const authService = require('./authService');
    const user = await authService.checkPreviousLogin();
    if(user)
        return require('./profileService').get();
}

export function start(){
    return requests.post('games/1234/start'); // not saving the game id yet
}

function looksLikeAuthToken(authToken){
    if(!authToken)
        return false;
    return authToken.length === 128 && helpers.isAlphaNumeric(authToken);
}

function refreshGameViews(){
    roleInfoView.refresh();
    require('../index').refreshActionButtonText();
}
