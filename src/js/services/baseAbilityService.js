const requests = require('../util/browserRequests');


export async function getBaseAbilities(){
    const serverResponse = await requests.get('baseAbilities');
    return serverResponse.response;
}
