const requests = require('../util/browserRequests');


function addIntegration(){
    return requests.post('user_integrations');
}

module.exports = {
    addIntegration,
};
