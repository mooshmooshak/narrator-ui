/* eslint-env browser */
const requests = require('../util/browserRequests');


async function create(roleArgs){
    const serverResponse = await requests.post('roles', {
        ...roleArgs,
        setupID: window.gameState.setup.id,
    });
    const role = serverResponse.response;
    window.gameState.setup.roleMap[role.id] = role;
    return role;
}

async function upsertRoleModifier(roleID, modifierName, value){
    const args = {
        name: modifierName,
        value,
    };
    const serverResponse = await requests.post(`roles/${roleID}/modifiers`,
        args);
    value = serverResponse.response.value;
    const role = window.gameState.setup.roleMap[roleID];
    role.modifiers.find(modifier => modifier.name === modifierName)
        .value = value;
}

async function sync(roleID){
    const serverResponse = await requests.get(`roles/${roleID}`);
    const role = serverResponse.response;
    window.gameState.setup.roleMap[role.id] = role;
}

module.exports = {
    create,
    upsertRoleModifier,
    sync,
};
