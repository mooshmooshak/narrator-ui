import store from '../reducers/store';
import {
    onLobbyChatLoad, onNewLobbyMessage as onNewLobbyMessageAction,
} from '../actions/payloads/chatActions';
/* eslint-env browser  */
const requests = require('../util/browserRequests');


export const chatQueue = [];

export async function getChats(gameID){
    const serverResponse = await requests.get(`chats?gameID=${gameID}`);
    return serverResponse.response;
}

export async function getLobbyMessages(gameID){
    const serverResponse = await requests.get(`chats/${gameID}/lobby`);
    const messages = serverResponse.response;
    store.dispatch(onLobbyChatLoad(messages));
}

export function onNewLobbyMessage(message){
    store.dispatch(onNewLobbyMessageAction(message));
}

export function sendLobbyMessage(gameID, text){
    const payload = {
        text,
    };
    return requests.post(`chats/${gameID}/lobby`, payload);
}

export function setActiveChat(specifiedChat){
    chatQueue.length = 0;
    window.gameState.chats[specifiedChat].messages
        .forEach(message => chatQueue.push(message));
}
