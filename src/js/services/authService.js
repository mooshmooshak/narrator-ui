/* eslint-env browser */
/* global firebase */
const helpers = require('../util/browserHelpers');
const requests = require('../util/browserRequests');


const config = {
    apiKey: 'AIzaSyBqatywpJsXTXBmYP9p8eR-p3B0OWsmDTk',
    authDomain: 'narrator-119be.firebaseapp.com',
    databaseURL: 'https://narrator-119be.firebaseio.com',
    storageBucket: 'narrator-119be.appspot.com',
};
firebase.initializeApp(config);
const UNKNOWN_AUTH_TOKEN = 'UNKNOWN_AUTH_TOKEN';

async function checkPreviousLogin(){
    return new Promise(resolve => {
        const stopListening = firebase.auth().onAuthStateChanged(user => {
            stopListening(); // this stops the auth state from firing a million times
            if(user)
                window.user = user;
            resolve(user);
        });
    });
}

async function createUserAnonymously(){
    await new Promise((resolve, reject) => {
        const unsubscribe = firebase.auth().onAuthStateChanged(user => {
            if(!user)
                return;
            unsubscribe();
            window.user = user;
            resolve();
        });

        firebase.auth().signInAnonymously().catch(error => {
            helpers.log(error, 'Failed to sign in anonymously.');
            unsubscribe();
            reject(error);
        });
    });
}

async function createUserWithEmailAndPassword(email, password){
    try{
        await firebase.auth().createUserWithEmailAndPassword(email, password);
    }catch(err){
        if(!err.code){
            helpers.log(err, 'Unknown error on signup.');
            return Promise.reject(err.toString());
        }
        const code = err.code;
        let parsedError;

        if(code === 'auth/invalid-email'){
            parsedError = 'Badly formatted username';
        }else if(code === 'auth/weak-password'){
            parsedError = 'Password is too weak.';
        }else if(code === 'auth/email-already-in-use'){
            parsedError = 'That username is taken!';
        }else{
            parsedError = code;
            helpers.log(err, 'Couldn\t parse signup error.');
        }
        throw parsedError;
    }
}

async function ensureLoggedIn(){
    if(!window.user)
        await createUserAnonymously();
}

function getUserIDWithAuthToken(authToken){
    const userService = require('./userService');
    return requests.get(`users?auth_token=${authToken}`)
        .then(responseObj => {
            const userID = responseObj.response.userID;
            userService.setUserID(userID);
            return userID;
        }).catch(() => {
            throw UNKNOWN_AUTH_TOKEN;
        });
}

function isLoggedIn(){
    return !!window.user;
}

function loginWithEmail(username, password){
    return firebase.auth().signInWithEmailAndPassword(username, password)
        .then(user => {
            window.user = user;
        }).catch(err => {
            if(!err.code){
                helpers.log(err, 'Unknown error on signup.');
                return Promise.reject(err);
            }

            const code = err.code;
            let parsedError;
            if(code === 'auth/wrong-password'){
                parsedError = 'Incorrect password.';
            }else if(code === 'auth/user-not-found' || code === 'auth/invalid-email'){
                parsedError = 'Username not found.';
            }else{
                parsedError = code;
                helpers.log(err, 'Couldn\t parse signup error.');
            }

            return Promise.reject(parsedError);
        });
}

async function logout(){
    try{
        await firebase.auth().signOut();
        window.user = null;
    }catch(err){
        helpers.log(err, 'Failed to log out.');
    }
}

module.exports = {
    checkPreviousLogin,
    createUserWithEmailAndPassword,
    createUserAnonymously,
    ensureLoggedIn,
    getUserIDWithAuthToken,
    isLoggedIn,
    loginWithEmail,
    logout,

    UNKNOWN_AUTH_TOKEN,
};
