import store from '../reducers/store';
import {
    onFactionAbilityCreateAction,
    onFactionAbilityDeleteAction,
} from '../actions/payloads/setupActions';

const requests = require('../util/browserRequests');


export async function create(factionID, abilityType){
    const serverResponse = await requests.post(`factions/${factionID}/abilities`, {
        ability: abilityType,
    });
    const ability = serverResponse.response;
    store.dispatch(onFactionAbilityCreateAction(factionID, ability));
    return ability;
}

export async function del(factionID, abilityID){
    await requests.delete(`factions/${factionID}/abilities/${abilityID}`);
    store.dispatch(onFactionAbilityDeleteAction(factionID, abilityID));
}

export function upsertModifier(factionAbilityID, name, value){
    return requests.post(`factionAbilities/${factionAbilityID}/modifiers`, { name, value });
}
