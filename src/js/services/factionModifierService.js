import store from '../reducers/store';
import { onFactionModifierUpdateAction } from '../actions/payloads/setupActions';
import { MAX_PLAYER_COUNT } from '../util/constants';

const requests = require('../util/browserRequests');


export async function upsert(value, modifierName, factionID){
    const args = {
        maxPlayerCount: MAX_PLAYER_COUNT,
        minPlayerCount: 0,
        name: modifierName,
        value,
    };
    await requests.post(`factions/${factionID}/modifiers`, args);
    store.dispatch(onFactionModifierUpdateAction(factionID, modifierName, value));
}
