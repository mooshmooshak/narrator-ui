import store from '../reducers/store';
import { onUserUpdateAction } from '../actions/payloads/sessionActions';
/* eslint-env browser */
const requests = require('../util/browserRequests');


export function setUserID(user){
    window.gameState.userID = user.id;
    store.dispatch(onUserUpdateAction(user));
}

export async function getOnlineUsers(){
    const responseObj = await requests.get('users/online');
    return responseObj.response;
}

export function isSignedInAnonymously(){
    if(!window.user)
        return false;
    return window.user.isAnonymous;
}
