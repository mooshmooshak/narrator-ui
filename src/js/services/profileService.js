import store from '../reducers/store';
import { onProfileUpdateAction } from '../actions/payloads/profileActions';

const requests = require('../util/browserRequests');


export async function get(){
    const { response: profile } = await requests.get('profiles');
    store.dispatch(onProfileUpdateAction(profile));
    return profile;
}
