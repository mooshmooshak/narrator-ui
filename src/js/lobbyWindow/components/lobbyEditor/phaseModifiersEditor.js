import React from 'react';
import { connect } from 'react-redux';

// eslint-disable-next-line max-len
import LengthDropdown, { GAME_LENGTH_OPTIONS } from '../../../landingWindow/components/gameCreate/lengthDropdown';
import PhaseLengthInput from './phaseLengthInput';

import { getGameModifierValue } from '../../../util/modifierUtil';

import { updateGameModifierAction } from '../../../actions/gameActions';
import LobbyOverviewToggle from './lobbyEditorToggle';


const phases = [{
    isBasic: true,
    name: 'DAY_LENGTH_START',
    label: 'Day Length',
}, {
    isBasic: true,
    name: 'NIGHT_LENGTH',
    label: 'Night Length',
}, {
    name: 'DISCUSSION_LENGTH',
    label: 'Discussion Length',
}, {
    name: 'TRIAL_LENGTH',
    label: 'Trial Length',
}, {
    name: 'DAY_LENGTH_MIN',
    label: 'Day Length Min',
}, {
    name: 'ROLE_PICKING_LENGTH',
    label: 'Pregame Role Picking Length',
}];

function PhaseModifiersEditor({ isShowingAdvanced, timeGameModifiers, upsertGameModifier }){
    const funcGameStateParam = { modifiers: timeGameModifiers };
    const dayLengthValue = getGameModifierValue('DAY_LENGTH_START', funcGameStateParam);
    const nightLengthValue = getGameModifierValue('NIGHT_LENGTH', funcGameStateParam);

    function updateDayNightLength({ dayLength, nightLength }){
        return Promise.all([
            upsertGameModifier('DAY_LENGTH_START', dayLength),
            upsertGameModifier('NIGHT_LENGTH', nightLength),
        ]);
    }

    const isDayNightLengthInDropdown = !!GAME_LENGTH_OPTIONS
        .find(
            ({ dayLength, nightLength }) => dayLengthValue === dayLength
                && nightLengthValue === nightLength,
        );

    const visiblePhases = phases.filter(
        ({ isBasic }) => isShowingAdvanced || (isBasic && !isDayNightLengthInDropdown),
    );

    return (
        <div className="lobbyOverviewMiscContainer">
            <LobbyOverviewToggle
                className="wideDropdown"
                label="Day Start"
                modifierName="DAY_START"
            />
            {!isShowingAdvanced && isDayNightLengthInDropdown && (
                <LengthDropdown
                    className="lobbyWindowElement wideDropdown"
                    dayLength={dayLengthValue}
                    nightLength={nightLengthValue}
                    setLengthState={updateDayNightLength}
                />
            )}
            {visiblePhases.map(({ name, label }) => (
                <PhaseLengthInput
                    isShowingAdvanced={isShowingAdvanced}
                    key={name}
                    modifierLabel={label}
                    modifierName={name}
                    timeGameModifiers={timeGameModifiers}
                    upsertGameModifier={upsertGameModifier}
                />
            ))}
        </div>
    );
}

const mapStateToProps = ({ gameState }) => ({
    timeGameModifiers: gameState.modifiers,
});

const mapDispatchToProps = {
    upsertGameModifier: updateGameModifierAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(PhaseModifiersEditor);
