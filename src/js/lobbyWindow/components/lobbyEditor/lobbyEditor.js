import React from 'react';

import FloatingTintedContainer from '../../../components/floatingTintedContainer';
import LobbyEditorAdvancedController from './lobbyEditorAdvancedController';
import VoteModifiersEditor from './voteModifiersEditor';
import LobbyOverviewPhaseSection from './phaseModifiersEditor';
import LobbyOverviewMiscSection from './miscModifiersEditor';


export default function LobbyEditor({ setClosed }){
    return (
        <FloatingTintedContainer className="lobbyEditorContainer" setClosed={setClosed}>
            <LobbyEditorAdvancedController>
                <VoteModifiersEditor />
            </LobbyEditorAdvancedController>
            <LobbyEditorAdvancedController>
                <LobbyOverviewPhaseSection />
            </LobbyEditorAdvancedController>
            <LobbyEditorAdvancedController>
                <LobbyOverviewMiscSection />
            </LobbyEditorAdvancedController>
        </FloatingTintedContainer>
    );
}
