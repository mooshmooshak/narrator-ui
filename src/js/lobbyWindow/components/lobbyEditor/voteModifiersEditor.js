import React from 'react';

import LobbyOverviewToggle from './lobbyEditorToggle';
import VoteSystemsDropdown from './voteSystemsDropdown';


export default function VoteModifiersEditor({ isShowingAdvanced }){
    return (
        <>
            <VoteSystemsDropdown className="lobbyWindowElement wideDropdown" />
            {isShowingAdvanced && (
                <div className="lobbyOverviewVoteContainer">
                    <LobbyOverviewToggle label="Skip Voting" modifierName="SKIP_VOTE" />
                    <LobbyOverviewToggle label="Self Voting" modifierName="SELF_VOTE" />
                </div>
            )}
        </>
    );
}
