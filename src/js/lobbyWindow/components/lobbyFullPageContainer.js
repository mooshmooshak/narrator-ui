import React, { useState } from 'react';

import Button from '../../components/baseElements/button';

import Toast from '../../components/toast';

import LobbyChatPane from './lobbyChatPane';
import LobbyDetailsContainer from './lobbyDetails/lobbyDetailsContainer';
import LobbySetupHiddens from './lobbySetupHiddens';
import ParticipantsContainer from './participants/participantsContainer';

import { start as startGame } from '../../services/gameService';


export default function LobbyFullPageContainer({
    gameModifiers, isModerator, lobbyID, setupModifiers, showSetup,
}){
    const [error, setError] = useState();

    async function start(){
        try{
            await startGame();
        }catch(err){
            setError(err.errors[0]);
        }
    }

    return (
        <>
            <div className="lobbyDetailsHeader">
                <span className="lobbyDetailsHeaderText">Game #{lobbyID}</span>
                <Button className="lobbyHeaderStart" onClick={start}>Start</Button>
            </div>
            <div className="lobbyContent">
                <div className="lobbyDetailsPlayers">
                    <LobbyDetailsContainer
                        gameModifiers={gameModifiers}
                        isModerator={isModerator}
                        lobbyID={lobbyID}
                        setupModifiers={setupModifiers}
                        showSetup={showSetup}
                    />
                    <ParticipantsContainer />
                </div>
                <LobbyChatPane className="lobbyTabletWidth" />
                <LobbySetupHiddens onStartClick={start} />
            </div>
            <Toast
                isOpen={!!error}
                setClosed={setError}
                text={error || ''}
                type="error"
            />
        </>
    );
}
