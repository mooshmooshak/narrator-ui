import React from 'react';
import { connect } from 'react-redux';

import ChatTicker from '../../components/chat/chatTicker';

import { getPlayerName, getUserColor, getUserIcon } from '../../util/playerUtil';


function LobbyChatTicker({ onExpand, ...senderTextProps }){
    const [sender, text] = getSenderAndText(senderTextProps);
    return (
        <ChatTicker className="lobbyChatTicker" onExpand={onExpand} sender={sender} text={text} />
    );
}


const mapStateToProps = ({ chatState, gameState }) => ({
    messages: chatState.lobby.messages,
    playerMap: gameState.playerMap,
    gameID: gameState.id,
    users: gameState.users,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(LobbyChatTicker);

function getSenderAndText({
    messages, gameID, playerMap, users,
}){
    if(!messages.length)
        return [];
    const { text, userID } = messages[messages.length - 1];
    return [{
        color: getUserColor(gameID, userID),
        icon: getUserIcon(gameID, userID),
        name: getPlayerName(userID, playerMap, users),
    },
    text];
}
