import React from 'react';
import { connect } from 'react-redux';

import ChatContainer from '../../components/chat/chatContainer';
import { sendLobbyMessage } from '../../services/chatService';
import { onLobbyChatInputUpdate } from '../../actions/payloads/chatActions';
import { MESSAGE_TYPE } from '../../util/constants';
import { getPlayerName, getUserColor, getUserIcon } from '../../util/playerUtil';


function LobbyChatPane({
    chatState, className = '', gameID, playerMap, updateLobbyChatTextInput, users,
}){
    if(className)
        className += ' ';
    return (
        <div className={`${className}lobbyChatContainer`}>
            <ChatContainer
                messages={mapMessages(gameID, chatState.messages, playerMap, users)}
                onChange={updateLobbyChatTextInput}
                onSubmit={() => {
                    updateLobbyChatTextInput('');
                    sendLobbyMessage(gameID, chatState.textInput);
                }}
                textInputValue={chatState.textInput}
            />
        </div>
    );
}

const mapStateToProps = ({ chatState, gameState }) => ({
    chatState: chatState.lobby,
    gameID: gameState.id,
    playerMap: gameState.playerMap,
    users: gameState.users,
});

const mapDispatchToProps = {
    updateLobbyChatTextInput: onLobbyChatInputUpdate,
};

export default connect(mapStateToProps, mapDispatchToProps)(LobbyChatPane);

function mapMessages(gameID, messages, playerMap, users){
    return messages.map(({
        createdAt, id, text, userID,
    }) => ({
        createdAt,
        id,
        messageType: MESSAGE_TYPE.LOBBY_CHAT_MESSAGE,
        sender: {
            color: getUserColor(gameID, userID),
            icon: getUserIcon(gameID, userID),
            name: getPlayerName(userID, playerMap, users),
        },
        text,
    }));
}
