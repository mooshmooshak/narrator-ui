import React, { Fragment, useState } from 'react';
import { connect } from 'react-redux';

import EntityDetailsContainer from '../../../components/entityDetailsContainer';
import FactionRoleClickable from '../../../components/factionRoleClickable';

import HiddenEditorContainer from './hiddenEditorContainer';

import { getColor as getHiddenColor, getSpawnableFactionRoleIDs } from '../../../util/hiddenUtils';


function HiddenDetails({
    panelViewState, setup, playerCount, userID,
}){
    const [isEditorOpen, setIsEditorOpen] = useState(false);
    const hidden = setup.hiddenMap[panelViewState.entityID];
    if(!hidden) // happens when a hidden is deselected
        return null;
    return (
        <>
            <EntityDetailsContainer
                headerText={hidden.name}
                headerColor={getHiddenColor(hidden, setup)}
                items={getSpawnedRoles(hidden, setup, playerCount)}
                openEditor={(userID === setup.ownerID) && (() => setIsEditorOpen(true))}
            />

            <HiddenEditorContainer
                hidden={isEditorOpen ? hidden : undefined}
                setHiddenID={() => setIsEditorOpen(false)}
            />
        </>
    );
}

const mapStateToProps = ({
    panelViewState, setupState, session, gameState,
}) => ({
    panelViewState,
    setup: setupState,
    playerCount: Object.values(gameState.playerMap).length,
    userID: session.userID,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(HiddenDetails);

function getSpawnedRoles(hidden, setup, playerCount){
    const spawnableRoles = getSpawnableFactionRoleIDs(hidden, playerCount)
        .map(factionRoleID => setup.factionRoleMap[factionRoleID]);
    spawnableRoles.sort((fr1, fr2) => fr1.name.localeCompare(fr2.name));
    if(!spawnableRoles.length)
        return [];

    return [(
        <Fragment key="otherFactionRoleVersions">
            Spawns:
            {' '}
            {spawnableRoles.map((factionRole, index) => (
                <Fragment key={factionRole.id}>
                    {!!index && ', '}
                    <FactionRoleClickable factionRole={factionRole} />
                </Fragment>
            ))}
            .
        </Fragment>
    )];
}
