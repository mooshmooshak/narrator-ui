import React from 'react';
import { connect } from 'react-redux';

import LabeledCheckbox from '../../../components/labeledCheckbox';
import LabeledNumberInput from '../../../components/labeledNumberInput';

import {
    upsertAbilityModifier as updateFactionRoleAbilityModifier,
    upsertRoleModifier as updateFactionRoleModifier,
    sync as syncFactionRole,
    syncOtherRoles as syncOtherFactionRoles,
} from '../../../services/factionRoleService';
import { upsert as upsertSetupModifier } from '../../../services/setupModifierService';
import { getSetupModifier } from '../../../util/modifierUtil';


function FactionRoleEditor({ factionRole, setup, playerCount }){
    const modifiers = [
        ...getSetupModifiers(factionRole, setup),
        ...getAbilityModifiers(factionRole, playerCount),
        ...getRoleModifiers(factionRole, playerCount),
    ];

    return (
        <ul className="factionRoleEditor">
            {
                modifiers.map(({ label, value, onChange }, index) => {
                    if(typeof(value) === 'number')
                        return (
                            // eslint-disable-next-line react/no-array-index-key
                            <li key={index}>
                                <LabeledNumberInput
                                    label={label}
                                    value={value}
                                    onChange={onChange}
                                />
                            </li>
                        );
                    return (
                        // eslint-disable-next-line react/no-array-index-key
                        <li key={index}>
                            <LabeledCheckbox
                                label={label}
                                value={value}
                                onChange={onChange}
                            />
                        </li>
                    );
                })
            }
        </ul>
    );
}

const mapStateToProps = ({ setupState, gameState }) => ({
    setup: setupState,
    playerCount: Object.values(gameState.playerMap).length,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(FactionRoleEditor);

function getSetupModifiers(factionRole, setup){
    return factionRole.abilities.map(ability => ability.setupModifierNames
        .map(setupModifierName => {
            const setupModifier = getSetupModifier(setupModifierName, setup);
            return {
                value: setupModifier.value,
                label: setupModifier.label,
                onChange: async function(newValue){
                    await upsertSetupModifier(setupModifierName, newValue);
                    await syncFactionRole(factionRole.id);
                    return syncOtherFactionRoles(factionRole.id, setupModifierName);
                },
            };
        })).flat();
}

function getAbilityModifiers(factionRole, playerCount){
    return factionRole.abilities.map(ability => getVisibleModifiers(ability.modifiers, playerCount)
        .map(modifier => ({
            label: modifier.label,
            value: modifier.value,
            onChange: async function(newValue){
                await updateFactionRoleAbilityModifier(factionRole.id, ability.id,
                    modifier.name, newValue);
                return syncFactionRole(factionRole.id);
            },
        }))).flat();
}

function getRoleModifiers(factionRole, playerCount){
    return getVisibleModifiers(factionRole.modifiers, playerCount).map(modifier => ({
        label: modifier.label,
        value: modifier.value,
        onChange: async function(newValue){
            await updateFactionRoleModifier(factionRole.id, modifier.name, newValue);
            return syncFactionRole(factionRole.id);
        },
    }));
}

function getVisibleModifiers(modifiers, playerCount){
    return modifiers.reduce((acc, modifier) => {
        if(modifier.maxPlayerCount < playerCount || modifier.minPlayerCount > playerCount)
            return acc;
        const prevModifier = acc.find(m => m.name === modifier.name);
        if(!prevModifier)
            return [...acc, modifier];
        acc = acc.filter(m => m.name !== modifier.name);
        if(prevModifier.upsertedAt > modifier.upsertedAt)
            return [...acc, prevModifier];
        return [...acc, modifier];
    }, []);
}
