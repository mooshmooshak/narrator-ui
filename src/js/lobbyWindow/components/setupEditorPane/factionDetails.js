import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import EntityDetailsContainer from '../../../components/entityDetailsContainer';
import FactionClickable from '../../../components/factionClickable';
import FactionModifiersEditor from './factionModifiersEditor';
import FactionRoleClickable from '../../../components/factionRoleClickable';

import { PANEL_VIEWS } from '../../../util/constants';


function FactionDetails({
    disabled, isGameStarted, panelViewState, game, ...setup
}){
    const factionID = panelViewState.entityType === PANEL_VIEWS.FACTION
        ? panelViewState.entityID : panelViewState.factionID;
    const faction = setup.factionMap[factionID];
    const bulletPoints = [
        ...(faction.description
            ? faction.description.split('\n')
            : []),
        ...faction.details,
        ...getAbilityDetails(faction),
        getEnemiesText(faction, setup),
        ...getPossibleRoles(faction, setup, isGameStarted),
    ];
    return (
        <EntityDetailsContainer
            deprecatedInputControls={!disabled && !isGameStarted
            && (<FactionModifiersEditor faction={faction} />)}
            headerText={faction.name}
            headerColor={faction.color}
            items={bulletPoints}
        />
    );
}

const mapStateToProps = ({
    gameState, panelViewState, setupState, session,
}) => ({
    isGameStarted: gameState.isStarted,
    panelViewState,
    ...setupState,
    session,
    game: gameState,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(FactionDetails);

function getAbilityDetails({ abilities }){
    return abilities.map(ability => [
        `Has shared faction ability ${ability.name}`,
        ability.description,
        ...ability.details,
    ]).flat();
}

function getEnemiesText(faction, setup){
    if(!faction.enemyIDs.length)
        return 'This faction has no enemies';
    return (
        <>
            Must eliminate:
            {' '}
            {faction.enemyIDs.map((enemyID, index) => (
                <Fragment key={enemyID}>
                    {!!index && ', '}
                    <FactionClickable faction={setup.factionMap[enemyID]} />
                </Fragment>
            ))}
            .
        </>
    );
}

function getPossibleRoles(faction, setup, isStarted){
    if(!isStarted)
        return [];
    return [(
        <Fragment key="possibleRoles">
            Possible roles:
            {' '}
            {faction.factionRoles.map((factionRole, index) => (
                <Fragment key={factionRole.id}>
                    {!!index && ', '}
                    <FactionRoleClickable factionRole={setup.factionRoleMap[factionRole.id]} />
                </Fragment>
            ))}
            .
        </Fragment>
    )];
}
