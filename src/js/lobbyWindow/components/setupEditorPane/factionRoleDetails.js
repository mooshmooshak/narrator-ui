import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { uniq } from 'lodash';

import EntityDetailsContainer from '../../../components/entityDetailsContainer';
import FactionRoleClickable from '../../../components/factionRoleClickable';
import FactionRoleEditor from './factionRoleEditor';
import HiddenClickable from '../../../components/hiddenClickable';

import { onShowFactionAction } from '../../../actions/payloads/panelViewActions';

import { getFactionRoleIDs } from '../../../util/hiddenUtils';

import roleImages from '../../../util/roleImages';


function FactionRoleDetails({
    disabled, isGameStarted, panelViewState, showFaction, ...setup
}){
    const { factionMap, factionRoleMap } = setup;
    const factionRole = factionRoleMap[panelViewState.entityID];

    const abilityNames = new Set(factionRole.abilities.map(ability => ability.name.toLowerCase()));
    const imageNames = roleImages.intersection(abilityNames);
    const imageName = imageNames.size ? [...imageNames][0] : 'citizen';

    const bulletPoints = [
        ...factionRole.details,
        ...getOtherVersionsBullet(factionMap, factionRole),
        getSpawnedFromBullet(setup, factionRole),
    ];

    return (
        <EntityDetailsContainer
            deprecatedInputControls={!disabled && !isGameStarted
                && (<FactionRoleEditor factionRole={factionRole} />)}
            headerText={factionRole.name}
            headerColor={factionMap[factionRole.factionID].color}
            imgName={imageName}
            items={bulletPoints}
            onSubheaderClick={() => showFaction(factionRole.factionID)}
            subheaderText={factionMap[factionRole.factionID].name}
        />
    );
}

const mapStateToProps = ({
    gameState, panelViewState, setupState,
}) => ({
    isGameStarted: gameState.isStarted,
    panelViewState,
    ...setupState,
});

const mapDispatchToProps = {
    showFaction: onShowFactionAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(FactionRoleDetails);

function getOtherVersionsBullet(factionMap, factionRole){
    const otherVersions = Object.values(factionMap)
        .map(faction => faction.factionRoles)
        .flat()
        .filter(otherFactionRole => otherFactionRole.id !== factionRole.id
            && otherFactionRole.roleID === factionRole.roleID);
    if(!otherVersions.length)
        return [];

    return [(
        <Fragment key="otherFactionRoleVersions">
            Also available in
            {' '}
            {otherVersions.map((fr, index) => (
                <Fragment key={fr.id}>
                    {!!index && ', '}
                    <FactionRoleClickable factionRole={fr} />
                </Fragment>
            ))}
            .
        </Fragment>
    )];
}

function getSpawnedFromBullet(setup, factionRole){
    let hiddens = setup.setupHiddens
        .map(setupHidden => setup.hiddenMap[setupHidden.hiddenID])
        .filter(hidden => getFactionRoleIDs(hidden).includes(factionRole.id));
    if(!hiddens.length)
        return 'This role cannot currently be spawned.';
    hiddens = hiddens.filter(hidden => getFactionRoleIDs(hidden).length > 1);
    if(!hiddens.length)
        return 'This role is guaranteed to be in the game.';

    hiddens = uniq(hiddens, 'id');

    return (
        <>
            Spawnable by:
            {' '}
            {hiddens.map((hidden, index) => (
                <Fragment key={hidden.id}>
                    {!!index && ', '}
                    <HiddenClickable hidden={hidden} />
                </Fragment>
            ))}
            .
        </>
    );
}
