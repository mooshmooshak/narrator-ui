import React, { useState } from 'react';
import { connect } from 'react-redux';

import {
    onShowFactionRoleAction,
    onShowHiddenAction, onHideEntityAction,
} from '../../../actions/payloads/panelViewActions';

import Button from '../../../components/baseElements/button';

import TitledListContainer from '../../../components/titledListContainer';

import FactionRoleDetails from './factionRoleDetails';
import HiddenDetails from './hiddenDetails';
import HiddenEditorContainer from './hiddenEditorContainer';

import { addSetupHidden } from '../../../services/setupHiddenService';

import { MAX_PLAYER_COUNT, PANEL_VIEWS } from '../../../util/constants';
import { getColor as getHiddenColor, getFactionRoleIDs } from '../../../util/hiddenUtils';
import { createHiddenAction, createHiddenSpawnsAction } from '../../../actions/hiddenActions';


const defaultSetupHiddenArgs = {
    isExposed: false,
    mustSpawn: false,
    minPlayerCount: 0,
    maxPlayerCount: MAX_PLAYER_COUNT,
};

function FactionRolesContainer({
    createHidden, createHiddenSpawns,
    disabled, isAdvancedMode, onHideEntity, onShowFactionRole,
    onShowHidden, panelViewState, setup,
}){
    const [hiddenEditorID, setHiddenEditorID] = useState();

    const items = getItems({ setup, panelViewState })
        .map(item => ({
            ...item,
            iconClass: !disabled && 'fa-plus-square',
            className: panelViewState.entityID === item.id && 'rSetupFocused',
        }));
    items.sort((a, b) => a.name.localeCompare(b.name));

    async function addFactionRoleToSetupHiddens(factionRoleID){
        const factionRole = setup.factionRoleMap[factionRoleID];
        let singleHidden = Object.values(setup.hiddenMap)
            .find(hidden => isHiddenOnlySpawningFactionRole(hidden, factionRole.id));
        if(!singleHidden){
            singleHidden = await createHidden(factionRole.name, setup.id);
            await createHiddenSpawns([{
                hiddenID: singleHidden.id,
                factionRoleID: factionRole.id,
            }]);
        }
        return addSetupHidden({
            hiddenID: singleHidden.id,
            ...defaultSetupHiddenArgs,
        });
    }

    function addEntityAttempt(entity){
        if(panelViewState.entityType === PANEL_VIEWS.HIDDEN)
            return addSetupHidden({
                ...defaultSetupHiddenArgs,
                hiddenID: entity.id,
            });
        return addFactionRoleToSetupHiddens(entity.id);
    }
    const canEditSetup = !disabled && isAdvancedMode;

    return (
        <div className="factionRolesContainer darkenedSetupPanel">
            <TitledListContainer
                containerClassName="factionRolesList"
                items={items}
                onActionIconClick={addEntityAttempt}
                onItemClick={item => {
                    if(item.id === panelViewState.entityID)
                        return onHideEntity();
                    const action = panelViewState.entityType === PANEL_VIEWS.HIDDEN
                        ? onShowHidden : onShowFactionRole;
                    action(item.id, item.factionID);
                }}
                titleText="Faction Roles"
            >
                {canEditSetup && (
                    <div className="setupEntityListButtons">
                        <Button
                            className="advCustomButton"
                            onClick={() => setHiddenEditorID(null)}
                        >New Hidden
                        </Button>
                    </div>
                )}
            </TitledListContainer>
            { PANEL_VIEWS.FACTION_ROLE === panelViewState.entityType
            && <FactionRoleDetails disabled={disabled} />}
            { PANEL_VIEWS.HIDDEN === panelViewState.entityType && <HiddenDetails />}
            <HiddenEditorContainer
                hidden={hiddenEditorID === null ? null : setup.hiddenMap[hiddenEditorID]}
                setHiddenID={setHiddenEditorID}
            />
        </div>
    );
}

const mapStateToProps = ({ panelViewState, setupState }) => ({
    setup: setupState,
    panelViewState,
});

const mapDispatchToProps = {
    createHidden: createHiddenAction,
    createHiddenSpawns: createHiddenSpawnsAction,
    onHideEntity: onHideEntityAction,
    onShowFactionRole: onShowFactionRoleAction,
    onShowHidden: onShowHiddenAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(FactionRolesContainer);

function isHiddenOnlySpawningFactionRole(hidden, factionRoleID){
    const factionRoleIDs = getFactionRoleIDs(hidden);
    return factionRoleIDs.length === 1 && factionRoleIDs.includes(factionRoleID);
}

function getItems({ panelViewState, setup }){
    const { factionMap } = setup;
    const faction = factionMap[panelViewState.factionID];
    switch (panelViewState.entityType){
    case PANEL_VIEWS.HIDDEN:
        return getHiddens(setup);
    case PANEL_VIEWS.FACTION:
        return factionMap[panelViewState.entityID].factionRoles.map(factionRole => ({
            ...factionRole,
            color: factionMap[panelViewState.entityID].color,
        }));
    case PANEL_VIEWS.FACTION_ROLE:
        return factionMap[faction.id].factionRoles.map(factionRole => ({
            ...factionRole,
            color: faction.color,
        }));
    default:
        return [];
    }
}

function getHiddens(setup){
    return Object.values(setup.hiddenMap)
        .filter(hidden => getFactionRoleIDs(hidden)
            .filter(frID => setup.factionRoleMap[frID].name === hidden.name).length !== 1)
        .map(hidden => ({
            ...hidden,
            color: getHiddenColor(hidden, setup),
        }));
}
