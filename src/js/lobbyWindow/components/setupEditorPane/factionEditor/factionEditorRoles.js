import React from 'react';
import { connect } from 'react-redux';

import DoubleListContainer from '../../../../components/doubleList/doubleListContainer';

import FactionEditorStep from './factionEditorStep';

import { create, deleteFactionRole } from '../../../../services/factionRoleService';

import { darkTextColor } from '../../../../util/colors';


function FactionEditorRoles({ disabled, faction, roles }){
    // eslint-disable-next-line no-nested-ternary
    const onLeftClick = disabled
        ? () => {}
        : deleteFactionRole;
    const list1 = {
        items: faction.factionRoles.map(fr => ({
            color: faction.color,
            onClick: onLeftClick,
            text: fr.name,
            value: fr.id,
        })),
        label: 'Spawnable',
    };

    const createdRoleIDs = new Set(faction.factionRoles.map(fr => fr.roleID));
    // eslint-disable-next-line no-nested-ternary
    const onRightClick = disabled
        ? () => {}
        : roleID => create(faction.id, roleID);
    const list2 = {
        items: roles
            .filter(role => !createdRoleIDs.has(role.id))
            .map(role => ({
                color: darkTextColor,
                onClick: onRightClick,
                text: role.name,
                value: role.id,
            })),
        label: 'Not Spawnable',
    };
    return (
        <FactionEditorStep>
            <DoubleListContainer
                list1={list1}
                list2={list2}
            />
        </FactionEditorStep>
    );
}

const mapStateToProps = ({ setupState }) => ({
    roles: Object.values(setupState.roleMap),
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(FactionEditorRoles);
