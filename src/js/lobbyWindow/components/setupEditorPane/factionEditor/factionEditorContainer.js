import React, { useState } from 'react';

import NavBarItem from '../../../../components/nav/navBarItem';
import Button from '../../../../components/baseElements/button';

import Toast from '../../../../components/toast';
import FloatingTintedContainer from '../../../../components/floatingTintedContainer';

import FactionEditorAbilities from './factionEditorAbilities';
import { FactionEditorDetails, INITIAL_FACTION_DETAILS } from './factionEditorDetails';
import FactionEditorEnemies from './factionEditorEnemies';
import FactionEditorRoles from './factionEditorRoles';

import { create as createFaction } from '../../../../services/factionService';


export default function FactionEditorContainer({
    disabled, faction, setFactionID, setupID,
}){
    const [step, setStep] = useState(0);
    const [factionView, setFactionView] = useState(INITIAL_FACTION_DETAILS);
    const [error, setError] = useState();
    if(faction === undefined)
        return null;

    const steps = [{
        body: (<FactionEditorDetails
            disabled={disabled}
            faction={faction}
            setError={setError}
            setView={setFactionView}
            setupID={setupID}
            view={factionView}
        />),
        navLabel: 'Details',
    }, {
        body: (<FactionEditorRoles disabled={disabled} faction={faction} />),
        navLabel: 'Roles',
    }, {
        body: (<FactionEditorEnemies disabled={disabled} faction={faction} />),
        navLabel: 'Allies',
    }, {
        body: (<FactionEditorAbilities disabled={disabled} faction={faction} />),
        navLabel: 'Abilities',
    }];

    async function handleButtonClick(){
        if(faction)
            return setStep(step + 1);
        try{
            const newFaction = await createFaction(setupID, factionView);
            setFactionID(newFaction.id);
            setStep(1);
        }catch(err){
            setError(err.errors[0]);
        }
    }

    return (
        <FloatingTintedContainer
            className="factionEditorContainer"
            setClosed={() => setFactionID(undefined)}
        >
            <div className="factionEditorNav">
                {steps.map((definition, index) => (
                    <NavBarItem
                        className={index === step ? 'selectedFactionEditorNav' : ''}
                        isOnWhite
                        key={definition.navLabel}
                        onClick={() => {
                            if(faction)
                                setStep(index);
                        }}
                    >
                        {definition.navLabel}
                    </NavBarItem>
                ))}
            </div>
            {steps[step].body}
            {step + 1 !== steps.length && (
                <Button
                    className="entityEditorButton"
                    disabled={!(factionView.name && factionView.color)}
                    onClick={handleButtonClick}
                >{faction ? 'Next' : 'Create'}
                </Button>
            )}
            <Toast
                isOpen={!!error}
                setClosed={setError}
                text={error || ''}
                type="error"
            />
        </FloatingTintedContainer>
    );
}
