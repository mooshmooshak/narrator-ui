import React from 'react';

export default function FactionEditorStep({ children }){
    return (
        <div className="factionEditorStepContainer">
            {children}
        </div>
    );
}
