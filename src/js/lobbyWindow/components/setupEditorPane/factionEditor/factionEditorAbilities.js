import React, { useEffect, useState } from 'react';

import DoubleListContainer from '../../../../components/doubleList/doubleListContainer';

import FactionEditorStep from './factionEditorStep';

import {
    create as createFactionAbility,
    del as deleteFactionAbility,
} from '../../../../services/factionAbilityService';

import { getBaseAbilities } from '../../../../services/baseAbilityService';

import { darkTextColor } from '../../../../util/colors';


export default function FactionEditorAbilities({ disabled, faction }){
    const [baseAbilities, setBaseAbilities] = useState([]);
    useEffect(() => {
        getBaseAbilities()
            .then(abilities => setBaseAbilities(
                abilities.filter(a => a.isFactionAllowed),
            ));
    }, []);
    const onLeftClick = disabled
        ? () => {}
        : abilityID => deleteFactionAbility(faction.id, abilityID);
    const list1 = {
        label: 'Available',
        items: faction.abilities.map(ability => ({
            color: faction.color,
            onClick: onLeftClick,
            text: ability.name,
            value: ability.id,
        })),
    };
    // eslint-disable-next-line no-nested-ternary
    const onRightClick = disabled
        ? () => {}
        : abilityType => createFactionAbility(faction.id, abilityType);

    const factionAbilityTypes = new Set(faction.abilities.map(a => a.name));
    const list2 = {
        label: 'Unavailable',
        items: baseAbilities
            .filter(({ type }) => !factionAbilityTypes.has(type))
            .map(ability => ({
                color: darkTextColor,
                onClick: onRightClick,
                text: ability.type,
                value: ability.type,
            })),
    };
    return (
        <FactionEditorStep>
            <DoubleListContainer
                list1={list1}
                list2={list2}
            />
        </FactionEditorStep>
    );
}
