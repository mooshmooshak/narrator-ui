import React, { useEffect } from 'react';

import Button from '../../../../components/baseElements/button';
import TextInput from '../../../../components/baseElements/textInput';

import FactionEditorStep from './factionEditorStep';

import {
    deleteFaction,
    update as updateFaction,
} from '../../../../services/factionService';


export const INITIAL_FACTION_DETAILS = { color: '', description: '', name: '' };

export function FactionEditorDetails({
    disabled, faction, setError, setView, setupID, view,
}){
    useEffect(() => {
        if(faction)
            setView(faction);
        else
            setView(INITIAL_FACTION_DETAILS);
    }, [faction]);

    function setViewValue(value, key){
        setView({
            ...view,
            [key]: value,
        });
    }

    async function tryUpdate(resetView = true){
        try{
            await updateFaction(setupID, faction.id, view);
        }catch(err){
            if(resetView)
                setView(faction);
            setError(err.errors[0]);
        }
    }

    function handleKeyUp(key){
        if(!faction)
            return;
        if(key === 'Enter')
            return tryUpdate(false);
    }

    return (
        <FactionEditorStep>
            <TextInput
                className="factionEditorDetailsTextInput"
                disabled={disabled}
                label="Name"
                onBlur={() => faction && tryUpdate()}
                onChange={value => setViewValue(value, 'name')}
                onKeyUp={handleKeyUp}
                value={view.name}
            />
            <TextInput
                className="factionEditorDetailsTextInput"
                disabled={!!faction}
                label="Color"
                onChange={value => setViewValue(value, 'color')}
                onKeyUp={handleKeyUp}
                value={view.color}
            />
            <TextInput
                className="factionEditorDetailsTextInput"
                disabled={disabled}
                label="Description"
                onBlur={tryUpdate}
                onChange={value => setViewValue(value, 'description')}
                onKeyUp={handleKeyUp}
                value={view.description}
            />
            {faction && !disabled && (
                <Button color="secondary" onClick={() => deleteFaction(faction.id)}>
                    Delete
                </Button>
            )}
        </FactionEditorStep>
    );
}
