import React, { useState } from 'react';
import { connect } from 'react-redux';

import Button from '../../../components/baseElements/button';
import Toggle from '../../../components/baseElements/toggle';

import FactionsContainer from './factionsContainer';
import FactionRolesContainer from './factionRolesContainer';
import SetupHiddensContainer from './setupHiddensContainer';

import GameDetails from '../../../gameWindow/infoPane/gameDetails';

import {
    onShowFactionRoleAction,
    onShowHiddenAction,
} from '../../../actions/payloads/panelViewActions';

import { cloneSetup, usePresetSetup } from '../../../services/setupService';

import { isModerator as getIsModerator } from '../../../util/gameUserUtil';
import { filterSetupHiddensByPlayerCount } from '../../../util/setupHiddenUtil';
import Toast from '../../../components/toast';


const INITIAL_CUSTOMIZE_BUTTON_STATE = {
    isEnabled: true,
    text: 'Customize',
};

function SetupEditorPane({
    disabled, showClonePrompt, showFactionRole, showHidden, gameState, setupState,
    isModerator,
}){
    const playerCount = Object.values(gameState.playerMap).length;
    const setupHiddens = filterSetupHiddensByPlayerCount(playerCount, setupState.setupHiddens);
    const isEditable = isModerator && !gameState.isStarted;
    const [isAdvancedMode, setIsAdvancedMode] = useState(false);

    // eslint-disable-next-line max-len
    const [customizeButtonState, setCustomizeButtonState] = useState(INITIAL_CUSTOMIZE_BUTTON_STATE);
    const [isToastOpen, setIsToastOpen] = useState(false);

    return (
        <div className="setupEditorPane">
            <div className="setupEditorHeader">
                {isModerator && !setupState.isEditable && (
                    <Button
                        className="setupEditorHeaderButton"
                        disabled={!customizeButtonState.isEnabled}
                        onClick={async() => {
                            setCustomizeButtonState({
                                isEnabled: false,
                                text: 'Cloning',
                            });
                            const setupID = await cloneSetup(setupState.id);
                            await usePresetSetup(gameState.id, setupID);
                            setCustomizeButtonState(INITIAL_CUSTOMIZE_BUTTON_STATE);
                            setIsToastOpen(true);
                        }}
                    >
                        {customizeButtonState.text}
                    </Button>
                )}
                <Toast
                    isOpen={isToastOpen}
                    setClosed={() => setIsToastOpen(false)}
                    type="info"
                    text="Clone complete."
                />
                <Toggle
                    className="setupEditorAdvancedToggle"
                    isRightSelected={isAdvancedMode}
                    onChange={setIsAdvancedMode}
                    label="Advanced Mode"
                />
            </div>
            <div className="setupEntitiesContainer">
                <div className="setupBaseEntitiesContainer">
                    <FactionsContainer disabled={disabled} isAdvancedMode={isAdvancedMode} />
                    <FactionRolesContainer disabled={disabled} isAdvancedMode={isAdvancedMode} />
                </div>
                <SetupHiddensContainer
                    canRemoveSetupHiddens={!disabled && isEditable}
                    className="darkenedSetupPanel"
                    playerCount={playerCount}
                    setup={setupState}
                    showClonePrompt={showClonePrompt}
                    showFactionRole={showFactionRole}
                    showHidden={showHidden}
                    titleText={`Roles List (${setupHiddens.length})`}
                />
            </div>
            <GameDetails />
        </div>
    );
}

const mapStateToProps = ({ setupState, gameState, session }) => ({
    isModerator: getIsModerator(session.userID, gameState.users),
    gameState,
    setupState,
});

const mapDispatchToProps = {
    showFactionRole: onShowFactionRoleAction,
    showHidden: onShowHiddenAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(SetupEditorPane);
