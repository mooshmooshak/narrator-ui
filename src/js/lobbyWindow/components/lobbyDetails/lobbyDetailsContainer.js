import React, { useState } from 'react';

import LobbyEditor from '../lobbyEditor/lobbyEditor';
import MiscDetails from './miscDetails';
import PhaseDetails from './phaseDetails';
import VoteDetails from './voteDetails';
import SetupOverview from './setupOverview';


export default function LobbyDetailsContainer({
    gameModifiers, isModerator, setupModifiers, showSetup,
}){
    const [isEditorOpen, setIsEditorOpen] = useState(false);

    const [isExpanded, setIsExpanded] = useState(false);
    const [showMoreIconClassName, showMoreText] = isExpanded
        ? ['lobbyOverviewAdvancedIcon fas fa-chevron-up', 'Show less']
        : ['lobbyOverviewAdvancedIcon fas fa-chevron-down', 'Show more'];

    const openEditor = isModerator && function(){
        return setIsEditorOpen(true);
    };

    return (
        <div className="lobbyDetailsContainer">
            <SetupOverview showSetup={showSetup} />
            <div className="lobbyOverviewDetailsContainer">
                {isEditorOpen && (<LobbyEditor setClosed={() => setIsEditorOpen(false)} />)}
                <PhaseDetails
                    gameModifiers={gameModifiers}
                    isExpanded={isExpanded}
                    openEditor={openEditor}
                />
                { isExpanded
                && (
                    <>
                        <VoteDetails setupModifiers={setupModifiers} />
                        <MiscDetails
                            gameModifiers={gameModifiers}
                            setupModifiers={setupModifiers}
                        />
                    </>
                )}
            </div>
            <span
                className="showMoreText"
                onClick={() => setIsExpanded(!isExpanded)}
            >
                {showMoreText}<i className={showMoreIconClassName} />
            </span>
        </div>
    );
}
