import React from 'react';

import LobbyDetailsTextSubsection from './lobbyDetailsTextSubsection';

import { getSetupModifierValue } from '../../../util/modifierUtil';


const voteModifierDetails = ['SECRET_VOTES', 'SELF_VOTE', 'SKIP_VOTE'];

export default function VoteDetails({ setupModifiers }){
    const items = voteModifierDetails.map(modifierName => ({
        key: modifierName,
        text: getVoteModifierText(modifierName, setupModifiers),
    }));
    return (
        <LobbyDetailsTextSubsection label="Vote Settings" items={items} />
    );
}

function getVoteModifierText(name, modifiers){
    const value = getSetupModifierValue(name, { modifiers });
    if(name === 'SECRET_VOTES'){
        if(value)
            return 'Votes are hidden.';
        return 'Votes are public.';
    }
    if(name === 'SELF_VOTE'){
        if(value)
            return 'Players may self vote.';
        return 'Players may not self vote.';
    }
    if(name === 'SKIP_VOTE'){
        if(value)
            return 'Players may vote to eliminate no one.';
        return 'Players may not vote to eliminate no one.';
    }
}
