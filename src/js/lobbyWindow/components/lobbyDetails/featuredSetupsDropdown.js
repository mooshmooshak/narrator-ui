import React from 'react';
import { connect } from 'react-redux';

import SetupDropdown from '../../../landingWindow/components/gameCreate/setupDropdown';

import { usePresetSetup } from '../../../services/setupService';


function FeaturedSetupsDropdown({
    className, setups, gameID, setupID,
}){
    return (
        <SetupDropdown
            className={className}
            onChange={newSetupID => usePresetSetup(gameID, newSetupID)}
            setups={setups}
            setupID={setupID}
        />
    );
}

const mapStateToProps = ({ gameState, setupsState, setupState }) => ({
    setups: [...setupsState.featuredSetups, ...setupsState.userSetups],
    gameID: gameState.id,
    setupID: setupState.id || '',
});

export default connect(mapStateToProps, {})(FeaturedSetupsDropdown);
