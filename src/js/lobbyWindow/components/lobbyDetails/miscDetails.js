import React from 'react';

import LobbyDetailsTextSubsection from './lobbyDetailsTextSubsection';

import { getGameModifierValue, getSetupModifierValue } from '../../../util/modifierUtil';


const miscModifierDetails = ['CHAT_ROLES', 'OMNISCIENT_DEAD', 'LAST_WILL', 'CHARGE_VARIABILITY',
    'AUTO_PARITY'];

export default function MiscDetails({ gameModifiers, setupModifiers }){
    const items = miscModifierDetails.map(modifierName => ({
        key: modifierName,
        text: getMiscModifierText(modifierName, { modifiers: setupModifiers },
            { modifiers: gameModifiers }),
    }));
    return (
        <LobbyDetailsTextSubsection items={items} label="Misc Settings" />
    );
}

function getMiscModifierText(name, setup, game){
    if(name === 'CHAT_ROLES'){
        const value = getGameModifierValue(name, game);
        if(value)
            return 'Chat roles are enabled.';
        return 'Some text-based roles will not spawn.';
    }
    if(name === 'OMNISCIENT_DEAD'){
        const value = getGameModifierValue(name, game);
        if(value)
            return 'Dead players know gain access to living players\' roles.';
        return 'Dead players do not gain access to living players\' roles.';
    }
    if(name === 'LAST_WILL'){
        const value = getSetupModifierValue(name, setup);
        if(value)
            return 'Players may submit a last will.';
        return 'No last wills.';
    }
    if(name === 'CHARGE_VARIABILITY'){
        const value = getGameModifierValue(name, game);
        if(value)
            return `Charge variation is ${value}.`;
        return 'The number of charges on role card are exactly what a player will receive.';
    }
    if(name === 'AUTO_PARITY'){
        const value = getSetupModifierValue(name, setup);
        if(value)
            return 'Game will auto skip day if it thinks day will only end in skip.';
        return 'Even if game thinks day will only end in skip, day will not auto skip.';
    }
}
