import React from 'react';
import { connect } from 'react-redux';

import SetupHiddensContainer from './setupEditorPane/setupHiddensContainer';

import { isModerator as getIsModerator } from '../../util/gameUserUtil';
import { filterSetupHiddensByPlayerCount } from '../../util/setupHiddenUtil';


function LobbySetupHiddens({
    isModerator, onStartClick, playerCount, setup, showClonePrompt,
}){
    const setupHiddens = filterSetupHiddensByPlayerCount(playerCount, setup.setupHiddens);
    const noOp = () => {};
    return (
        <SetupHiddensContainer
            canRemoveSetupHiddens={isModerator}
            className="lobbyDesktopWidth lobbySetupHiddens"
            onStartClick={isModerator && onStartClick}
            playerCount={playerCount}
            setup={setup}
            showClonePrompt={showClonePrompt}
            showFactionRole={noOp}
            showHidden={noOp}
            titleText={`Roles List (${setupHiddens.length})`}
        />
    );
}

const mapStateToProps = ({ gameState, session, setupState }) => ({
    isModerator: getIsModerator(session.userID, gameState.users),
    playerCount: Object.values(gameState.playerMap).length,
    setup: setupState,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(LobbySetupHiddens);
