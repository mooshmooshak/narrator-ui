import React, { useState } from 'react';

import ParticipantButtons from './participantButtons';
import ParticipantNameHeader from './participantNameHeader';


export default function ParticipantOverview({ participant }){
    const {
        color, icon, id: playerID, isActive, isModerator, name, userID: playerUserID,
    } = participant;
    const [isExpanded, setIsExpanded] = useState(false);

    const iconClassName = isExpanded ? 'fas fa-chevron-up' : 'fas fa-chevron-down';

    return (
        <div className="playerOverview" onClick={() => setIsExpanded(!isExpanded)}>
            <span className="playerOverviewHeaderContainer">
                <ParticipantNameHeader
                    color={color}
                    icon={icon}
                    name={name}
                    playerID={playerID}
                    playerUserID={participant.userID}
                />
                <i className={`playerOverviewMenu ${iconClassName}`} />
            </span>
            {isExpanded && (
                <>
                    <div className="playerOverviewDetails">
                        {/* eslint-disable-next-line max-len */}
                        {!playerUserID && (<span className="playerOverviewDetail">bot player</span>)}
                        {isModerator && (<span className="playerOverviewDetail">moderator</span>)}
                        {isActive && (<span className="playerOverviewDetail">active</span>)}
                        {/* <span>Win rate: 0%</span> */}
                        {/* <span>Games played: 50</span> */}
                        {/* <span>Points: 3291</span> */}
                    </div>
                    <ParticipantButtons participant={participant} />
                </>
            )}
        </div>
    );
}
