import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import TextInput from '../../../components/baseElements/textInput';
import Toast from '../../../components/toast';

import PlayerIcon from '../../../components/playerIcon';

import { updatePlayerNameAction } from '../../../actions/playerActions';


function ParticipantNameHeader({
    color, icon, name, playerID, playerUserID, updatePlayerName, userID,
}){
    const [isEditing, setIsEditing] = useState(false);
    const [nameValue, setNameValue] = useState(name);
    const playerNameValue = nameValue === undefined ? name : nameValue;
    const [error, setError] = useState();
    const [isTextInputEnabled, setIsTextInputEnabled] = useState(true);
    useEffect(() => {
        setNameValue(name);
    }, [name]);

    async function tryUpdate(){
        setIsTextInputEnabled(false);
        try{
            await updatePlayerName(playerID, nameValue);
        }catch(err){
            setError(err.errors[0]);
        }

        setIsEditing(false);
    }

    function handleKeyUp(key){
        if(key === 'Escape')
            return tryUpdate();
        if(key === 'Enter')
            return tryUpdate();
    }

    return (
        <span className="playerOverviewHeaderTextContainer">
            <PlayerIcon color={color} icon={icon} />
            {!isEditing && (
                <>
                    <span style={{ color }} className="playerOverviewHeaderText">{name}</span>
                    {userID === playerUserID && (
                        <i
                            className="playerNameEditIcon fas fa-pencil-alt"
                            onClick={e => {
                                setIsEditing(true);
                                e.stopPropagation();
                                setIsTextInputEnabled(true);
                            }}
                        />
                    )}
                </>
            )}
            {isEditing && (
                <>
                    <TextInput
                        disabled={!isTextInputEnabled}
                        onBlur={tryUpdate}
                        onChange={setNameValue}
                        onKeyUp={handleKeyUp}
                        type="text"
                        value={playerNameValue}
                    />
                </>
            )}
            <Toast
                isOpen={!!error}
                setClosed={setError}
                text={error || ''}
                type="error"
            />
        </span>
    );
}

const mapStateToProps = ({ session }) => ({
    userID: session.userID,
});

const mapDispatchToProps = {
    updatePlayerName: updatePlayerNameAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(ParticipantNameHeader);
