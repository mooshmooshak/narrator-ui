import React from 'react';

import NavBarItem from '../../components/nav/navBarItem';

import { LOBBY_PANE_TYPE } from '../lobbyPaneType';


export default function LobbyNavBarItems({ closeDrawer, leaveGame, setActivePane }){
    function goTo(lobbyPaneType){
        setActivePane(lobbyPaneType);
        closeDrawer();
    }

    return (
        <>
            <NavBarItem iconClassName="fas fa-tv" onClick={() => goTo(LOBBY_PANE_TYPE.DETAILS)}>
                Overview
            </NavBarItem>
            <NavBarItem iconClassName="fas fa-book" onClick={() => goTo(LOBBY_PANE_TYPE.SETUP)}>
                Setup
            </NavBarItem>
            <NavBarItem iconClassName="fas fa-arrow-left" onClick={leaveGame}>
                Leave
            </NavBarItem>
        </>
    );
}
