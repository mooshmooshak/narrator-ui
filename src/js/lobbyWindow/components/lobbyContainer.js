import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import NavBar from '../../components/nav/navBar';

import LobbyChatPane from './lobbyChatPane';
import LobbyChatTicker from './lobbyChatTicker';
import LobbyFullPageContainer from './lobbyFullPageContainer';
import LobbyNavBarItems from './lobbyNavBarItems';
import SetupEditorPane from './setupEditorPane/setupEditorPane';

import { LOBBY_PANE_TYPE } from '../lobbyPaneType';

import { getLobbyMessages } from '../../services/chatService';
import { leave as leaveLobby } from '../../services/playerService';

import { onLobbyLeaveAction } from '../../actions/payloads/profileActions';

import { isModerator as getIsModerator } from '../../util/gameUserUtil';


function LobbyContainer({
    gameID, gameModifiers, isModerator, isStarted, lobbyID, onLobbyLeave, setup, userID,
}){
    const [lobbyPaneType, setLobbyPaneType] = useState(LOBBY_PANE_TYPE.DETAILS);

    useEffect(() => {
        if(!isStarted)
            require('../../index').goToSetupPage();
    }, [isStarted]);

    useEffect(() => {
        if(gameID && !isStarted)
            getLobbyMessages(gameID);
    }, [gameID, isStarted]);

    if(isStarted)
        return null;

    async function leaveGame(){
        await leaveLobby();
        onLobbyLeave();
    }

    const isSetupEditable = setup.isEditable && setup.ownerID === userID;

    let activePane;
    switch (lobbyPaneType){
    case LOBBY_PANE_TYPE.SETUP:
        activePane = (<SetupEditorPane disabled={!isSetupEditable} />);
        break;

    case LOBBY_PANE_TYPE.CHAT:
        activePane = (
            <LobbyChatPane />
        );
        break;

    default:
        activePane = (
            <LobbyFullPageContainer
                gameModifiers={gameModifiers}
                isModerator={isModerator}
                lobbyID={lobbyID}
                setupModifiers={setup.modifiers}
                showSetup={() => setLobbyPaneType(LOBBY_PANE_TYPE.SETUP)}
            />
        );
    }

    const setupEditorClass = lobbyPaneType === LOBBY_PANE_TYPE.SETUP ? ' setupContainer' : '';

    return (
        <>
            <NavBar headerText={lobbyPaneType}>
                <LobbyNavBarItems leaveGame={leaveGame} setActivePane={setLobbyPaneType} />
            </NavBar>
            <div className={`fullPageContainer lobbyContainer${setupEditorClass}`}>
                {activePane}
            </div>
            {lobbyPaneType !== LOBBY_PANE_TYPE.CHAT
                && (
                    <LobbyChatTicker
                        onExpand={() => setLobbyPaneType(LOBBY_PANE_TYPE.CHAT)}
                    />
                )}
        </>
    );
}

const mapStateToProps = ({ gameState, session, setupState }) => ({
    gameID: gameState.id,
    gameModifiers: gameState.modifiers,
    isModerator: getIsModerator(session.userID, gameState.users),
    isStarted: gameState.isStarted,
    lobbyID: gameState.lobbyID,
    setup: {
        isEditable: setupState.isEditable,
        modifiers: setupState.modifiers,
        ownerID: setupState.ownerID,
    },
    userID: session.userID,
});

const mapDispatchToProps = {
    onLobbyLeave: onLobbyLeaveAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(LobbyContainer);
