export const LOBBY_PANE_TYPE = {
    CHAT: 'Lobby Chat',
    DETAILS: 'Overview',
    SETUP: 'Setup',
};
