export const TIME_LENGTH = {
    SEC: {
        label: 'sec',
        value: 1,
    },
    MIN: {
        label: 'min',
        value: 60,
    },
    HOUR: {
        label: 'hr',
        value: 60 * 60,
    },
    DAY: {
        label: 'day',
        value: 60 * 60 * 24,
    },
};

export function getDurationObject(seconds){
    if(seconds === undefined)
        return TIME_LENGTH.MIN;
    if(seconds < 180)
        return TIME_LENGTH.SEC;
    if(seconds / TIME_LENGTH.MIN.value <= 90) // minutes
        return TIME_LENGTH.MIN;
    if(seconds / TIME_LENGTH.DAY.value <= 24) // hours
        return TIME_LENGTH.HOUR;
    return TIME_LENGTH.DAY;
}
