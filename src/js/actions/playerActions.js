import { onPlayerNameUpdateAction } from './payloads/gameActions';

import { updatePlayerName } from '../services/playerService';
import { savePlayerName } from '../services/storageService';


export function updatePlayerNameAction(playerID, playerName){
    return async dispatch => {
        await updatePlayerName(playerID, playerName);
        dispatch(onPlayerNameUpdateAction(playerID, playerName));
        savePlayerName(playerName);
    };
}
