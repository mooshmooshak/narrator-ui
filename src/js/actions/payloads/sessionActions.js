export const SessionActions = {
    ON_FIREBASE_USER_UPDATE: 'ON_FIREBASE_USER_UPDATE',
    ON_USER_UPDATE: 'session/ON_USER_UPDATE',
    ON_WEBSOCKET_UPDATE: 'session/ON_WEBSOCKET_UPDATE',
};

export function onFirebaseUserUpdateAction(isLoggedIn){
    return {
        type: SessionActions.ON_FIREBASE_USER_UPDATE,
        payload: isLoggedIn,
    };
}

export function onUserUpdateAction(user){
    return {
        type: SessionActions.ON_USER_UPDATE,
        payload: user,
    };
}

export function onWebSocketUpdate(isConnected){
    return {
        type: SessionActions.ON_WEBSOCKET_UPDATE,
        payload: isConnected,
    };
}
