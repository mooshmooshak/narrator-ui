export const SetupsActions = {
    ON_GET_FEATURED_SETUPS: 'setups/ON_GET_FEATURED_SETUPS',
    ON_GET_USER_SETUPS: 'setups/ON_GET_USER_SETUPS',
};

export function onGetFeaturedSetupsAction(featuredSetups){
    return {
        type: SetupsActions.ON_GET_FEATURED_SETUPS,
        payload: featuredSetups,
    };
}

export function onGetUserSetupsAction(userSetups){
    return {
        type: SetupsActions.ON_GET_USER_SETUPS,
        payload: userSetups,
    };
}
