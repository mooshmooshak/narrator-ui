export const ChatActions = {
    ON_LOBBY_CHAT_INPUT_UPDATE: 'chat/ON_LOBBY_CHAT_INPUT_UPDATE',
    ON_LOBBY_CHAT_LOAD: 'chat/ON_LOBBY_CHAT_LOAD',
    ON_NEW_LOBBY_MESSAGE: 'chat/ON_NEW_LOBBY_MESSAGE',
};

export function onLobbyChatInputUpdate(newValue){
    return {
        type: ChatActions.ON_LOBBY_CHAT_INPUT_UPDATE,
        payload: newValue,
    };
}

export function onLobbyChatLoad(messages){
    return {
        type: ChatActions.ON_LOBBY_CHAT_LOAD,
        payload: messages,
    };
}

export function onNewLobbyMessage(message){
    return {
        type: ChatActions.ON_NEW_LOBBY_MESSAGE,
        payload: message,
    };
}
