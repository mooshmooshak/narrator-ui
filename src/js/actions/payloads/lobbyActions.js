export const LobbyActions = {
    ON_LOBBY_CREATE: 'lobby/ON_LOBBY_CREATE',
    ON_LOBBY_DELETE: 'lobby/ON_LOBBY_DELETE',
    ON_LOBBY_GET: 'lobby/ON_LOBBY_GET',
};

export function onLobbiesLoadAction(lobbies){
    return {
        type: LobbyActions.ON_LOBBY_GET,
        payload: lobbies,
    };
}

export function onLobbyCreateAction(lobby){
    return {
        type: LobbyActions.ON_LOBBY_CREATE,
        payload: lobby,
    };
}

export function onLobbyDeleteAction(gameID){
    return {
        type: LobbyActions.ON_LOBBY_DELETE,
        payload: gameID,
    };
}
