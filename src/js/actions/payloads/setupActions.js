export const SetupActions = {
    ON_FACTION_ABILITY_CREATE: 'setup/ON_FACTION_ABILITY_CREATE',
    ON_FACTION_ABILITY_DELETE: 'setup/ON_FACTION_ABILITY_DELETE',
    ON_FACTION_CREATE: 'setup/ON_FACTION_CREATE',
    ON_FACTION_DELETE: 'setup/ON_FACTION_DELETE',
    ON_FACTION_ENEMY_ADD: 'setup/ON_FACTION_ENEMY_ADD',
    ON_FACTION_ENEMY_DELETE: 'setup/ON_FACTION_ENEMY_DELETE',
    ON_FACTION_MODIFIER_UPDATE: 'setup/ON_FACTION_MODIFIER_UPDATE',
    ON_FACTION_UPDATE: 'setup/ON_FACTION_UPDATE',

    ON_FACTION_ROLE_CREATE: 'setup/ON_FACTION_ROLE_CREATE',
    ON_FACTION_ROLE_DELETE: 'setup/ON_FACTION_ROLE_DELETE',
    ON_FACTION_ROLE_UPDATE: 'setup/ON_FACTION_ROLE_UPDATE',

    ON_HIDDEN_CREATE: 'setup/ON_HIDDEN_CREATE',
    ON_HIDDEN_SPAWNS_CREATE: 'setup/ON_HIDDEN_SPAWNS_CREATE',
    ON_HIDDEN_SPAWN_DELETE: 'setup/ON_HIDDEN_SPAWN_DELETE',

    ON_SETUP_CHANGE: 'setup/ON_SETUP_CHANGE',

    ON_SETUP_HIDDEN_ADD: 'setup/ON_SETUP_HIDDEN_ADD',
    ON_SETUP_HIDDEN_REMOVE: 'setup/ON_SETUP_HIDDEN_REMOVE',

    ON_SETUP_MODIFIER_UPDATE: 'setup/ON_SETUP_MODIFIER_UPDATE_ACTION',
};

export function onFactionAbilityCreateAction(factionID, ability){
    return {
        type: SetupActions.ON_FACTION_ABILITY_CREATE,
        payload: { factionID, ability },
    };
}

export function onFactionAbilityDeleteAction(factionID, abilityID){
    return {
        type: SetupActions.ON_FACTION_ABILITY_DELETE,
        payload: { factionID, abilityID },
    };
}

export function onFactionCreateAction(faction){
    return {
        type: SetupActions.ON_FACTION_CREATE,
        payload: faction,
    };
}

export function onFactionEnemyAddAction(factionID, enemyFactionID){
    return {
        type: SetupActions.ON_FACTION_ENEMY_ADD,
        payload: { factionID, enemyFactionID },
    };
}

export function onFactionEnemyDeleteAction(factionID, enemyFactionID){
    return {
        type: SetupActions.ON_FACTION_ENEMY_DELETE,
        payload: { factionID, enemyFactionID },
    };
}

export function onFactionDeleteAction(factionID){
    return {
        type: SetupActions.ON_FACTION_DELETE,
        payload: factionID,
    };
}

export function onFactionModifierUpdateAction(factionID, name, value){
    return {
        type: SetupActions.ON_FACTION_MODIFIER_UPDATE,
        payload: { factionID, name, value },
    };
}

export function onFactionRoleCreateAction(factionRole){
    return {
        type: SetupActions.ON_FACTION_ROLE_CREATE,
        payload: factionRole,
    };
}

export function onFactionRoleDeleteAction(factionRoleID){
    return {
        type: SetupActions.ON_FACTION_ROLE_DELETE,
        payload: factionRoleID,
    };
}

export function onFactionRoleUpdateAction(factionRole){
    return {
        type: SetupActions.ON_FACTION_ROLE_UPDATE,
        payload: factionRole,
    };
}

export function onFactionUpdateAction(factionID, { description, name }){
    return {
        type: SetupActions.ON_FACTION_UPDATE,
        payload: {
            description,
            factionID,
            name,
        },
    };
}

export function onHiddenCreateAction(hidden){
    return {
        type: SetupActions.ON_HIDDEN_CREATE,
        payload: hidden,
    };
}

export function onHiddenSpawnsAdd(hiddenSpawns){
    return {
        type: SetupActions.ON_HIDDEN_SPAWNS_CREATE,
        payload: hiddenSpawns,
    };
}

export function onHiddenSpawnDelete(hiddenID, hiddenSpawnID){
    return {
        type: SetupActions.ON_HIDDEN_SPAWN_DELETE,
        payload: {
            hiddenID,
            hiddenSpawnID,
        },
    };
}

export function onSetupChangeAction(setup){
    return {
        type: SetupActions.ON_SETUP_CHANGE,
        payload: setup,
    };
}

export function onSetupHiddenAddAction(setupHidden){
    return {
        type: SetupActions.ON_SETUP_HIDDEN_ADD,
        payload: setupHidden,
    };
}

export function onSetupHiddenRemoveAction(setupHiddenID){
    return {
        type: SetupActions.ON_SETUP_HIDDEN_REMOVE,
        payload: setupHiddenID,
    };
}

export function onSetupModifierUpdateAction(name, value){
    return {
        type: SetupActions.ON_SETUP_MODIFIER_UPDATE,
        payload: { name, value },
    };
}
