export const PanelViewActions = {
    ON_HIDE_ENTITY: 'panelView/ON_HIDE_ENTITY',
    ON_SHOW_FACTION: 'panelView/ON_SHOW_FACTION',
    ON_SHOW_FACTION_ROLE: 'panelView/ON_SHOW_FACTION_ROLE',
    ON_SHOW_HIDDEN: 'panelView/ON_SHOW_HIDDEN',
    ON_SHOW_PLAYER: 'panelView/ON_SHOW_PLAYER',

    ON_VOTE_SORT: 'panelView/ON_VOTE_SORT',
};

export function onHideEntityAction(){
    return {
        type: PanelViewActions.ON_HIDE_ENTITY,
    };
}

export function onShowFactionAction(factionID){
    return {
        type: PanelViewActions.ON_SHOW_FACTION,
        payload: factionID,
    };
}

export function onShowFactionRoleAction(factionRoleID, factionID){
    return {
        type: PanelViewActions.ON_SHOW_FACTION_ROLE,
        payload: { factionRoleID, factionID },
    };
}

export function onShowHiddenAction(hiddenID){
    return {
        type: PanelViewActions.ON_SHOW_HIDDEN,
        payload: hiddenID,
    };
}

export function onShowPlayerAction(playerName){
    return {
        type: PanelViewActions.ON_SHOW_PLAYER,
        payload: playerName,
    };
}

export function onVoteSortAction(voteSortType){
    return {
        type: PanelViewActions.ON_VOTE_SORT,
        payload: voteSortType,
    };
}
