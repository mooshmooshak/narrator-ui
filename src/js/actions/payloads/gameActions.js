/* eslint-env browser */
import { keyBy } from 'lodash';

export const GameActions = {
    ON_GAME_MODIFIER_UPDATE: 'game/ON_GAME_MODIFIER_UPDATE',
    ON_GAME_UPDATE: 'game/ON_GAME_UPDATE',
    ON_GAME_USER_ACTIVITY_CHANGE: 'game/ON_GAME_USER_ACTIVITY_CHANGE',
    ON_GAME_USER_ACTIVITY_LOAD: 'game/ON_GAME_USER_ACTIVITY_LOAD',
    ON_HOST_UPDATE: 'game/ON_HOST_UPDATE',
    ON_PLAYERS_UPDATE: 'game/ON_PLAYERS_UPDATE',
    ON_PLAYER_NAME_UPDATE: 'game/ON_PLAYER_NAME_UPDATE',
    ON_PLAYER_EXIT: 'game/ON_PAYER_EXIT',
    ON_TRIAL_START: 'game/ON_TRIAL_START',
    ON_VOTE_DELETE: 'game/ON_VOTE_DELETE',
    ON_VOTE_UPDATE: 'game/ON_VOTE_UPDATE',
    ON_VOTES_UPDATE: 'game/ON_VOTES_UPDATE',
};

export function onGameModifierUpdate(payload){
    return {
        type: GameActions.ON_GAME_MODIFIER_UPDATE,
        payload,
    };
}

export function onGameUpdate(payload){
    return {
        type: GameActions.ON_GAME_UPDATE,
        payload,
    };
}

export function onGameUserActivityChange(userIDisActive){
    return {
        type: GameActions.ON_GAME_USER_ACTIVITY_CHANGE,
        payload: userIDisActive,
    };
}

export function onGameUserActivityLoad(activeUserIDs){
    return {
        type: GameActions.ON_GAME_USER_ACTIVITY_LOAD,
        payload: activeUserIDs,
    };
}

export function onHostChangeAction(moderatorIDs){
    return {
        type: GameActions.ON_HOST_UPDATE,
        payload: moderatorIDs,
    };
}

export function onPlayersUpdateAction(players){
    window.gameState.playerMap = {
        ...window.gameState.playerMap,
        ...keyBy(players, 'name'),
    };
    return {
        type: GameActions.ON_PLAYERS_UPDATE,
        payload: players,
    };
}

export function onPlayerExitAction(playerName){
    return {
        type: GameActions.ON_PLAYER_EXIT,
        payload: playerName,
    };
}

export function onPlayerNameUpdateAction(playerID, playerName){
    return {
        type: GameActions.ON_PLAYER_NAME_UPDATE,
        payload: { playerID, playerName },
    };
}

export function onTrialStart(event){
    return {
        type: GameActions.ON_TRIAL_START,
        payload: event,
    };
}

export function onVoteDeleteAction(unvoterName){
    return {
        type: GameActions.ON_VOTE_DELETE,
        payload: unvoterName,
    };
}

export function onVoteUpdateAction(voteUpdate){
    return {
        type: GameActions.ON_VOTE_UPDATE,
        payload: voteUpdate,
    };
}

export function onVotesUpdateAction(voteInfo){
    return {
        type: GameActions.ON_VOTES_UPDATE,
        payload: voteInfo,
    };
}
