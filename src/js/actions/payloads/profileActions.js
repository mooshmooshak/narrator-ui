/* eslint-env browser */
export const ProfileActions = {
    ACTION_REMOVE: 'profile/ACTION_REMOVE',
    ACTIONS_UPDATE: 'profile/ACTIONS_UPDATE',
    LOBBY_LEAVE: 'profile/LOBBY_LEAVE',
    UPDATE_PROFILE: 'profile/UPDATE_PROFILE',
};


export function onProfileUpdateAction(profile){
    window.gameState.profile = profile;
    return {
        type: ProfileActions.UPDATE_PROFILE,
        payload: profile,
    };
}

export function onActionRemoveAction(actionIndex){
    window.gameState.actions.actionList.splice(actionIndex, 1);
    return {
        type: ProfileActions.ACTION_REMOVE,
        payload: actionIndex,
    };
}

export function onActionsUpdateAction(actions){
    window.gameState.actions = actions;
    return {
        type: ProfileActions.ACTIONS_UPDATE,
        payload: actions.actionList,
    };
}

export function onLobbyLeaveAction(){
    return {
        type: ProfileActions.LOBBY_LEAVE,
    };
}
