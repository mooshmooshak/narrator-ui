import { addSpawns, createHidden, deleteSpawn } from '../services/hiddenService';

import {
    onHiddenCreateAction, onHiddenSpawnDelete, onHiddenSpawnsAdd,
} from './payloads/setupActions';

export function createHiddenAction(hiddenName, setupID){
    return async dispatch => {
        const hidden = await createHidden({ name: hiddenName, setupID });
        dispatch(onHiddenCreateAction(hidden));
        return hidden;
    };
}

export function createHiddenSpawnsAction(spawns){
    return async dispatch => {
        spawns = await addSpawns(spawns);
        dispatch(onHiddenSpawnsAdd(spawns));
    };
}

export function deleteHiddenSpawnAction(hiddenID, hiddenSpawnID){
    return async dispatch => {
        await deleteSpawn(hiddenSpawnID);
        dispatch(onHiddenSpawnDelete(hiddenID, hiddenSpawnID));
    };
}
