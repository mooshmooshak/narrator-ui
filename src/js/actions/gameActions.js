import { onGameModifierUpdate } from './payloads/gameActions';

import { upsert as upsertGameModifier } from '../services/gameModifierService';


export function updateGameModifierAction(name, value){
    return async dispatch => {
        await upsertGameModifier(name, value);
        dispatch(onGameModifierUpdate({ name, value }));
    };
}
