import React from 'react';
import { connect } from 'react-redux';

import FactionDetails from '../../lobbyWindow/components/setupEditorPane/factionDetails';
import FactionRoleDetails from '../../lobbyWindow/components/setupEditorPane/factionRoleDetails';
import GraveDetails from './graveyard/graveDetails';
import HiddenDetails from '../../lobbyWindow/components/setupEditorPane/hiddenDetails';

import { PANEL_VIEWS } from '../../util/constants';


function GameDetails({ panelViewState }){
    return (
        <>
            { panelViewState.entityType === PANEL_VIEWS.FACTION_ROLE
            && <FactionRoleDetails />}
            { panelViewState.entityType === PANEL_VIEWS.FACTION && <FactionDetails />}
            { panelViewState.entityType === PANEL_VIEWS.HIDDEN && <HiddenDetails />}
            { panelViewState.entityType === PANEL_VIEWS.PLAYER && <GraveDetails />}
        </>
    );
}

const mapStateToProps = ({ panelViewState }) => ({ panelViewState });

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(GameDetails);
