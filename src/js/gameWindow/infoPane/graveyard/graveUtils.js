import { white } from '../../../util/colors';


export function getDeadPlayerText(deadPlayer){
    const roleName = deadPlayer.flip.factionID ? `(${deadPlayer.flip.roleName})` : '????';
    return `${deadPlayer.name} ${roleName}`;
}

export function getGraveyardColor(deadPlayer, factionMap){
    if(!deadPlayer.flip.factionID)
        return white;
    return factionMap[deadPlayer.flip.factionID].color;
}
