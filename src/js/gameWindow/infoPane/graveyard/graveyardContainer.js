import React from 'react';
import { connect } from 'react-redux';

import TitledListContainer from '../../../components/titledListContainer';

import { onShowPlayerAction } from '../../../actions/payloads/panelViewActions';

import { getDeadPlayerText, getGraveyardColor } from './graveUtils';


function GraveyardContainer({ factionMap, graveyard, showDeadPlayer }){
    const graveItems = graveyard.map(grave => ({
        color: getGraveyardColor(grave, factionMap),
        id: grave.name,
        name: getDeadPlayerText(grave),
        playerName: grave.name,
    }));

    return (
        <TitledListContainer
            containerClassName="graveyardContainer darkenedSetupPanel"
            items={graveItems}
            onItemClick={grave => showDeadPlayer(grave.playerName)}
            titleText="Graveyard"
        />
    );
}

const mapStateToProps = ({ gameState, setupState }) => ({
    factionMap: setupState.factionMap,
    graveyard: Object.values(gameState.playerMap).filter(p => p.flip),
});

const mapDispatchToProps = {
    showDeadPlayer: onShowPlayerAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(GraveyardContainer);
