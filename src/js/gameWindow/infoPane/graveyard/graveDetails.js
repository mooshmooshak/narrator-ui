import React from 'react';
import { connect } from 'react-redux';

import EntityDetailsContainer from '../../../components/entityDetailsContainer';

import { onShowFactionAction } from '../../../actions/payloads/panelViewActions';

import { getDeadPlayerText, getGraveyardColor } from './graveUtils';


function GraveDetails({
    panelViewState, playerMap, factionMap, showFaction,
}){
    const player = playerMap[panelViewState.entityID];
    const graveFactionName = player.flip.factionID in factionMap
        ? factionMap[player.flip.factionID].name
        : undefined;


    let descText;
    if(player.flip.isDayDeath)
        descText = 'Day';
    else
        descText = 'Night';
    descText = `Died on ${descText} ${player.flip.dayNumber}`;

    return (
        <EntityDetailsContainer
            headerText={getDeadPlayerText(player)}
            headerColor={getGraveyardColor(player, factionMap)}
            subheaderText={graveFactionName}
            onSubheaderClick={() => showFaction(player.flip.factionID)}
            items={[
                descText,
                ...player.flip.deathTypes,
            ]}
        />
    );
}

const mapStateToProps = ({ panelViewState, setupState, gameState }) => ({
    factionMap: setupState.factionMap,
    playerMap: gameState.playerMap,
    panelViewState,
});

const mapDispatchToProps = {
    showFaction: onShowFactionAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(GraveDetails);
