import React, { useState, useEffect } from 'react';

export default function VotePaneSubheaderPopout({ phase, playerMap, trialedPlayerName }){
    const [currentTime, setCurrentTime] = useState(new Date().getTime());
    const [isPopoutOpen, setPopoutOpen] = useState(true);

    useEffect(() => {
        const interval = setInterval(() => {
            setCurrentTime(new Date().getTime());
        }, 1000);
        return () => clearInterval(interval);
    }, []);

    const minimumVoteCount = getMinimumVoteCount(phase, currentTime, playerMap);
    if(!trialedPlayerName){
        const subtitleText = `(${minimumVoteCount} vote minimum)`;
        return (
            <span className="voteMainSubtitle">{subtitleText}</span>
        );
    }
    if(!isPopoutOpen){
        const subtitleText = `${minimumVoteCount} votes or death `;
        return (
            <span className="voteMainSubtitle">
                {subtitleText}
                <i
                    className="votePopupOpenIcon fas fa-exclamation-circle"
                    onClick={() => setPopoutOpen(true)}
                />
            </span>
        );
    }
    return (
        <div className="votePopout">
            <span className="votePopoutTopRow">
                <span>
                    {trialedPlayerName}
                    {' '}
needs to:
                </span>
                <i
                    className="fas fa-minus-square votePopoutClose"
                    onClick={() => setPopoutOpen(false)}
                />
            </span>
            <span className="votePopoutBullet">
                - convince the voters that someone else should be on trial
            </span>
            <span className="votePopoutBullet">
- get their vote count below
                {' '}
                {minimumVoteCount}
            </span>
            <span className="votePopoutOrElse">Or face ELIMINATION!</span>
        </div>
    );
}

function getMinimumVoteCount({ endTime, length }, currentTime, playerMap){
    const livePlayerCount = Object.values(playerMap).filter(player => !player.flip).length - 1; // -1 for no skipper
    const timeLeft = (endTime - currentTime) / (length * 1000);
    const minTrialCount = timeLeft * livePlayerCount;
    return Math.floor(minTrialCount) + 1;
}
