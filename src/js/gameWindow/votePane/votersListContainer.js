import React from 'react';

import { NOT_VOTING, VOTE_VIEW_TYPES } from '../../util/constants';


export default function VotersListContainer({
    profileName, votedName, voters, onUnvoteSubmit,
}){
    return (
        <ul className="voteVotersList">
            {voters.map(voter => {
                const cancellable = profileName === voter.name && votedName !== NOT_VOTING
                    && voter.status === VOTE_VIEW_TYPES.IN_SYNC;
                const voterTextClassName = voter.status === VOTE_VIEW_TYPES.PENDING_CANCEL
                    ? 'voteCancelPendingText '
                    : '';
                return (
                    <li
                        className={`voterContainer${cancellable ? ' voteCancellable' : ''}`}
                        onClick={onUnvoteSubmit}
                        key={voter.name}
                    >
                        <div className="voteListActionContainer">
                            { getVotedActionItem(profileName, voter, votedName) }
                        </div>
                        <span className={`${voterTextClassName}voterText`}>{voter.name}</span>
                        { voter.status && voter.status !== VOTE_VIEW_TYPES.IN_SYNC
                        && <i className="fas fa-stopwatch voteIcon votePendingIcon" /> }
                    </li>
                );
            })}
        </ul>
    );
}

function getVotedActionItem(profileName, voter, votedName){
    if(profileName !== voter.name || votedName === NOT_VOTING)
        return (<i className="fas fa-check-square voteIcon" />);
    return (<i className="fas fa-check-square voteIcon voteCheckableIcon" />);
}
