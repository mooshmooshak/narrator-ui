var seed_key = "seed";

function getStory() {
  return $("#story_text").val();
}

function getVictims() {
  var victims = [];
  $("#deadTypes .selected").each(function(i, val){
     if($(this).attr('name') === 'Generic')
      victims.push(null);
    else
      victims.push($(this).attr('name'));
  });
  return victims;
}

function getTeams() {
  var victims = [];
  $("#teamTypes .selected").each(function(i, val){
     if($(this).attr('name') === 'Generic')
      victims.push('0');
    else
      victims.push($(this).attr('name'));
  });
  return victims;
}

function getKillers() {
  var killers = [];
  $("#killTypes .selected").each(function(i, val){
    if($(this).attr('name') === 'Generic')
      killers.push(null);
    else
      killers.push($(this).attr('name'));
  });
  return killers;
}

var seed;
$(document).ready(function(){
  if(storageEnabled())
    implantSeed();
  else
    seed = 0;

  requestStory();

  new SimpleBar(game_log);

  $("#size_30").click(function() {
    setButtonClicked($("#size_30"), true);
    setButtonClicked($("#size_15"), false);
    requestStory();
  });

  $("#size_15").click(function() {
    setButtonClicked($("#size_15"), true);
    setButtonClicked($("#size_30"), false);
    requestStory();
  });

  $("#submit_button").click(function() {
    submitStory();
  });

  $("#next_story").click(function() {
    incrementSeed();
    requestStory();
  });

  $("#previous_story").click(function() {
    decrementSeed();
    requestStory();
  });
});

function setButtonClicked(button, clicked){
  if(!clicked){
    button.removeClass("clickedButton");
    button.addClass("unclickedButton");
  }else{
    button.removeClass("unclickedButton");
    button.addClass("clickedButton");
  }
}

function validateStory(){
    var input = $('#story_text').val();
    if (input.length > 1000){
        alert("The field cannot contain more than 1000 characters!")
        return false
    }
    else
      return true
}

function requestStory() {

  var url = "/requestStory";

  var method = "POST";
  var async = true;
  var request = new XMLHttpRequest();
  request.open(method, url, async);
  request.onload = function() {
    var status = request.status;

    if(status !== 200){
      console.log(request.responseText);
      return;
    }
    var o = JSON.parse(request.responseText);
    createVictimList(o.roles);
    createKillerList(o.deaths);
    createTeamList(o.roles);
    displayLog(o.deaths);
  }

  var data = {'size': getSize(), 'seed': getSeed(), 'genre': getGenre(), 'setup': getGenre()};
  console.log(data);
  request.send(JSON.stringify(data));


}

function clearLog() {
  $("#game_log").empty();
}

function displayLog(story) {
  clearLog();

  var sl = new SimpleBar($("#game_log")[0]);

  var ul = $("#game_log");
  var li;
  var span;

  story.forEach(function(d){
    li = $("<li>");
    li.html(d.text);
    li.append($("<br>"));
    li.append($("<span class='deathDescriptor'>" + d.attacks.map(function(d){return d.name}).join(', ') + "</span>"));
    $(sl.getContentElement()).append(li);
  });

  sl.recalculate();
}

function incrementSeed() {
  if(storageEnabled()){
    localStorage.setItem(seed_key, getSeed() + 1);
    implantSeed();
  }
  else
    seed += 1;
}

function decrementSeed() {
  if(storageEnabled()){
    localStorage.setItem(seed_key, getSeed() - 1);
    implantSeed();
  }
  else
    seed -= 1;
}

function storageEnabled(){
  return typeof(Storage) !== undefined;
}

function implantSeed() {
  if(localStorage.getItem(seed_key) === null)
    localStorage.setItem(seed_key, 0);
  var seed = localStorage.getItem(seed_key);
  seed = parseInt(seed);
  if(seed > 2147483646 || seed < -2147483646)
    localStorage.setItem(seed_key, 0);
}

function createTeamList(roles) {
  console.log(roles);
  roles.sort(function(a, b){
      return a.teamName.localeCompare(b.teamName);
  })
  var li;
  var dropdown = $("#teamTypes");
  dropdown.empty();

  li = $("<li>");
  li.addClass('selected generic');
  li.text("Generic");
  li.attr("name", "Generic");
  li.click(function(){
    $("#teamTypes .selected").removeClass('selected');
    $(this).addClass('selected');
  });
  dropdown.append(li);

  var seen = [];

  for(var i = 0; i < roles.length; i++){
    if(seen.indexOf(roles[i].teamName) !== -1)
      continue;
    seen.push(roles[i].teamName);
    li = $("<li>");
    li.text(roles[i].teamName);
    li.attr("name", roles[i].teamVal);
    li.css('color', roles[i].color)
    li.click(function(){
      if($(this).hasClass('selected'))
        $(this).removeClass('selected');
      else{
        $('#teamTypes .generic').removeClass('selected');
        $(this).addClass('selected');
      }
      if(!$('#teamTypes .selected').length)
        $('#teamTypes .generic').addClass('selected');

    });

    dropdown.append(li);
  }
}

function createVictimList(roles) {
  roles.sort(function(a, b){
      return a.name.localeCompare(b.name);
  })
  var li;
  var dropdown = $("#deadTypes");
  dropdown.empty();

  li = $("<li>");
  li.addClass('selected generic');
  li.text("Generic");
  li.attr("name", "Generic");
  li.click(function(){
    $("#deadTypes .selected").removeClass('selected');
    $(this).addClass('selected');
  });
  dropdown.append(li);

  var seen = [];

  for(var i = 0; i < roles.length; i++){
    if(seen.indexOf(roles[i].name) !== -1)
      continue;
    seen.push(roles[i].name);
    li = $("<li>");
    li.text(roles[i].name);
    li.attr("name", roles[i].roleVal);
    li.click(function(){
      if($(this).hasClass('selected'))
        $(this).removeClass('selected');
      else{
        $('#deadTypes .generic').removeClass('selected');
        $(this).addClass('selected');
      }
      if(!$('#deadTypes .selected').length)
        $('#deadTypes .generic').addClass('selected');

    });

    dropdown.append(li);
  }
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function createKillerList(deaths) {
  deaths = deaths.map(function(d){
    return d.attacks;
  });
  deaths = deaths.reduce(function(total, n){
    return total.concat(n);
  });
  deaths = deaths.map(function(d){
    return d.killID;
  });
  deaths = deaths.filter(onlyUnique);
  deaths = deaths.filter(function(d){ return d.toLowerCase() !== 'lynch';});

  var li;
  var dropdown = $("#killTypes");
  dropdown.empty();

  li = $("<li>");
  li.addClass('selected generic');
  li.text("Generic");
  li.attr("name", "Generic");
  li.css('color', 'white');
  li.click(function(){
    $("#killTypes .selected").removeClass('selected');
    $(this).addClass('selected');
  });

  dropdown.append(li);

  for(var i = 0; i < deaths.length; i++){
    li = $("<li>");
    li.text(deaths[i]);
    li.attr("name", deaths[i]);

    li.click(function(){
      if($(this).hasClass('selected'))
        $(this).removeClass('selected');
      else{
        $('#killTypes .generic').removeClass('selected');
        $(this).addClass('selected');
      }
      if(!$('#killTypes .selected').length)
        $('#killTypes .generic').addClass('selected');

    });

    dropdown.append(li);
  }
}

function getSize(){
  return parseInt($("#playerInput").val());
}

function getSeed(){
  if(storageEnabled())
    return parseInt(localStorage.getItem(seed_key));
  return seed;
}

function getGenre(){
  return $("#setupInput").val();
}

function submitStory() {
  var story = getStory();
  if(story.length === 0){
    requestStory();
    return;
  }
  var submission = {
    "size": getSize(),
    "seed": getSeed(),
    "applicableDeaths": getKillers(),
    "applicableRoles": getVictims(),
    "applicableTeams": getTeams(),
    "story": getStory(),
    "genre": getGenre(),
    "setup": getGenre()
  };
  console.log(submission);


  var url = "/storySubmit";

  var method = "POST";
  var async = true;
  var request = new XMLHttpRequest();
  request.open(method, url, async);
  request.onload = function() {
    var status = request.status;

    if(status !== 200){
      console.log(request.responseText);
      return;
    }


    var o = JSON.parse(request.responseText);
    createVictimList(o.roles);
    createKillerList(o.deaths);
    createTeamList(o.roles);
    displayLog(o.deaths);

    $("#story_text").val('');
  }

  request.send(JSON.stringify(submission));
}
